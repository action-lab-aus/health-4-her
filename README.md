# Health4Her

A website designed to educate on the dangers of alcohol consumption.

## Setup

**Prerequisites**:

- [NodeJS](https://nodejs.org/en/download) (>=16)
- [pnpm](https://pnpm.io/installation) (>=8)

**Steps**:

- `pnpm install`
- Ensure the firebase project is selected:
  - `firebase projects:list`
  - If not selected, `firebase use <project-id>`

## Running a development environment

- From the root directory `pnpm watch:api` to watch for changes in the Firebase functions
- From the root directory `pnpm emulators` to run emulators
- Finally `pnpm dev` to run the NextJS development server

## Apps:

| Apps |                                            |
| ---- | ------------------------------------------ |
| APP  | [health-4-her-app](apps/health-4-her-app/) |
| API  | [health-4-her-api](apps/health-4-her-api/) |
