# Health4Her App

A website designed to educate on the dangers of alcohol consumption.

## Setup

**Prerequisites**:

- [NodeJS](https://nodejs.org/en/download) (>=16)
- [pnpm](https://pnpm.io/installation) (>=8)

**Steps**:

- Create a new .env file and add in the required [env variables)](src/env.mjs) for Firebase project config
