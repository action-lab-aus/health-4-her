import { initializeApp } from "firebase/app";
import { getFirestore, connectFirestoreEmulator } from "firebase/firestore";
import { getAuth, connectAuthEmulator } from "firebase/auth";
import { env } from "@/env.mjs";
import { connectFunctionsEmulator, getFunctions } from "firebase/functions";

const firebaseConfig = {
  apiKey: env.NEXT_PUBLIC_FIREBASE_API_KEY,
  authDomain: env.NEXT_PUBLIC_FIREBASE_AUTH_DOMAIN,
  projectId: env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
  storageBucket: env.NEXT_PUBLIC_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: env.NEXT_PUBLIC_FIREBASE_MESSAGING_SENDER_ID,
  appId: env.NEXT_PUBLIC_FIREBASE_APP_ID,
};

const initialise = () => {
  if (typeof window === "undefined") {
    return;
  }
  const app = initializeApp(firebaseConfig);
  if (process.env.NODE_ENV === "development") {
    console.debug("Using firebase emulators...");
    connectFirestoreEmulator(getFirestore(), "localhost", 8080);
    connectAuthEmulator(getAuth(), "http://localhost:9099", {
      disableWarnings: true,
    });
    connectFunctionsEmulator(
      getFunctions(app, "australia-southeast1"),
      "localhost",
      5001
    );
  }
  return app;
};

export const app = initialise();
