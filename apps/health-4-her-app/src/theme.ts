import config from "../tailwind.config";
import { ObjectValues } from "./typeUtils";

// { [ColourPrefix]: string | Record<Variant, string> }
type ColourConfig = (typeof config)["theme"]["colors"];

export type ColourPrefixes = keyof ColourConfig;

// Given a colour prefix, return the set of all of its variants
type ColourVariants<ColourPrefix extends ColourPrefixes> =
  string extends ColourConfig[ColourPrefix]
    ? never
    : string & keyof ColourConfig[ColourPrefix];

type NewType = ObjectValues<{
  [ColourPrefix in ColourPrefixes]: {
    // If there is no colour variants, return just the prefix,
    // otherwise return all of the permutations of the prefix
    // and its variants
    value: ColourVariants<ColourPrefix> extends never
      ? ColourPrefix
      : `${ColourPrefix}-${ColourVariants<ColourPrefix>}`;
  };
}>;

// This blob of typescript gets all of the colours for the tailwind config
// It does this by walking all of the colour prefixes and walking all of
// the variants (if there are any)
export type Colours = NewType["value"];

// Union of all colour variants
type AllVariants = ObjectValues<{
  [ColourPrefix in ColourPrefixes]: {
    value: ColourConfig[ColourPrefix] extends string
      ? never
      : keyof ColourConfig[ColourPrefix];
  };
}>["value"];

// Given a variant, return all of the colour prefixes with that variant
export type ColourPrefixesWithVariant<Variant extends AllVariants> =
  ObjectValues<{
    [ColourPrefix in ColourPrefixes]: {
      value: ColourConfig[ColourPrefix] extends string
        ? never
        : Variant extends keyof ColourConfig[ColourPrefix]
        ? ColourPrefix
        : never;
    };
  }>["value"];

export const contrastText = {
  white: "text-black-500",
  transparent: "text-black-500",
  error: "text-white",
  "dark-pink": "text-white",
  "light-pink": "text-black-500",
  pink: "text-white",
  black: "text-white",
  inherit: "text-inherit",
  green: "text-black-500",
  red: "text-black-500",
  blue: "text-black-500",
} satisfies Record<ColourPrefixes, `text-${Colours}`>;
