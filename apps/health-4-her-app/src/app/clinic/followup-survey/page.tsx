"use client";

import { AppBar } from "@/components/AppBar";
import { Loading } from "@/components/Loading";
import { invalidateAuth, useAuth } from "@/queries/useAuth";
import { Button } from "@/components/Button";
import { useSignOut } from "@/mutations/useSignOut";
import { useQueryClient } from "react-query";
import { useEffect, useState } from "react";
import { Welcome } from "@/components/Welcome";
import { QuitModal } from "@/components/QuitModal";
import {
  AppEnv,
  FollowupSurveyCollectionType,
  FollowupSurveyProgressCollectionType,
} from "@/types";
import Exit from "@/components/icons/Exit";
import toast, { Toaster } from "react-hot-toast";
import { Timestamp } from "firebase/firestore";
import { FollowupSurveyCard } from "@/components/FollowupSurveyCard";
import { useFollowupSurvey } from "@/queries/followupSurvey";
import { useFollowupSurveyProgress } from "@/queries/followupSurveyProgress";
import { checkReferenceId } from "@/services/functions";
import { useSearchParams } from "next/navigation";
import { InfoCard } from "@/components/InfoCard";
import { Arm } from "@/surveys/types";

const CLOSING_DATE = new Date("2023-11-04T00:00:00+11:00");

export default function Home() {
  const searchParams = useSearchParams();
  const refId = searchParams.get("refId");

  const authQuery = useAuth();
  const [showConsent, setShowConsent] = useState(false);
  const [showPIS, setShowPIS] = useState(false);

  const queryClient = useQueryClient();
  const followupSurveyQuery = useFollowupSurvey(
    FollowupSurveyCollectionType.CLINICAL,
    authQuery.data?.uid
  );
  const followupSurveyProgressQuery = useFollowupSurveyProgress(
    FollowupSurveyProgressCollectionType.CLINICAL,
    authQuery.data?.uid
  );
  const dataIsLoading =
    authQuery.isLoading ||
    followupSurveyQuery.isLoading ||
    followupSurveyProgressQuery.isLoading;
  const signOutMutation = useSignOut({
    onSuccess: () => {
      setShowConsent(false);
      invalidateAuth(queryClient);
    },
  });

  const [isRefIdValid, setIsRefIdValid] = useState(false);
  const [arm, setArm] = useState<Arm | null>(null);
  const [open, setOpen] = useState(false);
  const [startTimestamp, setStartTimestamp] = useState<Timestamp>(
    Timestamp.now()
  );

  const handleGetStartedClick = () => {
    verifyReferenceId();
    setStartTimestamp(Timestamp.now());
    setShowConsent(true);
  };

  const verifyReferenceId = () => {
    const errorMessage =
      "Invalid reference ID. Please re-check the link provided to you in the Email/SMS";

    const successStyle = {
      style: {
        border: "1px solid #C52E65",
        padding: "16px",
        color: "#C52E65",
      },
      iconTheme: {
        primary: "#C52E65",
        secondary: "#FFFAEE",
      },
    };

    const errorStyle = {
      style: {
        border: "1px solid #C52E65",
        padding: "16px",
        color: "#C52E65",
      },
      iconTheme: {
        primary: "#C52E65",
        secondary: "#FFFAEE",
      },
    };

    if (checkReferenceId) {
      checkReferenceId!({
        referenceId: refId ?? "",
        appEnv: AppEnv.CLINICAL,
      })
        .then((res) => {
          if (!res.data) {
            toast.error(errorMessage, errorStyle);
            setIsRefIdValid(false);
            signOutMutation.mutate();
          } else {
            if (!res.data.result) {
              toast.error(res.data.errMsg, errorStyle);
              setIsRefIdValid(false);
              signOutMutation.mutate();
            } else {
              toast.success("Welcome back!", successStyle);
              setIsRefIdValid(true);
              setArm(res.data.arm);
            }
          }
        })
        .catch(() => {
          toast.error(errorMessage, errorStyle);
          setIsRefIdValid(false);
          signOutMutation.mutate();
        });
    }
  };

  useEffect(() => {
    verifyReferenceId();
  }, []);

  return (
    <>
      <AppBar>
        <div className="-z-50">
          <Toaster />
        </div>
        {isRefIdValid &&
        !authQuery.isLoading &&
        (authQuery.data || showConsent) ? (
          <Button
            onClick={() => {
              if (authQuery.data) {
                setOpen(true);
              } else {
                signOutMutation.mutate();
              }
            }}
            variant="outlined"
            className="text-dark-pink-600 border-0 p-5"
            colour="dark-pink"
          >
            {signOutMutation.isLoading ? (
              <Loading />
            ) : (
              <span className="flex gap-2">
                <p className="hidden sm:block">Exit program</p>
                <Exit className="h-7 w-7 transition-transform ease-linear hover:scale-110 hover:duration-150" />
              </span>
            )}
          </Button>
        ) : null}
        {isRefIdValid && !authQuery.isLoading && authQuery.data ? (
          <QuitModal
            appEnv={AppEnv.CLINICAL}
            open={open}
            setOpen={setOpen}
            progress={followupSurveyProgressQuery.data ?? undefined}
            userId={authQuery.data.uid}
            handleSignOut={signOutMutation.mutate}
          />
        ) : null}
      </AppBar>
      <div className="bg-light-pink-600 flex w-full grow items-start justify-center bg-opacity-10 p-3 md:p-6">
        {!showPIS && authQuery.isLoading ? (
          <Loading>Loading Auth</Loading>
        ) : null}
        {!showPIS && !dataIsLoading && !authQuery.data && !showConsent ? (
          <div className="pt-10">
            <Welcome image="followup" closingDate={CLOSING_DATE}>
              {isRefIdValid && (
                <Button
                  onClick={handleGetStartedClick}
                  variant="filled"
                  colour="pink"
                  className="rounded-full px-12 text-lg md:text-xl"
                >
                  Start Final Survey
                </Button>
              )}
            </Welcome>
          </div>
        ) : null}
        {!showPIS && !dataIsLoading && !authQuery.data && showConsent ? (
          <InfoCard />
        ) : null}
        {authQuery.data && !dataIsLoading ? (
          <FollowupSurveyCard
            appEnv={AppEnv.CLINICAL}
            userId={authQuery.data.uid}
            progress={followupSurveyProgressQuery.data}
            state={followupSurveyQuery.data}
            metadata={{
              startedAt: startTimestamp,
              screeningNumber: "N/A",
              deviceType: navigator.userAgent,
            }}
            signoutMutation={signOutMutation}
            referenceId={!isRefIdValid ? "" : refId ?? ""}
            arm={arm ?? "intervention"}
          />
        ) : null}
      </div>
    </>
  );
}
