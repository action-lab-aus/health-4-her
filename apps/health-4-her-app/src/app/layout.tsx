"use client";

import "./globals.css";
import { Lato } from "next/font/google";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";

const lato = Lato({ subsets: ["latin"], weight: ["400", "700"] });

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <title>Health4Her</title>
      <QueryClientProvider client={queryClient}>
        <body className={`${lato.className} absolute inset-0 flex flex-col`}>
          {children}
          <ReactQueryDevtools initialIsOpen={false} />
        </body>
      </QueryClientProvider>
    </html>
  );
}
