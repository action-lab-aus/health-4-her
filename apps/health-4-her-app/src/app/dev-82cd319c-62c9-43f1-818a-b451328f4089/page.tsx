"use client";

import { AppBar } from "@/components/AppBar";
import { Loading } from "@/components/Loading";
import { useUserProgress } from "@/queries/userProgress";
import { Tool } from "@/components/Tool";
import { invalidateAuth, useAuth } from "@/queries/useAuth";
import { BaselineSurveyCard } from "@/components/BaselineSurveyCard";
import { Button } from "@/components/Button";
import { useSignOut } from "@/mutations/useSignOut";
import { useQueryClient } from "react-query";
import { ConsentCard } from "@/components/ConsentCard";
import { useEffect, useState } from "react";
import { Welcome } from "@/components/Welcome";
import { PostInterventionSurveyCard } from "@/components/PostInterventionSurveyCard";
import { ThanksCard } from "@/components/ThanksCard";
import { QuitModal } from "@/components/QuitModal";
import PISForm from "@/components/PISForm";
import {
  AppEnv,
  BaselineSurveyCollectionType,
  BaselineSurveyProgressCollectionType,
  Consent,
  ScheduledNotificationsCollectionType,
  UserProgressCollectionType,
} from "@/types";
import { useBaselineSurveyProgress } from "@/queries/baselineSurveyProgress";
import { useBaselineSurvey } from "@/queries/baselineSurvey";
import Exit from "@/components/icons/Exit";
import { Toaster } from "react-hot-toast";
import { Timestamp } from "firebase/firestore";
import { usePathname } from "next/navigation";
import { useScheduledNotifications } from "@/queries/scheduledNotifications";
import { getScreeningNumber } from "@/services/functions";

const CLOSING_DATE = new Date("2024-11-04T00:00:00+11:00");

export default function Home() {
  const pathname = usePathname();
  const authQuery = useAuth();
  const [showConsent, setShowConsent] = useState(false);
  const [showPIS, setShowPIS] = useState(false);
  const [hasConsented, setHasConsented] = useState(false);
  const [screeningNumberHasLoaded, setScreeningNumberHasLoaded] =
    useState(false);
  const [consent, setConsent] = useState<Consent>({
    surveyAndActivity: false,
    followUpSurvey: false,
    telephoneInterview: false,
    consentedAt: null,
  });

  const queryClient = useQueryClient();
  const userProgressQuery = useUserProgress(
    authQuery.data?.uid,
    UserProgressCollectionType.DEV
  );
  const baselineSurveyQuery = useBaselineSurvey(
    BaselineSurveyCollectionType.DEV,
    authQuery.data?.uid
  );
  const baselineSurveyProgressQuery = useBaselineSurveyProgress(
    BaselineSurveyProgressCollectionType.DEV,
    authQuery.data?.uid
  );
  const scheduledNotificationsQuery = useScheduledNotifications(
    authQuery.data?.uid,
    ScheduledNotificationsCollectionType.DEV
  );
  const dataIsLoading =
    authQuery.isLoading ||
    baselineSurveyProgressQuery.isLoading ||
    userProgressQuery.isLoading;
  const signOutMutation = useSignOut({
    onSuccess: () => {
      setShowConsent(false);
      invalidateAuth(queryClient);
    },
  });

  const lastPage = userProgressQuery.data?.journey.at(-1);
  const completedLastPage = lastPage
    ? Boolean(userProgressQuery.data?.[lastPage]?.completedAt)
    : false;

  const [open, setOpen] = useState(false);
  const [startTimestamp, setStartTimestamp] = useState<Timestamp>(
    Timestamp.now()
  );
  const [screeningNumber, setScreeningNumber] = useState<string>(
    baselineSurveyQuery.data?.screeningNumber ?? ""
  );

  useEffect(() => {
    setHasConsented(false);
    localStorage.removeItem("hideOverlay");

    const lastRoute = localStorage.getItem("lastRoute");
    if (lastRoute && lastRoute !== pathname) {
      signOutMutation.mutate();
    }
    localStorage.setItem("lastRoute", pathname);

    // Get screening number
    getScreeningNumber!({
      client: AppEnv.DEV,
    })
      .then((number) => {
        if (!screeningNumber) {
          setScreeningNumber(number.data.toString());
        }
      })
      .catch(() => {});
  }, []);

  useEffect(() => {
    if (!authQuery.data) {
      setScreeningNumberHasLoaded(false);
    }
  }, [authQuery.data]);

  useEffect(() => {
    if (baselineSurveyQuery.data?.screeningNumber) {
      setScreeningNumber(baselineSurveyQuery.data?.screeningNumber);
      setScreeningNumberHasLoaded(true);
    }
  }, [baselineSurveyQuery.data]);

  // useEffect(() => {
  //   if (
  //     authQuery.data &&
  //     baselineSurveyQuery.isFetchedAfterMount &&
  //     baselineSurveyQuery.data == undefined &&
  //     !screeningNumber
  //   ) {
  //     signOutMutation.mutate();
  //   }
  // }, [baselineSurveyQuery.isFetchedAfterMount]);

  const handleGetStartedClick = () => {
    setStartTimestamp(Timestamp.now());
    setShowConsent(true);
  };

  const handleConsentNextClick = () => {
    setScreeningNumberHasLoaded(true);
  };

  return (
    <>
      <AppBar>
        <div>
          <Toaster />
        </div>
        {!authQuery.isLoading && (authQuery.data || showConsent) ? (
          <Button
            onClick={() => {
              if (authQuery.data && userProgressQuery.data?.page !== "done") {
                setOpen(true);
              } else {
                signOutMutation.mutate();
              }
            }}
            variant="outlined"
            className="text-dark-pink-600 border-0 p-5"
            colour="dark-pink"
          >
            {signOutMutation.isLoading ? (
              <Loading />
            ) : (
              <span className="flex gap-2">
                <p className="hidden sm:block">Exit program</p>
                <Exit className="h-7 w-7 transition-transform ease-linear hover:scale-110 hover:duration-150" />
              </span>
            )}
          </Button>
        ) : null}
        {!authQuery.isLoading && authQuery.data ? (
          <QuitModal
            appEnv={AppEnv.DEV}
            open={open}
            setOpen={setOpen}
            progress={
              userProgressQuery.data ??
              baselineSurveyProgressQuery.data ??
              undefined
            }
            userId={authQuery.data.uid}
            handleSignOut={signOutMutation.mutate}
          />
        ) : null}
      </AppBar>
      <div className="bg-light-pink-600 flex w-full grow items-start justify-center bg-opacity-10 p-3 md:p-6">
        {!showPIS && authQuery.isLoading ? (
          <Loading>Loading Auth</Loading>
        ) : null}
        {!showPIS && userProgressQuery.isLoading ? (
          <Loading>Loading Session</Loading>
        ) : null}
        {!showPIS && !dataIsLoading && !authQuery.data && showConsent ? (
          <ConsentCard onNextClick={handleConsentNextClick} />
        ) : null}
        {showPIS ? (
          <PISForm
            hasConsented={hasConsented}
            setHasConsented={setHasConsented}
            setShowPIS={setShowPIS}
            consent={consent}
            setConsent={setConsent}
          />
        ) : null}
        {!showPIS && !dataIsLoading && !authQuery.data && !showConsent ? (
          <Welcome image="baseline" closingDate={CLOSING_DATE}>
            <Button
              onClick={handleGetStartedClick}
              variant="filled"
              colour="pink"
              className="rounded-full px-12 text-lg md:text-xl"
            >
              Get Started
            </Button>
          </Welcome>
        ) : null}
        {!showPIS &&
        authQuery.data &&
        !userProgressQuery.data &&
        !dataIsLoading ? (
          <BaselineSurveyCard
            appEnv={AppEnv.DEV}
            userId={authQuery.data.uid}
            consent={consent}
            progress={baselineSurveyProgressQuery.data}
            state={baselineSurveyQuery.data}
            metadata={{
              startedAt: startTimestamp,
              screeningNumber: screeningNumber,
              deviceType: navigator.userAgent,
            }}
            signoutMutation={signOutMutation}
          />
        ) : null}
        {!showPIS &&
        authQuery.data &&
        userProgressQuery.data &&
        !completedLastPage ? (
          !dataIsLoading ? (
            <Tool
              appEnv={AppEnv.DEV}
              progress={userProgressQuery.data}
              auth={authQuery.data}
            />
          ) : (
            <Loading />
          )
        ) : null}
        {!showPIS &&
        authQuery.data &&
        completedLastPage &&
        userProgressQuery.data?.page !== "done" ? (
          <PostInterventionSurveyCard
            appEnv={AppEnv.DEV}
            userId={authQuery.data.uid}
          />
        ) : null}
        {!showPIS &&
        authQuery.data &&
        userProgressQuery.data?.page === "done" ? (
          <ThanksCard
            appEnv={AppEnv.DEV}
            signOutMutation={signOutMutation}
            scheduledNotificationsQuery={scheduledNotificationsQuery}
          />
        ) : null}
      </div>
    </>
  );
}
