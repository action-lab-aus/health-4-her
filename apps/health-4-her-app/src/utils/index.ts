export const formatToAUPrefix = (number: string) => {
  // Remove any spaces from the input.
  let sanitizedInput = number.replace(/\s/g, "");

  // Check the beginning of the input and adjust accordingly.
  if (sanitizedInput.startsWith("0")) {
    return "+61" + sanitizedInput.substring(1);
  } else if (sanitizedInput.startsWith("+61")) {
    return sanitizedInput;
  } else {
    throw new Error("Invalid input"); // Return an error message for inputs that do not start with '0' or '+61'.
  }
};
