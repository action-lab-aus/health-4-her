import { ActivityType, ActivityKeys } from "@/types";

type Activities = {
  [ActivityKey in ActivityKeys]: {
    key: ActivityKey;
    showCorrectAnswers: boolean;
  } & Omit<ActivityType, "key">;
};

export const activities = {
  activity1: {
    key: "activity1",
    type: "multiple-choice",
    showCorrectAnswers: true,
    question: {
      text: (
        <>
          Which of the following lifestyle factors{" "}
          <span className="italic">decrease</span> risk of breast cancer?
        </>
      ),
      src: "/audio/h4h/Activity1.mp3",
    },
    title: "Activity 1",
    options: [
      { value: "1", label: "avoiding coffee" },
      { value: "2", label: "physical activity", correct: true },
      {
        value: "3",
        label: "keeping weight in a healthy range",
        correct: true,
      },
      { value: "4", label: "avoiding soy products" },
    ],
    feedback: {
      correct: {
        text: "You are correct! Being physically active and keeping weight in a healthy range are protective factors that decrease breast cancer risk",
        audioSrc: "/audio/h4h/Activity1FeedbackCorrect.mp3",
      },
      incorrect: {
        text: "Not quite! Being physically active and keeping weight in a healthy range are protective factors that decrease breast cancer risk.",
        audioSrc: "/audio/h4h/Activity1FeedbackWrong.mp3",
      },
    },
  },
  activity2: {
    key: "activity2",
    type: "multiple-choice",
    showCorrectAnswers: true,
    question: {
      text: (
        <>
          Drinking <i>very low amounts</i> of alcohol <i>increases</i> the risk
          of which of the following cancers?
        </>
      ),
      src: "/audio/h4h/Activity2.mp3",
    },
    title: "Activity 1",
    options: [
      { value: "1", label: "liver cancer" },
      { value: "2", label: "bowel cancer" },
      {
        value: "3",
        label: "pancreatic cancer",
      },
      { value: "4", label: "breast cancer", correct: true },
    ],
    feedback: {
      correct: {
        text: "You are correct – while alcohol does contribute to increased risk of all of these cancers, only breast cancer is associated with drinking very low amounts.",
        audioSrc: "/audio/h4h/Activity2FeedbackCorrect.mp3",
      },
      incorrect: {
        text: "Not quite – while alcohol does contribute to increased risk of all these cancers, only breast cancer is associated with drinking very low amounts.",
        audioSrc: "/audio/h4h/Activity2FeedbackWrong.mp3",
      },
    },
  },
  activity3: {
    key: "activity3",
    type: "single-choice",
    showCorrectAnswers: false,
    question: {
      text: "Of the following, which health improvement do you look forward to most?",
      src: "/audio/h4h/Activity3.mp3",
    },
    title: "Activity 3",
    options: [
      {
        value: "1",
        label: "more restful sleep",
        correct: true,
      },
      {
        value: "2",
        label: "improved memory and mental clarity",
        correct: true,
      },
      {
        value: "3",
        label: "more energy",
        correct: true,
      },
      {
        value: "4",
        label: "better mood",
        correct: true,
      },
      {
        value: "5",
        label: "improved physical health",
        correct: true,
      },
    ],
    feedback: {
      "1": {
        text: "Good choice! Drinking less is a great way to improve your sleep. There are many great health benefits to drinking less.",
        audioSrc: "/audio/h4h/Activity3FeedbackImproveSleep.mp3",
      },
      "2": {
        text: "Good choice! Drinking less is a great way to improve your memory and mental clarity. There are many great health benefits to drinking less.",
        audioSrc: "/audio/h4h/Activity3FeedbackImproveMemory.mp3",
      },
      "3": {
        text: "Good choice! Drinking less is a great way to improve your energy levels. There are many great health benefits to drinking less.",
        audioSrc: "/audio/h4h/Activity3FeedbackImproveEnergy.mp3",
      },
      "4": {
        text: "Good choice! Drinking less is a great way to improve your mood. There are many great health benefits to drinking less.",
        audioSrc: "/audio/h4h/Activity3FeedbackImproveMood.mp3",
      },
      "5": {
        text: "Good choice! Drinking less is a great way to improve your physical health. There are many great health benefits to drinking less.",
        audioSrc: "/audio/h4h/Activity3FeedbackImprovePhysicalHealth.mp3",
      },
    },
  },
  activity4: {
    key: "activity4",
    type: "single-choice",
    showCorrectAnswers: false,
    question: {
      text: "Kate has been invited out to celebrate a big milestone with friends. She’s excited about the evening, but is a little nervous as she is trying to drink less alcohol.\n\nWhich strategy would you suggest she use?",
      src: "/audio/h4h/Activity4.mp3",
    },
    title: "Activity 4",
    options: [
      {
        label: "Alternate alcoholic with non-alcoholic drinks",
        value: "1",
        correct: true,
      },
      {
        label: "Offer to buy a round of fun non-alcoholic drinks",
        value: "2",
        correct: true,
      },
      {
        label:
          "Tell her friends she’s having a night off from drinking because she’s drinking less for her health",
        value: "3",
        correct: true,
      },
      {
        label:
          "Tell her friends she’s having a night off from drinking because she’s driving",
        value: "4",
        correct: true,
      },
    ],
    feedback: {
      1: {
        text: "Good choice! Remember, there are many ways we can reduce our alcohol intake.",
        audioSrc: "/audio/h4h/Activity4FeedbackCorrect.mp3",
      },
      2: {
        text: "Good choice! Remember, there are many ways we can reduce our alcohol intake.",
        audioSrc: "/audio/h4h/Activity4FeedbackCorrect.mp3",
      },
      3: {
        text: "Good choice! Remember, there are many ways we can reduce our alcohol intake.",
        audioSrc: "/audio/h4h/Activity4FeedbackCorrect.mp3",
      },
      4: {
        text: "Good choice! Remember, there are many ways we can reduce our alcohol intake.",
        audioSrc: "/audio/h4h/Activity4FeedbackCorrect.mp3",
      },
    },
  },
} satisfies Activities;
