import { VideoKeys } from "./types";

type Videos = {
  [VideoKey in VideoKeys]: {
    key: VideoKey;
    src: string;
    vimeoSrc: string;
  };
};

export const videoSrc = {
  video1: {
    key: "video1",
    src: "/videos/final/H4H_2023_Chunk1_Subs.mp4",
    vimeoSrc: "https://vimeo.com/866646210/136da498d2",
  },
  video2: {
    key: "video2",
    src: "/videos/final/H4H_2023_Chunk2_Subs.mp4",
    vimeoSrc: "https://vimeo.com/866646236/c6179d952d",
  },
  video3: {
    key: "video3",
    src: "/videos/final/H4H_2023_Chunk3a_b_Subs.mp4",
    vimeoSrc: "https://vimeo.com/866646254/a62d0568ff",
  },
  video4: {
    key: "video4",
    src: "/videos/final/H4H_2023_Chunk4_Subs.mp4",
    vimeoSrc: "https://vimeo.com/866646321/94e32cea1a",
  },
  video5Control: {
    key: "video5Control",
    src: "/videos/final/H4H_2023_Chunk5_Control_Subs.mp4",
    vimeoSrc: "https://vimeo.com/866646340/d3169d4a4e",
  },
  video5Intervention: {
    key: "video5Intervention",
    src: "/videos/h4h/H4H_2023_Chunk5_Intervention_Subs.mp4",
    vimeoSrc: "https://vimeo.com/866646358/b05400e64c",
  },
} satisfies Videos;
