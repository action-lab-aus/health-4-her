import { followupSurveyCollection } from "./../services/firestore";
import { getDoc, doc } from "firebase/firestore";
import { QueryClient, useQuery } from "react-query";
import { FollowupSurveyCollectionType } from "@/types";

const makeQueryKey = (
  collectionType: FollowupSurveyCollectionType,
  userId: string | undefined
) => [collectionType, userId];

export const useFollowupSurvey = (
  collectionType: FollowupSurveyCollectionType,
  userId: string | undefined
) => {
  return useQuery({
    queryKey: makeQueryKey(collectionType, userId),
    enabled: Boolean(userId),
    queryFn: async () => {
      const collection = followupSurveyCollection(collectionType);
      if (!collection) {
        return;
      }
      const snapshot = await getDoc(doc(collection, userId));
      return snapshot.data();
    },
  });
};

export const invalidateFollowupSurvey = (
  collectionType: FollowupSurveyCollectionType,
  client: QueryClient,
  userId: string
) => {
  client.invalidateQueries(makeQueryKey(collectionType, userId));
};
