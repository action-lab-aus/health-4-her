import { scheduledNotificationsCollection } from "@/services/firestore";
import { ScheduledNotificationsCollectionType } from "@/types";
import { getDoc, doc } from "firebase/firestore";
import { QueryClient, useQuery } from "react-query";

const makeQueryKey = (
  userId: string | undefined,
  collectionType: ScheduledNotificationsCollectionType
) => [collectionType, userId];

export const useScheduledNotifications = (
  userId: string | undefined,
  collectionType: ScheduledNotificationsCollectionType
) => {
  return useQuery({
    queryKey: makeQueryKey(userId, collectionType),
    enabled: false,
    queryFn: async () => {
      const collection = scheduledNotificationsCollection(collectionType);
      if (!collection) {
        return;
      }
      const snapshot = await getDoc(doc(collection, userId));
      return snapshot.data();
    },
  });
};

export const invalidateScheduledNotifications = (
  client: QueryClient,
  userId: string,
  collectionType: ScheduledNotificationsCollectionType
) => {
  client.invalidateQueries(makeQueryKey(userId, collectionType));
};
