import { FollowupSurveyProgressCollectionType } from "./../types";
import { followupSurveyProgressCollection } from "./../services/firestore";
import { getDoc, doc } from "firebase/firestore";
import { QueryClient, useQuery } from "react-query";

const makeQueryKey = (
  collectionType: FollowupSurveyProgressCollectionType,
  userId: string | undefined
) => [collectionType, userId];

export const useFollowupSurveyProgress = (
  collectionType: FollowupSurveyProgressCollectionType,
  userId: string | undefined
) => {
  return useQuery({
    queryKey: makeQueryKey(collectionType, userId),
    enabled: Boolean(userId),
    queryFn: async () => {
      const collection = followupSurveyProgressCollection(collectionType);
      if (!collection) {
        return;
      }
      const snapshot = await getDoc(doc(collection, userId));
      return snapshot.data();
    },
  });
};

export const invalidateFollowupSurveyProgress = (
  collectionType: FollowupSurveyProgressCollectionType,
  client: QueryClient,
  userId: string
) => {
  client.invalidateQueries(makeQueryKey(collectionType, userId));
};
