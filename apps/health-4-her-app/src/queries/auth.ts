import { User, getAuth, onAuthStateChanged } from "firebase/auth";
import { QueryKey, UseQueryOptions, useQuery } from "react-query";
import { app } from "@/firebase";

const getUserAuth = () => {
  return new Promise<User | undefined>((res) => {
    const unsubscribe = onAuthStateChanged(getAuth(app), (user) => {
      unsubscribe();
      res(user ?? undefined);
    });
  });
};

export const useAuth = (
  options?: Omit<
    UseQueryOptions<User | undefined, unknown, User | undefined, QueryKey>,
    "queryFn"
  >
) =>
  useQuery({
    queryKey: ["auth"],
    queryFn: getUserAuth,
    ...options,
  });
