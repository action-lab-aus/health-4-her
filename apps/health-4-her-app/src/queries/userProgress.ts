import { userProgressCollection } from "@/services/firestore";
import { UserProgressCollectionType } from "@/types";
import { getDoc, doc } from "firebase/firestore";
import { QueryClient, useQuery } from "react-query";

const makeQueryKey = (
  userId: string | undefined,
  collectionType: UserProgressCollectionType
) => [collectionType, userId];

export const useUserProgress = (
  userId: string | undefined,
  collectionType: UserProgressCollectionType
) => {
  return useQuery({
    queryKey: makeQueryKey(userId, collectionType),
    enabled: Boolean(userId),
    queryFn: async () => {
      const collection = userProgressCollection(collectionType);
      if (!collection) {
        return;
      }
      const snapshot = await getDoc(doc(collection, userId));
      return snapshot.data();
    },
  });
};

export const invalidateUserProgress = (
  client: QueryClient,
  userId: string,
  collectionType: UserProgressCollectionType
) => {
  client.invalidateQueries(makeQueryKey(userId, collectionType));
};
