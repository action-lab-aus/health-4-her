import { baselineSurveyCollection } from "./../services/firestore";
import { getDoc, doc } from "firebase/firestore";
import { QueryClient, useQuery } from "react-query";
import { BaselineSurveyCollectionType } from "@/types";

const makeQueryKey = (
  collectionType: BaselineSurveyCollectionType,
  userId: string | undefined
) => [collectionType, userId];

export const useBaselineSurvey = (
  collectionType: BaselineSurveyCollectionType,
  userId: string | undefined
) => {
  return useQuery({
    queryKey: makeQueryKey(collectionType, userId),
    enabled: Boolean(userId),
    queryFn: async () => {
      const collection = baselineSurveyCollection(collectionType);
      if (!collection) {
        return;
      }
      const snapshot = await getDoc(doc(collection, userId));
      return snapshot.data();
    },
  });
};

export const invalidateBaselineSurvey = (
  collectionType: BaselineSurveyCollectionType,
  client: QueryClient,
  userId: string
) => {
  client.invalidateQueries(makeQueryKey(collectionType, userId));
};
