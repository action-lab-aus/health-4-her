import { BaselineSurveyProgressCollectionType } from "./../types";
import { baselineSurveyProgressCollection } from "./../services/firestore";
import { getDoc, doc } from "firebase/firestore";
import { QueryClient, useQuery } from "react-query";

const makeQueryKey = (
  collectionType: BaselineSurveyProgressCollectionType,
  userId: string | undefined
) => [collectionType, userId];

export const useBaselineSurveyProgress = (
  collectionType: BaselineSurveyProgressCollectionType,
  userId: string | undefined
) => {
  return useQuery({
    queryKey: makeQueryKey(collectionType, userId),
    enabled: Boolean(userId),
    queryFn: async () => {
      const collection = baselineSurveyProgressCollection(collectionType);
      if (!collection) {
        return;
      }
      const snapshot = await getDoc(doc(collection, userId));
      return snapshot.data();
    },
  });
};

export const invalidateBaselineSurveyProgress = (
  collectionType: BaselineSurveyProgressCollectionType,
  client: QueryClient,
  userId: string
) => {
  client.invalidateQueries(makeQueryKey(collectionType, userId));
};
