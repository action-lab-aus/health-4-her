import {
  SurveyDiscontinueInfo,
  BaselineSurveyProgress,
  SurveyMetadata,
  FollowupSurveyProgress,
} from "./../surveys/types";
import {
  BaselineSurveyCollectionType,
  BaselineSurveyProgressCollectionType,
  Consent,
  FollowupSurveyCollectionType,
  FollowupSurveyProgressCollectionType,
  PostInterventionSurveyCollectionType,
  ScheduledNotificationsCollectionType,
  SurveyDiscontinueCollectionType,
  UserProgressCollectionType,
} from "./../types";
import {
  DocumentSnapshot,
  Timestamp,
  UpdateData,
  WithFieldValue,
  WriteBatch,
  collection,
  doc,
  getFirestore,
  serverTimestamp,
  updateDoc,
  writeBatch,
} from "firebase/firestore";
import { app } from "@/firebase";
import { UserProgress } from "@/types";
import { SurveyState } from "@/surveys/types";
import { baselineSurveyClinical } from "@/components/BaselineSurveyCard";
import { postInterventionSurvey } from "@/components/PostInterventionSurveyCard";
import { followupSurveyControl } from "@/components/FollowupSurveyCard";

export type BaselineSurveyResponses = SurveyState<
  typeof baselineSurveyClinical
>;
export type PostInterventionSurveyResponses = SurveyState<
  typeof postInterventionSurvey
>;
export type FollowupSurveyResponses = SurveyState<
  typeof followupSurveyControl
>;

export const consentCollection = () =>
  app
    ? collection(getFirestore(app), "/consent").withConverter({
        toFirestore: (data: Consent) => data,
        fromFirestore: (snap: DocumentSnapshot) => snap.data() as Consent,
      })
    : undefined;

export const baselineSurveyProgressCollection = (
  collectionType: BaselineSurveyProgressCollectionType
) =>
  app
    ? collection(getFirestore(app), collectionType).withConverter({
        toFirestore: (data: BaselineSurveyProgress) => data,
        fromFirestore: (snap: DocumentSnapshot) =>
          snap.data() as BaselineSurveyProgress,
      })
    : undefined;

export const followupSurveyProgressCollection = (
  collectionType: FollowupSurveyProgressCollectionType
) =>
  app
    ? collection(getFirestore(app), collectionType).withConverter({
        toFirestore: (data: FollowupSurveyProgress) => data,
        fromFirestore: (snap: DocumentSnapshot) =>
          snap.data() as FollowupSurveyProgress,
      })
    : undefined;

export const userProgressCollection = (type: UserProgressCollectionType) =>
  app
    ? collection(getFirestore(app), `/${type}`).withConverter({
        toFirestore: (data: UserProgress) => data,
        fromFirestore: (snap: DocumentSnapshot) => snap.data() as UserProgress,
      })
    : undefined;

export const baselineSurveyCollection = (type: BaselineSurveyCollectionType) =>
  app
    ? collection(getFirestore(app), `/${type}`).withConverter({
        toFirestore: (
          data: BaselineSurveyResponses & {
            startedAt?: Timestamp | null;
            screeningNumber?: string;
            deviceType?: string;
            completedAt: Timestamp | null;
          }
        ) => data,
        fromFirestore: (snap: DocumentSnapshot) =>
          snap.data() as BaselineSurveyResponses & {
            startedAt?: Timestamp | null;
            screeningNumber?: string;
            deviceType?: string;
            completedAt: Timestamp | null;
          },
      })
    : undefined;

export const followupSurveyCollection = (type: FollowupSurveyCollectionType) =>
  app
    ? collection(getFirestore(app), `/${type}`).withConverter({
        toFirestore: (
          data: FollowupSurveyResponses & {
            startedAt?: Timestamp | null;
            deviceType?: string;
            completedAt: Timestamp | null;
            referenceId: string;
          }
        ) => data,
        fromFirestore: (snap: DocumentSnapshot) =>
          snap.data() as FollowupSurveyResponses & {
            startedAt?: Timestamp | null;
            deviceType?: string;
            completedAt: Timestamp | null;
            referenceId: string;
          },
      })
    : undefined;

const postInterventionSurveyCollection = (
  collectionType: PostInterventionSurveyCollectionType
) =>
  app
    ? collection(getFirestore(app), collectionType).withConverter({
        toFirestore: (
          data: PostInterventionSurveyResponses & { completedAt: Timestamp }
        ) => data,
        fromFirestore: (snap: DocumentSnapshot) =>
          snap.data() as PostInterventionSurveyResponses & {
            completedAt: Timestamp;
          },
      })
    : undefined;

const surveyDiscontinueCollection = (
  collectionType: SurveyDiscontinueCollectionType
) =>
  app
    ? collection(getFirestore(app), collectionType).withConverter({
        toFirestore: (data: SurveyDiscontinueInfo & { createdAt: Timestamp }) =>
          data,
        fromFirestore: (snap: DocumentSnapshot) =>
          snap.data() as SurveyDiscontinueInfo & { createdAt: Timestamp },
      })
    : undefined;

export const scheduledNotificationsCollection = (
  collectionType: ScheduledNotificationsCollectionType
) =>
  app
    ? collection(getFirestore(app), collectionType).withConverter({
        toFirestore: (data: { referenceId: string }) => data,
        fromFirestore: (snap: DocumentSnapshot) =>
          snap.data() as { referenceId: string },
      })
    : undefined;

export const setConsent = (
  batch: WriteBatch,
  userId: string,
  consent: WithFieldValue<Consent>
) => {
  const collection = consentCollection()!;
  batch.set(doc(collection, userId), consent);
};

export const setUserProgressWithBatch = (
  type: UserProgressCollectionType,
  batch: WriteBatch,
  userId: string,
  progress: WithFieldValue<UserProgress>
) => {
  const collection = userProgressCollection(type)!;
  batch.set(doc(collection, userId), progress);
};

export const updateUserProgressWithBatch = (
  type: UserProgressCollectionType,
  batch: WriteBatch,
  userId: string,
  progress: UpdateData<UserProgress>
) => {
  const collection = userProgressCollection(type)!;
  batch.update(doc(collection, userId), progress);
};

export const updateUserProgress = (
  type: UserProgressCollectionType,
  userId: string,
  progress: UpdateData<UserProgress>
) => {
  const collection = userProgressCollection(type)!;
  return updateDoc(doc(collection, userId), progress);
};

export const updateSurveyProgress = (
  collectionType: BaselineSurveyProgressCollectionType,
  userId: string,
  progress: UpdateData<BaselineSurveyProgress>
) => {
  const collection = baselineSurveyProgressCollection(collectionType)!;
  return updateDoc(doc(collection, userId), progress);
};

export const updateFollowupSurveyProgress = (
  collectionType: FollowupSurveyProgressCollectionType,
  userId: string,
  progress: UpdateData<FollowupSurveyProgress>
) => {
  const collection = followupSurveyProgressCollection(collectionType)!;
  return updateDoc(doc(collection, userId), progress);
};

export const createBatch = () => writeBatch(getFirestore(app!));

export const setBaselineSurveyWithBatch = (
  collectionType: BaselineSurveyCollectionType,
  batch: WriteBatch,
  userId: string,
  responses: BaselineSurveyResponses,
  isCompleted: boolean,
  metadata: SurveyMetadata | undefined
) => {
  const collection = baselineSurveyCollection(collectionType)!;

  if (metadata) {
    return batch.set(doc(collection, userId), {
      ...responses,
      startedAt: metadata.startedAt,
      screeningNumber: metadata.screeningNumber,
      deviceType: metadata.deviceType,
      completedAt: isCompleted ? serverTimestamp() : null,
    });
  }

  return batch.set(doc(collection, userId), {
    ...responses,
    completedAt: isCompleted ? serverTimestamp() : null,
  });
};

export const setFollowupSurveyWithBatch = (
  collectionType: FollowupSurveyCollectionType,
  batch: WriteBatch,
  userId: string,
  responses: FollowupSurveyResponses,
  isCompleted: boolean,
  metadata: SurveyMetadata | undefined,
  referenceId: string
) => {
  const collection = followupSurveyCollection(collectionType)!;

  if (metadata) {
    return batch.set(doc(collection, userId), {
      ...responses,
      startedAt: metadata.startedAt,
      deviceType: metadata.deviceType,
      completedAt: isCompleted ? serverTimestamp() : null,
      referenceId: referenceId,
    });
  }

  return batch.set(doc(collection, userId), {
    ...responses,
    completedAt: isCompleted ? serverTimestamp() : null,
    referenceId: referenceId,
  });
};

export const setBaselineSurveyProgressWithBatch = (
  collectionType: BaselineSurveyProgressCollectionType,
  batch: WriteBatch,
  userId: string,
  progress: WithFieldValue<BaselineSurveyProgress>
) => {
  const collection = baselineSurveyProgressCollection(collectionType)!;
  return batch.set(doc(collection, userId), progress);
};

export const setFollowupSurveyProgressWithBatch = (
  collectionType: FollowupSurveyProgressCollectionType,
  batch: WriteBatch,
  userId: string,
  progress: WithFieldValue<FollowupSurveyProgress>
) => {
  const collection = followupSurveyProgressCollection(collectionType)!;
  return batch.set(doc(collection, userId), { ...progress, type: "followup" });
};

export const setPostInterventionSurveyWithBatch = (
  collectionType: PostInterventionSurveyCollectionType,
  batch: WriteBatch,
  userId: string,
  responses: PostInterventionSurveyResponses
) => {
  const collection = postInterventionSurveyCollection(collectionType)!;
  batch.set(doc(collection, userId), {
    ...responses,
    completedAt: serverTimestamp(),
  });
};

export const setSurveyDiscontinueWithBatch = (
  collectionType: SurveyDiscontinueCollectionType,
  batch: WriteBatch,
  userId: string,
  discontinueInfo: WithFieldValue<SurveyDiscontinueInfo>
) => {
  const collection = surveyDiscontinueCollection(collectionType)!;
  batch.set(doc(collection, userId), {
    ...discontinueInfo,
    createdAt: serverTimestamp(),
  });
};
