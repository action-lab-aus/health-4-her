import { AppEnv } from "@/types";
import { app } from "@/firebase";
import { getFunctions, httpsCallable } from "firebase/functions";
import { Arm } from "@/surveys/types";

const functions = app ? getFunctions(app, "australia-southeast1") : undefined;
export const assignSlot = functions
  ? httpsCallable<{ appEnv: AppEnv }, { randomisationNo: string; arm: Arm }>(
      functions,
      "assignSlot"
    )
  : undefined;

export const getScreeningNumber = functions
  ? httpsCallable<{ client: AppEnv }, string>(functions, "getScreeningNumber")
  : undefined;

export const checkReferenceId = functions
  ? httpsCallable<
      { referenceId: string; appEnv: AppEnv },
      { result: boolean; arm: Arm | null; errMsg: string }
    >(functions, "checkReferenceId")
  : undefined;
