import { ObjectValues } from "@/typeUtils";
import { Timestamp } from "firebase/firestore";
import { baselineSurveyClinical } from "@/components/BaselineSurveyCard";
import {
  followupSurveyIntervention,
  followupSurveyControl,
} from "@/components/FollowupSurveyCard";

type notRenderedCond = readonly [
  sectionKey: string,
  questionKey: string,
  questionValue: string
];

type SurveyElement = {
  key: string;
  type: "element";
  content: React.ReactNode;
  notRenderedConds?: readonly notRenderedCond[];
  freeText?: {
    property: string;
    value: string;
    label: string;
    required?: boolean;
    inputType?: React.HTMLInputTypeAttribute;
    className?: string;
  };
};

type SurveyInput = {
  key: string;
  type: "input";
  label: React.ReactNode;
  placeholder?: string;
  inputType?: React.HTMLInputTypeAttribute;
  inputMode?: React.HTMLAttributes<HTMLInputElement>["inputMode"];
  pattern?: string;
  min?: number | string;
  max?: number | string;
  minlength?: number;
  maxlength?: number;
  required?: boolean;
  className?: string;
  labelClassName?: string;
  notRenderedConds?: readonly notRenderedCond[];
  customErrMsg?: string;
  freeText?: {
    property: string;
    value: string;
    label: string;
    required?: boolean;
    inputType?: React.HTMLInputTypeAttribute;
    className?: string;
  };
};

type SurveyNumberInput = {
  key: string;
  type: "number-input";
  label: React.ReactNode;
  placeholder?: string;
  inputType?: React.HTMLInputTypeAttribute;
  inputMode?: React.HTMLAttributes<HTMLInputElement>["inputMode"];
  pattern?: string;
  min?: number;
  max?: number;
  className?: string;
  labelClassName?: string;
  notRenderedConds?: readonly notRenderedCond[];
  freeText?: {
    property: string;
    value: string;
    label: string;
    required?: boolean;
    inputType?: React.HTMLInputTypeAttribute;
    className?: string;
  };
};

type SurveyDropdown = {
  key: string;
  type: "dropdown";
  label: React.ReactNode;
  options: readonly { label: string; value: string }[];
  freeText?: {
    property: string;
    value: string;
    label: string;
    required?: boolean;
    inputType?: React.HTMLInputTypeAttribute;
    className?: string;
  };
  className?: string;
  wrapperClassName?: string;
  labelClassName?: string;
  notRenderedConds?: readonly notRenderedCond[];
};

type SurveyRadioGroup = {
  key: string;
  type: "radio-group";
  label: React.ReactNode;
  options: readonly { label: string; value: string; labelClassName?: string }[];
  freeText?: {
    property: string;
    value: string;
    label: string;
    required?: boolean;
    inputType?: React.HTMLInputTypeAttribute;
    className?: string;
  };
  className?: string;
  labelClassName?: string;
  notRenderedConds?: readonly notRenderedCond[];
};

type SurveyButtonGroup = {
  key: string;
  type: "button-group";
  label: React.ReactNode;
  options: readonly { label: string; value: string }[];
  freeText?: {
    property: string;
    value: string;
    label: string;
    required?: boolean;
    inputType?: React.HTMLInputTypeAttribute;
    className?: string;
  };
  className?: string;
  labelClassName?: string;
  notRenderedConds?: readonly notRenderedCond[];
};

type SurveyCheckboxGroup = {
  key: string;
  type: "checkbox-group";
  label: React.ReactNode;
  options: readonly {
    label: string;
    value: string;
  }[];
  freeText?: {
    property: string;
    value: string;
    label: string;
    required?: boolean;
    inputType?: React.HTMLInputTypeAttribute;
    className?: string;
  };
  className?: string;
  labelClassName?: string;
  notRenderedConds?: readonly notRenderedCond[];
};

type SurveyImageCounter = {
  key: string;
  type: "image-counter";
  label: React.ReactNode;
  options: readonly { label: string; image: string; value: string }[];
  freeText?: {
    property: string;
    value: string;
    label: string;
    required?: boolean;
    inputType?: React.HTMLInputTypeAttribute;
    className?: string;
  };
  className?: string;
  labelClassName?: string;
  notRenderedConds?: readonly notRenderedCond[];
};

type SurveyDatePicker = {
  key: string;
  type: "datepicker";
  label: React.ReactNode;
  className?: string;
  labelClassName?: string;
  freeText?: {
    property: string;
    value: string;
    label: string;
    required?: boolean;
    inputType?: React.HTMLInputTypeAttribute;
    className?: string;
  };
  notRenderedConds?: readonly notRenderedCond[];
};

type SurveyEntry =
  | SurveyElement
  | SurveyInput
  | SurveyNumberInput
  | SurveyDropdown
  | SurveyRadioGroup
  | SurveyImageCounter
  | SurveyButtonGroup
  | SurveyCheckboxGroup
  | SurveyDatePicker;

export type Survey = {
  [sectionName in string]: {
    className?: string;
    questions: readonly SurveyEntry[];
  };
};

type SurveyInputState<TSurveyEntry extends SurveyEntry> =
  TSurveyEntry extends SurveyInput
    ? { [K in TSurveyEntry["key"]]: string }
    : never;

type SurveyNumberInputState<TSurveyEntry extends SurveyEntry> =
  TSurveyEntry extends SurveyNumberInput
    ? { [K in TSurveyEntry["key"]]: string }
    : never;

type SurveyDropdownState<TSurveyEntry extends SurveyEntry> =
  TSurveyEntry extends SurveyDropdown
    ? "freeText" extends keyof TSurveyEntry
      ? "property" extends keyof TSurveyEntry["freeText"]
        ? TSurveyEntry["freeText"]["property"] extends string
          ? {
              [K in TSurveyEntry["key"]]:
                | TSurveyEntry["options"][number]
                | "Please select";
            } & {
              [K in TSurveyEntry["freeText"]["property"]]?: string;
            }
          : {
              [K in TSurveyEntry["key"]]:
                | TSurveyEntry["options"][number]
                | "Please select";
            }
        : {
            [K in TSurveyEntry["key"]]:
              | TSurveyEntry["options"][number]
              | "Please select";
          }
      : {
          [K in TSurveyEntry["key"]]:
            | TSurveyEntry["options"][number]
            | "Please select";
        }
    : never;

type SurveyRadioGroupState<TSurveyEntry extends SurveyEntry> =
  TSurveyEntry extends SurveyRadioGroup
    ? {
        [K in TSurveyEntry["key"]]:
          | TSurveyEntry["options"][number]["value"]
          | null;
      }
    : never;

type SurveyButtonGroupState<TSurveyEntry extends SurveyEntry> =
  TSurveyEntry extends SurveyButtonGroup
    ? {
        [K in TSurveyEntry["key"]]:
          | TSurveyEntry["options"][number]["value"]
          | null;
      }
    : never;

type SurveyCheckboxGroupState<TSurveyEntry extends SurveyEntry> =
  TSurveyEntry extends SurveyCheckboxGroup
    ? "freeText" extends keyof TSurveyEntry
      ? "property" extends keyof TSurveyEntry["freeText"]
        ? TSurveyEntry["freeText"]["property"] extends string
          ? {
              [K in TSurveyEntry["key"]]:
                | TSurveyEntry["options"][number]["value"][]
                | null;
            } & {
              [K in TSurveyEntry["freeText"]["property"]]?: string;
            }
          : {
              [K in TSurveyEntry["key"]]:
                | TSurveyEntry["options"][number]["value"][]
                | null;
            }
        : {
            [K in TSurveyEntry["key"]]:
              | TSurveyEntry["options"][number]["value"][]
              | null;
          }
      : {
          [K in TSurveyEntry["key"]]:
            | TSurveyEntry["options"][number]["value"][]
            | null;
        }
    : never;

type SurveyImageCounterState<TSurveyEntry extends SurveyEntry> =
  TSurveyEntry extends SurveyImageCounter
    ? {
        [K in TSurveyEntry["key"]]: {
          [Option in TSurveyEntry["options"][number]["value"]]: number;
        };
      }
    : never;

type SurveyDatePickerState<TSurveyEntry extends SurveyEntry> =
  TSurveyEntry extends SurveyDatePicker
    ? { [K in TSurveyEntry["key"]]: string | Date }
    : never;

type TupleIndices<T extends readonly any[]> = Extract<
  keyof T,
  `${number}`
> extends `${infer N extends number}`
  ? N
  : never;

type UnionToIntersection<U> = (U extends any ? (k: U) => void : never) extends (
  k: infer I
) => void
  ? I
  : never;

export type Prettify<T> = { [K in keyof T]: T[K] } & {};

export type SurveyState<TSurvey extends Survey> = {
  [K in keyof TSurvey]: SurveySectionState<TSurvey[K]["questions"]>;
};

type SurveySectionState<TSurvey extends readonly SurveyEntry[]> = Prettify<
  UnionToIntersection<
    ObjectValues<{
      [K in TupleIndices<TSurvey>]: {
        value:
          | SurveyInputState<TSurvey[K]>
          | SurveyNumberInputState<TSurvey[K]>
          | SurveyDropdownState<TSurvey[K]>
          | SurveyRadioGroupState<TSurvey[K]>
          | SurveyButtonGroupState<TSurvey[K]>
          | SurveyCheckboxGroupState<TSurvey[K]>
          | SurveyImageCounterState<TSurvey[K]>
          | SurveyDatePickerState<TSurvey[K]>;
      };
    }>["value"]
  >
>;

type Progress = { startedAt: Timestamp | null; completedAt: Timestamp | null };
export type SurveyProgress = {
  sectionTimestamps: {
    [section: string]: Progress | null;
  };
  page: string | null;
  pageBeforeDiscontinuing?: string | null;
  discontinuedReason?: string;
  contactForFollowUpSurvey?: boolean;
};

export type GenericSurveyProgress<Section extends Survey> = {
  sectionTimestamps: Partial<{ [section in keyof Section]: Progress | null }>;
  page: keyof Section | "discontinued" | null;
  pageBeforeDiscontinuing?: keyof Section | "discontinued" | null;
  discontinueReason?: string;
  contactForFollowUpSurvey?: boolean;
  type?: "baseline" | "followup";
};

export type BaselineSurveyProgress = GenericSurveyProgress<
  typeof baselineSurveyClinical
>;

export type FollowupSurveyProgress = GenericSurveyProgress<
  typeof followupSurveyIntervention
>;

export type SurveyDiscontinueInfo = {
  discontinuedReason: string;
  contactForFollowUpSurvey: boolean;
  baselineSurveyCompleted: boolean;
};

export type SurveyMetadata = {
  startedAt: Timestamp;
  screeningNumber: string;
  deviceType: string;
};

export type Arm = "intervention" | "control";
