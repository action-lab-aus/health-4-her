import { Survey, SurveyState } from "@/surveys/types";

export const getErrors = <
  TSurvey extends Survey,
  Section extends keyof TSurvey
>(
  surveySectionState: SurveyState<TSurvey>[Section],
  surveySection: TSurvey[Section]
) => {
  const errors: Map<string, string> = new Map();

  for (const question of surveySection.questions) {
    if (!(question.key in surveySectionState)) {
      continue;
    }
    if (
      question.type === "radio-group" &&
      !surveySectionState[question.key as keyof typeof surveySectionState]
    ) {
      errors.set(question.key, "Please select an option");
    }
    if (
      question.type === "button-group" &&
      !surveySectionState[question.key as keyof typeof surveySectionState]
    ) {
      errors.set(question.key, "Please select an option");
    }
    if (question.type === "checkbox-group") {
      if (
        !surveySectionState[question.key as keyof typeof surveySectionState] ||
        (
          surveySectionState[
            question.key as keyof typeof surveySectionState
          ] as string[]
        ).length < 1
      ) {
        errors.set(question.key, "Please select an option");
      }
    }
    if (question.type === "image-counter") {
      const selections =
        surveySectionState[question.key as keyof typeof surveySectionState];

      const keysToCheck = ["bhvfreq_alc", "intfreq_alc"];
      const doNotDrink = keysToCheck.some(
        (key) =>
          surveySectionState[key as keyof typeof surveySectionState] === "8"
      );

      if (!doNotDrink) {
        let hasError = true;
        for (const key in selections) {
          if ((selections[key] as number) > 0) {
            hasError = false;
            break;
          }
        }

        if (hasError) {
          errors.set(question.key, "Please specify the drink(s)");
        }
      }
    }
    if (question.type === "dropdown") {
      const selected =
        surveySectionState[question.key as keyof typeof surveySectionState];
      if (!selected || selected === "Please select") {
        errors.set(question.key, "Please select an option");
      }
    }
    if (question.type === "datepicker") {
      const selected =
        surveySectionState[question.key as keyof typeof surveySectionState];
      if (typeof selected !== "string" || selected.length < 1) {
        errors.set(question.key, "Please select a date");
      }
    }
  }

  return errors;
};
