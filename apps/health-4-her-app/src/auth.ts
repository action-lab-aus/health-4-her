import {
  User,
  getAuth,
  onAuthStateChanged,
  signOut,
  signInAnonymously,
  signInWithEmailAndPassword,
} from "firebase/auth";
import { useEffect } from "react";
import toast from "react-hot-toast";
import { app } from "./firebase";

export const useAuthListener = (onAuthChange: (auth: User | null) => void) => {
  useEffect(() => {
    const unsubscribe = onAuthStateChanged(getAuth(app), onAuthChange);
    return unsubscribe;
  }, [onAuthChange]);
};

export const signInWithEmailPassword = async (
  email: string,
  password: string
) => {
  const userCredential = await signInWithEmailAndPassword(
    getAuth(app),
    email,
    password
  ).catch((error) => {
    toast.error(error.message);
  });

  return userCredential?.user;
};

export const signOutAuth = async () => await signOut(getAuth(app));

export const createAnonymousUser = async () =>
  await signInAnonymously(getAuth(app));
