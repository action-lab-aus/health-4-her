import { ReactNode } from "react";
import { Survey } from "@/surveys/types";
import { Timestamp } from "firebase/firestore";

export type PageProgress = { startedAt: Timestamp; completedAt?: Timestamp };

type Page = VideoKeys | ActivityKeys;

export type Consent = {
  surveyAndActivity: boolean;
  followUpSurvey: boolean;
  telephoneInterview: boolean;
  consentedAt: Timestamp | null;
};

export type UserProgress = {
  page: Page | "discontinued" | "done";
  video1: PageProgress;
  video2?: PageProgress;
  video3?: PageProgress;
  video4?: PageProgress;
  video5Control?: PageProgress;
  video5Intervention?: PageProgress;
  activity1?: PageProgress & {
    answer?: string[] | string;
    answered?: Timestamp;
  };
  activity2?: PageProgress & {
    answer?: string[] | string;
    answered?: Timestamp;
  };
  activity3?: PageProgress & {
    answer?: string[] | string;
    answered?: Timestamp;
  };
  activity4?: PageProgress & {
    answer?: string[] | string;
    answered?: Timestamp;
  };
  thankyou?: PageProgress;
  journey: Page[];
  discontinuedReason?: string;
  contactForFollowUpSurvey?: boolean;
};

export type ActivityKeys =
  | "activity1"
  | "activity2"
  | "activity3"
  | "activity4";

export const videos = {
  video1: {
    videoSrc: "./videos/final/H4H_2023_Chunk1_Subs.mp4",
    vimeoSrc: "https://vimeo.com/866646210/136da498d2",
    captionSrc: "",
  },
  video2: {
    videoSrc: "./videos/final/H4H_2023_Chunk2_Subs.mp4",
    vimeoSrc: "https://vimeo.com/866646236/c6179d952d",
    captionSrc: "",
  },
  video3: {
    videoSrc: "./videos/final/H4H_2023_Chunk3a_b_Subs.mp4",
    vimeoSrc: "https://vimeo.com/866646254/a62d0568ff",
    captionSrc: "",
  },
  video4: {
    videoSrc: "./videos/final/H4H_2023_Chunk4_Subs.mp4",
    vimeoSrc: "https://vimeo.com/866646321/94e32cea1a",
    captionSrc: "",
  },
  video5Control: {
    videoSrc: "./videos/final/H4H_2023_Chunk5_Control_Subs.mp4",
    vimeoSrc: "https://vimeo.com/866646340/d3169d4a4e",
    captionSrc: "",
  },
  video5Intervention: {
    videoSrc: "./videos/final/H4H_2023_Chunk5_Intervention_Subs.mp4",
    vimeoSrc: "https://vimeo.com/866646358/b05400e64c",
    captionSrc: "",
  },
} as const;

export type VideoKeys = keyof typeof videos;

export type Feedback = {
  [_ in "correct" | "incorrect"]: {
    text: ReactNode;
    audioSrc: string;
  };
};

export type MultipleChoiceActivityType = {
  key: ActivityKeys;
  type: "multiple-choice";
  question: { text: React.ReactNode; src: string };
  title: string;
  options: { value: string; label: string; correct?: true }[];
  feedback: Feedback;
};

export type SingleChoiceActivityType = {
  key: ActivityKeys;
  type: "single-choice";
  question: { text: React.ReactNode; src: string };
  title: string;
  options: { value: string; label: string; correct?: true }[];
  feedback: {
    [answer in string]: Feedback["correct"];
  };
};

export type ActivityType =
  | MultipleChoiceActivityType
  | SingleChoiceActivityType;

export type ReasonsForDiscontinuing = {
  key: string;
  label: string;
  options: readonly { id: number; label: string }[];
  wrapperClassName?: string;
  labelClassName?: string;
  freeText?: {
    property: string;
    value: string;
    label: string;
    inputType?: React.HTMLInputTypeAttribute;
    className?: string;
  };
};

export enum AppEnv {
  DEV = "dev",
  CLINICAL = "clinical",
  PILOT = "pilot",
}

export enum UserProgressCollectionType {
  DEV = "userProgressDev",
  CLINICAL = "userProgressClinical",
  PILOT = "userProgressPilot",
}

export enum BaselineSurveyCollectionType {
  DEV = "baseline-survey-dev",
  CLINICAL = "baseline-survey-clinical",
  PILOT = "baseline-survey-pilot",
}

export enum BaselineSurveyProgressCollectionType {
  DEV = "baseline-survey-progress-dev",
  CLINICAL = "baseline-survey-progress-clinical",
  PILOT = "baseline-survey-progress-pilot",
}

export enum PostInterventionSurveyCollectionType {
  DEV = "post-intervention-survey-dev",
  CLINICAL = "post-intervention-survey-clinical",
  PILOT = "post-intervention-survey-pilot",
}

export enum SurveyDiscontinueCollectionType {
  DEV = "survey-discontinue-dev",
  CLINICAL = "survey-discontinue-clinical",
  PILOT = "survey-discontinue-pilot",
}

export enum FollowupSurveyCollectionType {
  DEV = "followup-survey-dev",
  CLINICAL = "followup-survey-clinical",
  PILOT = "followup-survey-pilot",
}

export enum FollowupSurveyProgressCollectionType {
  DEV = "followup-survey-progress-dev",
  CLINICAL = "followup-survey-progress-clinical",
  PILOT = "followup-survey-progress-pilot",
}

export enum ScheduledNotificationsCollectionType {
  DEV = "scheduled-notifications-dev",
  CLINICAL = "scheduled-notifications-clinical",
  PILOT = "scheduled-notifications-pilot",
}

type CollectionMappings = {
  UserProgress: UserProgressCollectionType;
  BaselineSurvey: BaselineSurveyCollectionType;
  BaselineSurveyProgress: BaselineSurveyProgressCollectionType;
  PostInterventionSurvey: PostInterventionSurveyCollectionType;
  FollowupSurvey: FollowupSurveyCollectionType;
  FollowupSurveyProgress: FollowupSurveyProgressCollectionType;
  SurveyDiscontinue: SurveyDiscontinueCollectionType;
  ScheduledNotifications: ScheduledNotificationsCollectionType;
};

type EnvToCollectionTypes = {
  [key in AppEnv]: CollectionMappings;
};

export const envToCollectionTypes: EnvToCollectionTypes = {
  dev: {
    UserProgress: UserProgressCollectionType.DEV,
    BaselineSurvey: BaselineSurveyCollectionType.DEV,
    BaselineSurveyProgress: BaselineSurveyProgressCollectionType.DEV,
    PostInterventionSurvey: PostInterventionSurveyCollectionType.DEV,
    FollowupSurvey: FollowupSurveyCollectionType.DEV,
    FollowupSurveyProgress: FollowupSurveyProgressCollectionType.DEV,
    SurveyDiscontinue: SurveyDiscontinueCollectionType.DEV,
    ScheduledNotifications: ScheduledNotificationsCollectionType.DEV,
  },
  clinical: {
    UserProgress: UserProgressCollectionType.CLINICAL,
    BaselineSurvey: BaselineSurveyCollectionType.CLINICAL,
    BaselineSurveyProgress: BaselineSurveyProgressCollectionType.CLINICAL,
    PostInterventionSurvey: PostInterventionSurveyCollectionType.CLINICAL,
    FollowupSurvey: FollowupSurveyCollectionType.CLINICAL,
    FollowupSurveyProgress: FollowupSurveyProgressCollectionType.CLINICAL,
    SurveyDiscontinue: SurveyDiscontinueCollectionType.CLINICAL,
    ScheduledNotifications: ScheduledNotificationsCollectionType.CLINICAL,
  },
  pilot: {
    UserProgress: UserProgressCollectionType.PILOT,
    BaselineSurvey: BaselineSurveyCollectionType.PILOT,
    BaselineSurveyProgress: BaselineSurveyProgressCollectionType.PILOT,
    PostInterventionSurvey: PostInterventionSurveyCollectionType.PILOT,
    FollowupSurvey: FollowupSurveyCollectionType.PILOT,
    FollowupSurveyProgress: FollowupSurveyProgressCollectionType.PILOT,
    SurveyDiscontinue: SurveyDiscontinueCollectionType.PILOT,
    ScheduledNotifications: ScheduledNotificationsCollectionType.PILOT,
  },
};
