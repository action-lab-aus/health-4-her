import { useEffect, useRef, useState } from "react";

export const usePlayPauseState = () => {
  const playerRef = useRef<HTMLAudioElement | HTMLVideoElement>(null);
  const [playing, setPlaying] = useState(false);

  useEffect(() => {
    const handlePause = () => setPlaying(false);
    const handlePlay = () => setPlaying(true);
    const currentRef = playerRef.current;
    currentRef?.addEventListener("play", handlePlay);
    currentRef?.addEventListener("pause", handlePause);
    return () => {
      currentRef?.removeEventListener("play", handlePlay);
      currentRef?.removeEventListener("pause", handlePause);
    };
  }, [playerRef]);

  return {
    playerRef,
    playing,
  };
};
