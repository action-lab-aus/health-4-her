import React, {
  Dispatch,
  Fragment,
  SetStateAction,
  useEffect,
  useState,
} from "react";
import { Modal } from "./Modal";
import { Button } from "./Button";
import {
  AppEnv,
  envToCollectionTypes,
  ReasonsForDiscontinuing,
  UserProgress,
} from "@/types";
import { Select } from "./Select";
import { Input } from "./Input";
import { useQueryClient } from "react-query";
import { useUpdateUserProgress } from "@/mutations/useUpdateUserProgress";
import { invalidateUserProgress } from "@/queries/userProgress";
import { serverTimestamp } from "firebase/firestore";
import { Listbox } from "@headlessui/react";
import { twJoin, twMerge } from "tailwind-merge";
import {
  SurveyDiscontinueInfo,
  BaselineSurveyProgress,
  FollowupSurveyProgress,
} from "@/surveys/types";
import { useUpdateSurveyProgress } from "@/mutations/useUpdateSurveyProgress";
import { invalidateBaselineSurveyProgress } from "@/queries/baselineSurveyProgress";
import { useSurveyDiscontinue } from "@/mutations/useSaveSurveyDiscontinue";

const reasonForDiscontinuing: ReasonsForDiscontinuing = {
  key: "discont",
  label: "Reason for discontinuing",
  options: [
    { id: 1, label: "I don't have time" },
    { id: 2, label: "It takes too long" },
    { id: 3, label: "Hard to use iPad" },
    { id: 4, label: "Not relevant to me" },
    { id: 5, label: "Dislike content" },
    { id: 6, label: "No longer interested in participating" },
    { id: 7, label: "Other" },
  ] as const,
  labelClassName: "font-bold",
  freeText: {
    label: "Please specify",
    property: "discon_oth",
    inputType: "text",
    className: "w-full",
    value: "Other",
  },
};

const contactForFinalSurvey = {
  key: "contactForFinalSurvey",
  label: "Can we contact you in 4-weeks' time to complete the final survey?",
  options: [
    { label: "Yes", value: true },
    { label: "No – I would like to withdraw from the study", value: false },
  ],
};

const isUserProgress = (
  obj:
    | UserProgress
    | BaselineSurveyProgress
    | FollowupSurveyProgress
    | undefined
): obj is UserProgress => {
  return obj !== undefined && "journey" in obj && !("sectionTimestamps" in obj);
};

const isFollowupProgress = (
  obj:
    | UserProgress
    | BaselineSurveyProgress
    | FollowupSurveyProgress
    | undefined
): obj is FollowupSurveyProgress => {
  return obj !== undefined && !("journey" in obj) && obj.type === "followup";
};

export const QuitModal = (props: {
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  progress:
    | BaselineSurveyProgress
    | UserProgress
    | FollowupSurveyProgress
    | undefined;
  userId: string;
  handleSignOut: () => void;
  appEnv: AppEnv;
}) => {
  const isInAboutYouPage = () => {
    return (
      props.progress &&
      !isUserProgress(props.progress) &&
      Object.keys((props.progress as BaselineSurveyProgress).sectionTimestamps)
        .length <= 1
    );
  };
  const [discontinuedReason, setDiscontinuedReason] = useState(
    reasonForDiscontinuing.options[0]?.label
  );
  const [otherValue, setOtherValue] = useState("");
  const [selected, setSelected] = useState(
    contactForFinalSurvey.options[1]?.label
  );

  const queryClient = useQueryClient();
  const updateUserProgressMutation = useUpdateUserProgress({
    onSuccess: () => {
      invalidateUserProgress(
        queryClient,
        props.userId,
        envToCollectionTypes[props.appEnv].UserProgress
      );
    },
  });
  const updateSurveyProgressMutation = useUpdateSurveyProgress({
    onSuccess: () => {
      invalidateBaselineSurveyProgress(
        envToCollectionTypes[props.appEnv].BaselineSurveyProgress,
        queryClient,
        props.userId
      );
    },
  });
  const saveSurveyDiscontinueMutation = useSurveyDiscontinue({
    onSuccess: () => {
      invalidateUserProgress(
        queryClient,
        props.userId,
        envToCollectionTypes[props.appEnv].UserProgress
      );
    },
  });

  const handleFormKeyDown = (e: {
    key: string;
    preventDefault: () => void;
  }) => {
    if (e.key === "Enter") {
      e.preventDefault();
    }
  };

  const handleSubmit: React.FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();

    if (props.progress) {
      const optionIndex = reasonForDiscontinuing.options.findIndex(
        (r) => r.label === discontinuedReason
      );
      const discReason =
        discontinuedReason === reasonForDiscontinuing.freeText?.value
          ? `Other - ${otherValue}`
          : optionIndex !== -1
          ? reasonForDiscontinuing.options[optionIndex]!.id.toString()
          : "";
      const shouldContactAgain = contactForFinalSurvey.options.find(
        (e) => e.label === selected
      )?.value;

      const commonSurveyDiscontinuePayload = {
        discontinuedReason: discReason ?? "",
        contactForFollowUpSurvey: shouldContactAgain ?? false,
        baselineSurveyCompleted: true,
      } satisfies SurveyDiscontinueInfo;

      if (isUserProgress(props.progress)) {
        saveSurveyDiscontinueMutation.mutate({
          collectionType: envToCollectionTypes[props.appEnv].SurveyDiscontinue,
          userId: props.userId,
          discontinueInfo: {
            ...commonSurveyDiscontinuePayload,
            baselineSurveyCompleted: true,
          },
        });

        updateUserProgressMutation.mutate({
          collectionType: envToCollectionTypes[props.appEnv].UserProgress,
          userId: props.userId,
          progress: {
            page: "discontinued",
            [`${props.progress.page}.completedAt`]: serverTimestamp(),
            discontinuedReason: discReason,
            contactForFollowUpSurvey: shouldContactAgain,
          },
        });
      } else if (isFollowupProgress(props.progress)) {
        saveSurveyDiscontinueMutation.mutate({
          collectionType: envToCollectionTypes[props.appEnv].SurveyDiscontinue,
          userId: props.userId,
          discontinueInfo: {
            ...commonSurveyDiscontinuePayload,
            baselineSurveyCompleted: false,
          },
        });

        // TODO: Add update followup survey progress mutation
      } else {
        saveSurveyDiscontinueMutation.mutate({
          collectionType: envToCollectionTypes[props.appEnv].SurveyDiscontinue,
          userId: props.userId,
          discontinueInfo: {
            ...commonSurveyDiscontinuePayload,
            baselineSurveyCompleted: false,
          },
        });

        updateSurveyProgressMutation.mutate({
          collectionType:
            envToCollectionTypes[props.appEnv].BaselineSurveyProgress,
          userId: props.userId,
          progress: {
            page: "discontinued",
            pageBeforeDiscontinuing: props.progress.page,
            discontinueReason: discReason,
            contactForFollowUpSurvey: shouldContactAgain,
          },
        });
      }
    }

    props.handleSignOut();
    props.setOpen(false);
  };

  useEffect(() => {
    if (isUserProgress(props.progress)) {
      setSelected(contactForFinalSurvey.options[0]?.label);
    }
  }, [props.progress]);

  useEffect(() => {
    if (isUserProgress(props.progress)) {
      setSelected(contactForFinalSurvey.options[0]?.label);
    } else {
      setSelected(contactForFinalSurvey.options[1]?.label);
    }
    setDiscontinuedReason(reasonForDiscontinuing.options[0]?.label);
  }, [props.open, props.setOpen, props.progress]);

  return (
    <Modal open={props.open} setOpen={props.setOpen} className="bg-white p-4">
      <Modal.Title className="flex gap-2">
        {!props.progress || isInAboutYouPage()
          ? "Are you sure you want to exit? You still have a few sections left to complete."
          : "We're sorry to see you leave. Please tell us why."}
      </Modal.Title>

      <form onSubmit={handleSubmit} onKeyDown={handleFormKeyDown}>
        {props.progress && !isInAboutYouPage() ? (
          <div key={reasonForDiscontinuing.key} className="mt-10">
            <label
              className={reasonForDiscontinuing.labelClassName}
              htmlFor={reasonForDiscontinuing.key}
            >
              {reasonForDiscontinuing.label}
            </label>
            <Select
              id={reasonForDiscontinuing.key}
              label={discontinuedReason}
              value={discontinuedReason}
              onChange={(e: string) => {
                setDiscontinuedReason(e);
              }}
              wrapperClassName={twMerge(
                reasonForDiscontinuing.wrapperClassName,
                "mb-5"
              )}
              colour="pink"
            >
              {reasonForDiscontinuing.options.map((o) => (
                <Listbox.Option
                  key={o.id}
                  value={o.label}
                  className=" hover:bg-light-pink-500 mx-0 rounded-lg border-b bg-white p-2 hover:cursor-pointer"
                >
                  {o.label}
                </Listbox.Option>
              ))}
            </Select>
            {reasonForDiscontinuing.freeText &&
            discontinuedReason === reasonForDiscontinuing.freeText.value ? (
              <>
                <label htmlFor={reasonForDiscontinuing.freeText.property}>
                  {reasonForDiscontinuing.freeText.label}
                </label>
                <Input
                  id={reasonForDiscontinuing.key}
                  placeholder={reasonForDiscontinuing.freeText.label}
                  type={reasonForDiscontinuing.freeText.inputType || "text"}
                  className={reasonForDiscontinuing.freeText.className}
                  required
                  disabled={false}
                  value={otherValue}
                  onChange={(e) => setOtherValue(e.target.value)}
                  colour="pink"
                />
              </>
            ) : null}
          </div>
        ) : undefined}
        <div className="my-10" />

        {/* Contact for final survey */}
        {props.progress &&
        !isInAboutYouPage() &&
        isUserProgress(props.progress) ? (
          <Fragment key={contactForFinalSurvey.key}>
            <div
              className={twMerge(
                "col-span-full rounded-lg border border-pink-500 p-4"
              )}
            >
              <label
                className={twMerge(
                  "mb-2 flex flex-col",
                  reasonForDiscontinuing.labelClassName
                )}
              >
                {contactForFinalSurvey.label}
              </label>
              {contactForFinalSurvey.options.map((o) => (
                <button
                  type="button"
                  key={o.label}
                  className={twJoin(
                    "flex w-full cursor-pointer items-center gap-2"
                  )}
                  onClick={() => setSelected(o.label)}
                >
                  <input
                    type="radio"
                    value={o.label}
                    id={`${contactForFinalSurvey.key}${o.label}`}
                    className="border-dark-pink-500 before:bg-dark-pink-500 flex h-[24px] w-[24px] shrink-0 cursor-pointer appearance-none items-center justify-center rounded-full border-4 border-solid before:block before:h-[12px] before:w-[12px] before:scale-0 before:rounded-full before:transition-transform before:duration-75 before:ease-in before:content-[''] checked:before:scale-100"
                    checked={o.label === selected}
                    readOnly
                  />
                  <label
                    className={twMerge("mt-0 text-left")}
                    htmlFor={`${contactForFinalSurvey.key}${o.label}`}
                  >
                    {o.label}
                  </label>
                </button>
              ))}
            </div>
          </Fragment>
        ) : undefined}
        <div className="mt-3 flex w-full flex-wrap justify-end gap-3">
          <Button
            type="button"
            variant="outlined"
            colour="black"
            className="w-full sm:w-auto"
            onClick={() => props.setOpen(false)}
          >
            Cancel
          </Button>
          <Button
            type="submit"
            className="w-full sm:w-auto"
            variant="filled"
            colour="dark-pink"
          >
            Confirm
          </Button>
        </div>
      </form>
    </Modal>
  );
};
