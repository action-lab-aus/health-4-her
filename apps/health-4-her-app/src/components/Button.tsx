type Variant = "filled" | "outlined";

import { ColourPrefixesWithVariant, contrastText } from "@/theme";
import { twMerge } from "tailwind-merge";

type ButtonColours =
  | (ColourPrefixesWithVariant<"500"> & ColourPrefixesWithVariant<"600">)
  | undefined;

export const Button = (props: {
  children: React.ReactNode;
  variant: Variant;
  onClick?: React.MouseEventHandler<HTMLButtonElement> | undefined;
  className?: string;
  colour?: ButtonColours;
  type?: React.ButtonHTMLAttributes<HTMLButtonElement>["type"];
  disabled?: boolean;
  unstyled?: boolean;
}) => {
  return (
    <button
      disabled={props.disabled}
      type={props.type}
      className={
        props.unstyled
          ? props.className
          : twMerge(
              "rounded-md px-3 py-2 text-sm font-semibold",
              props.colour
                ? extraStyleForVariant(props.variant, props.colour)
                : "",
              props.className
            )
      }
      onClick={props.onClick}
    >
      {props.children}
    </button>
  );
};

const filledStyleForColour = {
  pink: "bg-pink-500 hover:bg-pink-600",
  black: "bg-black-500 hover:bg-black-600",
  "dark-pink": "bg-dark-pink-500 hover:bg-dark-pink-600",
  "light-pink": "bg-light-pink-500 hover:bg-light-pink-600",
} satisfies Record<Exclude<ButtonColours, undefined>, string>;

const outlinedStyleForColour = {
  pink: "border-pink-500",
  black: "border-black-500",
  "dark-pink": "border-dark-pink-500",
  "light-pink": "border-light-pink-500",
} satisfies Record<Exclude<ButtonColours, undefined>, string>;

const extraStyleForVariant = (variant: Variant, colour: ButtonColours) => {
  switch (variant) {
    case "filled":
      return twMerge(
        "disabled:bg-opacity-25",
        filledStyleForColour[colour ?? "black"],
        contrastText[colour ?? "black"]
      );
    case "outlined":
      return twMerge(
        "border hover:bg-black-500 hover:bg-opacity-10",
        outlinedStyleForColour[colour ?? "black"]
      );
    // Compile-time assertion we never reach here
    default:
      ((_: never) => undefined)(variant);
  }
};
