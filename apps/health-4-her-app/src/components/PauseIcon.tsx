export const PauseIcon = (props: { className?: string }) => {
  return (
    <svg
      className={props.className}
      fill="currentColor"
      height="100%"
      width="100%"
      version="1.1"
      id="Capa_1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 210 210"
      xmlSpace="preserve"
    >
      <g id="SVGRepo_bgCarrier" strokeWidth="0"></g>
      <g
        id="SVGRepo_tracerCarrier"
        strokeLinecap="round"
        strokeLinejoin="round"
      ></g>
      <g id="SVGRepo_iconCarrier">
        <rect x="60" y="30" width="30" height="150"></rect>
        <rect x="120" y="30" width="30" height="150"></rect>
      </g>
    </svg>
  );
};
