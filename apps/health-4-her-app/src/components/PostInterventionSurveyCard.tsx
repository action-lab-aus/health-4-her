import { Card } from "@/components/Card";
import { SurveyForm } from "./SurveyForm";
import { Survey, SurveyState } from "@/surveys/types";
import { invalidateUserProgress } from "@/queries/userProgress";
import { useQueryClient } from "react-query";
import { useSavePostInterventionSurvey } from "@/mutations/useSavePostInterventionSurvey";
import { AppEnv, envToCollectionTypes } from "@/types";

export const postInterventionSurvey = {
  prelude: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-2xl font-bold">
                Just before you go we’d like to ask you some final questions
                about how you feel after participating in the Health4Her
                activities.
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  outcomes: {
    className: "grid gap-3 w-full",
    questions: [
      {
        key: "increaseExercise",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to increase the amount of exercise you do?
          </span>
        ),
        labelClassName: "mt-4",
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "increaseSunscreen",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to increase your sun protection practices?
          </span>
        ),

        labelClassName: "mt-4",
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "reduceAlcohol",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to reduce the amount of alcohol you consume?
          </span>
        ),
        labelClassName: "mt-4",
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "reduceTobaccoProducts",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to reduce the amount of tobacco or e-cigarette products you use?
          </span>
        ),
        labelClassName: "mt-4",
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "increaseVegetables",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to increase the amount of vegetables you consume?
          </span>
        ),
        labelClassName: "mt-4",
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
    ] as const,
  },
} satisfies Survey;

const postInterventionSurveyDefaultState: SurveyState<
  typeof postInterventionSurvey
> = {
  prelude: {},
  outcomes: {
    increaseExercise: null,
    increaseSunscreen: null,
    reduceTobaccoProducts: null,
    reduceAlcohol: null,
    increaseVegetables: null,
  },
};

export const PostInterventionSurveyCard = (props: {
  appEnv: AppEnv;
  userId: string;
}) => {
  const queryClient = useQueryClient();
  const saveBaselineMutation = useSavePostInterventionSurvey({
    onSuccess: () =>
      invalidateUserProgress(
        queryClient,
        props.userId,
        envToCollectionTypes[props.appEnv].UserProgress
      ),
  });
  return (
    <Card className="max-w-6xl">
      <SurveyForm
        survey={postInterventionSurvey}
        defaultState={postInterventionSurveyDefaultState}
        defaultProgress={undefined}
        saving={saveBaselineMutation.isLoading}
        onComplete={(s) =>
          saveBaselineMutation.mutate({
            postInterventionSurveyCollectionType:
              envToCollectionTypes[props.appEnv].PostInterventionSurvey,
            userProgressCollectionType:
              envToCollectionTypes[props.appEnv].UserProgress,
            userId: props.userId,
            responses: s,
          })
        }
      />
    </Card>
  );
};
