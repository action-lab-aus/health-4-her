import {
  ActivityType,
  Feedback,
  MultipleChoiceActivityType,
  SingleChoiceActivityType,
  UserProgress,
} from "@/types";
import { UpdateData, serverTimestamp } from "firebase/firestore";
import { useState } from "react";
import { Button } from "./Button";
import { Audio } from "./Audio";
import { twJoin, twMerge } from "tailwind-merge";
import Pause from "./icons/Pause";
import Play from "./icons/Play";
import PlayCircle from "./icons/PlayCircle";
export const Activity = <TActivity extends ActivityType>(props: {
  activity: TActivity;
  showCorrectAnswers: boolean;
  activityProgress: UserProgress[TActivity["key"]];
  userId: string;
  saveProgress: (progress: UpdateData<UserProgress>) => void;
  goToNextPage: () => void;
  progressSaving: boolean;
  className?: string;
}) => {
  const handleSaveActivity = (answer: string | string[]) => {
    if (props.activityProgress?.answer) {
      props.goToNextPage();
    } else {
      props.saveProgress({
        [`${props.activity.key}.answeredAt`]: serverTimestamp(),
        [`${props.activity.key}.answer`]: answer,
      });
    }
  };

  return (
    <div className={props.className}>
      {props.activity.type === "multiple-choice" &&
      (props.activityProgress?.answer === undefined ||
        Array.isArray(props.activityProgress.answer)) ? (
        <MultipleChoiceActivity
          activity={props.activity}
          showCorrectAnswers={props.showCorrectAnswers}
          onSave={handleSaveActivity}
          saving={props.progressSaving}
          savedAnswer={props.activityProgress?.answer}
        />
      ) : null}
      {props.activity.type === "single-choice" &&
      (props.activityProgress?.answer === undefined ||
        typeof props.activityProgress.answer === "string") ? (
        <SingleChoiceActivity
          activity={props.activity}
          showCorrectAnswers={props.showCorrectAnswers}
          onSave={handleSaveActivity}
          saving={props.progressSaving}
          savedAnswer={props.activityProgress?.answer}
        />
      ) : null}
    </div>
  );
};

const MultipleChoiceActivity = (props: {
  activity: MultipleChoiceActivityType;
  showCorrectAnswers: boolean;
  onSave: (answer: string[]) => void;
  savedAnswer: string[] | undefined;
  saving: boolean;
}) => {
  const [selected, setSelected] = useState<Set<string>>(new Set());
  const answerIsCorrect =
    props.savedAnswer?.every(
      (a) => props.activity.options.find((o) => o.value === a)?.correct
    ) &&
    props.savedAnswer.length ===
      props.activity.options.filter((o) => o.correct).length;
  const handleOptionClick = (option: string) => {
    setSelected((s) => {
      const newValue = new Set(s);
      if (newValue.has(option)) {
        newValue.delete(option);
      } else {
        newValue.add(option);
      }
      return newValue;
    });
  };
  return (
    <ButtonGroupActivity
      activity={props.activity}
      showCorrectAnswers={props.showCorrectAnswers}
      onSave={() => props.onSave(Array.from(selected.values()))}
      feedback={
        props.savedAnswer
          ? props.activity.feedback[answerIsCorrect ? "correct" : "incorrect"]
          : undefined
      }
      savedAnswer={props.savedAnswer}
      selected={selected}
      saving={props.saving}
      handleOptionClick={handleOptionClick}
    >
      <span className="mt-4 font-bold">Select all that apply:</span>
    </ButtonGroupActivity>
  );
};

const SingleChoiceActivity = (props: {
  activity: SingleChoiceActivityType;
  showCorrectAnswers: boolean;
  onSave: (answer: string) => void;
  savedAnswer: string | undefined;
  saving: boolean;
}) => {
  const [selected, setSelected] = useState<string | undefined>(undefined);

  const handleOptionClick = (value: string) => setSelected(value);

  const selectedOrSavedAnswer = selected || props.savedAnswer;

  const feedback = props.savedAnswer
    ? props.activity.feedback[props.savedAnswer]
    : undefined;

  return (
    <ButtonGroupActivity
      activity={props.activity}
      showCorrectAnswers={props.showCorrectAnswers}
      savedAnswer={props.savedAnswer ? [props.savedAnswer] : undefined}
      saving={props.saving}
      selected={selected ? new Set([selected]) : new Set()}
      feedback={feedback}
      onSave={() =>
        selectedOrSavedAnswer ? props.onSave(selectedOrSavedAnswer) : null
      }
      handleOptionClick={handleOptionClick}
    >
      <span className="mt-4 font-bold">Select one option:</span>
    </ButtonGroupActivity>
  );
};

const ButtonGroupActivity = (props: {
  activity: ActivityType;
  showCorrectAnswers: boolean;
  savedAnswer: string[] | undefined;
  saving: boolean;
  selected: Set<string>;
  feedback: undefined | Feedback["correct"];
  onSave: () => void;
  handleOptionClick: (value: string) => void;
  children?: React.ReactNode;
}) => {
  const savedAnswer = new Set(props.savedAnswer);
  const [error, setError] = useState(" ");
  const handleSaveClick = () => {
    if (props.selected.size || props.savedAnswer?.length) {
      props.onSave();
      setError(" ");
    } else {
      setError("Please select an answer");
    }
  };
  return (
    <div className="flex flex-col">
      <span className="mt-2 flex items-center justify-between whitespace-pre-line">
        <div>{props.activity.question.text}</div>
        <Audio
          src={props.activity.question.src}
          replay={
            <Play className="h-7 w-7 text-pink-500 transition-transform ease-in-out hover:scale-110 hover:duration-200" />
          }
          paused={
            <Play className="h-7 w-7 text-pink-500 transition-transform ease-in-out hover:scale-110 hover:duration-200" />
          }
          playing={
            <Pause className="h-7 w-7 text-pink-500 transition-transform ease-in-out hover:scale-110 hover:duration-200" />
          }
        />
      </span>
      <span className="mt-4">{props.children}</span>
      <div className="mt-2 flex flex-col gap-1 lg:grid lg:grid-cols-2">
        {props.activity.options.map((o) => {
          return (
            <Button
              disabled={props.saving || Boolean(props.savedAnswer?.length)}
              key={o.value}
              onClick={() => props.handleOptionClick(o.value)}
              variant="filled"
              colour={undefined}
              className={twJoin(
                "text-left font-normal",
                (o.correct &&
                  props.savedAnswer?.length &&
                  props.showCorrectAnswers) ||
                  savedAnswer?.has(o.value)
                  ? "text-black-500 border-4 border-solid border-green-600"
                  : undefined,
                savedAnswer?.has(o.value) && !o.correct
                  ? "text-black-500 border-4 border-solid border-red-600 border-opacity-30"
                  : undefined
              )}
            >
              <span className="flex w-full items-center justify-start gap-3">
                <input
                  readOnly
                  name={o.value}
                  checked={props.selected.has(o.value)}
                  type={
                    props.activity.type === "single-choice"
                      ? "radio"
                      : "checkbox"
                  }
                  className={twMerge(
                    "border-dark-pink-500 before:bg-dark-pink-500 flex h-[24px] w-[24px] shrink-0 cursor-pointer appearance-none items-center justify-center border-4 border-solid before:block before:scale-0 before:transition-transform before:duration-100 before:ease-out checked:before:scale-100",
                    props.activity.type === "single-choice"
                      ? "rounded-full before:h-[12px] before:w-[12px] before:rounded-full"
                      : "before:h-0 before:w-0 before:-translate-x-2 before:-translate-y-4 before:content-['✓']"
                  )}
                />
                <span>{o.label}</span>
              </span>
            </Button>
          );
        })}
      </div>
      {props.feedback ? (
        <div className="mt-6">
          <span className="mt-2 flex flex-col items-center justify-between whitespace-pre-line sm:flex-row">
            <div>{props.feedback.text}</div>
            <Audio
              src={props.feedback.audioSrc}
              replay={
                <Play className="h-7 w-7 text-pink-500 transition-transform ease-in-out hover:scale-110 hover:duration-200" />
              }
              paused={
                <Play className="h-7 w-7 text-pink-500 transition-transform ease-in-out hover:scale-110 hover:duration-200" />
              }
              playing={
                <Pause className="h-9 w-9 text-pink-500 transition-transform ease-in-out hover:scale-110 hover:duration-200 sm:h-7 sm:w-7" />
              }
            />
          </span>
        </div>
      ) : null}
      <div className="text-error">{error}</div>
      <Button
        disabled={props.saving}
        onClick={handleSaveClick}
        colour="pink"
        variant="filled"
        className="mt-7"
      >
        {props.savedAnswer?.length ? "Next" : "Save"}
      </Button>
    </div>
  );
};
