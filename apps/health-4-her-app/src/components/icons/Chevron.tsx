import { twJoin } from "tailwind-merge";

export const Chevron = (props: {
  direction: "up" | "down";
  className?: string;
}) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 -960 960 960"
      className={twJoin(
        props.direction === "up" ? "rotate-180" : undefined,
        props.className
      )}
      fill="currentColor"
    >
      <path d="M480-354.362q-7.464 0-14.167-2.685-6.703-2.685-12.594-8.576L260.145-558.717q-11.022-11.022-11.022-26.522 0-15.5 11.022-26.522 11.021-11.022 26.855-11.022 15.833 0 26.855 11.022L480-445.616l166.145-166.145q11.022-11.022 26.522-11.022 15.5 0 26.522 11.022 11.021 11.022 11.021 26.855t-11.021 26.855L506.761-365.623q-5.891 5.891-12.594 8.576-6.703 2.685-14.167 2.685Z" />
    </svg>
    // <svg
    //   className={twJoin(
    //     props.direction === "down" ? "rotate-180" : undefined,
    //     props.className
    //   )}
    //   fill="currentColor"
    //   xmlns="http://www.w3.org/2000/svg"
    //   viewBox="0 0 512 512"
    // >
    //   <path d="M233.4 105.4c12.5-12.5 32.8-12.5 45.3 0l192 192c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L256 173.3 86.6 342.6c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3l192-192z" />

    //   <path d="M480-354.362q-7.464 0-14.167-2.685-6.703-2.685-12.594-8.576L260.145-558.717q-11.022-11.022-11.022-26.522 0-15.5 11.022-26.522 11.021-11.022 26.855-11.022 15.833 0 26.855 11.022L480-445.616l166.145-166.145q11.022-11.022 26.522-11.022 15.5 0 26.522 11.022 11.021 11.022 11.021 26.855t-11.021 26.855L506.761-365.623q-5.891 5.891-12.594 8.576-6.703 2.685-14.167 2.685Z" />
    // </svg>
  );
};
