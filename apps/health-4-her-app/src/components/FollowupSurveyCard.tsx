import { useEffect } from "react";
import { Card } from "@/components/Card";
import {
  Survey,
  SurveyState,
  SurveyMetadata,
  FollowupSurveyProgress,
  Arm,
} from "@/surveys/types";
import { UseMutationResult, useQueryClient } from "react-query";
import { AppEnv, envToCollectionTypes } from "@/types";
import { Timestamp } from "firebase/firestore";
import { FollowupSurveyResponses } from "@/services/firestore";
import { useSaveFollowup } from "@/mutations/useSaveFollowup";
import { invalidateFollowupSurveyProgress } from "@/queries/followupSurveyProgress";
import { FollowupSurveyForm } from "./FollowupSurveyForm";
import Image from "next/image";

const today = new Date();
const minYears = 40;
const maxYears = 110;

const messagingImages = [
  {
    title: "Messaging 1",
    text: "Overwhelming evidence shows that drinking alcohol increases risk of breast cancer, even in very low amounts.",
    src: "/images/messaging/Message1image.png",
  },
  {
    title: "Messaging 2",
    text: "In Australia, 1 in 15 breast cancer cases, and 1 in 5 breast cancer deaths, are due to alcohol consumption.",
    src: "/images/messaging/Message2image.png",
  },
  {
    title: "Messaging 3",
    text: "Remember, every drink you don't have will benefit your health.",
    src: "/images/messaging/Message3image.png",
  },
  {
    title: "Messaging 4",
    text: "It can be helpful to start with a few small positive steps in your journey to improve your health",
    src: "/images/messaging/Message4image.png",
  },
  {
    title: "Messaging 5",
    text: "If there's one message we want you to take away... it's that drinking less is best for your breasts.",
    src: "/images/messaging/Message5image.png",
  },
];

function getPageCountForProgramEvaluationSection(
  state: FollowupSurveyResponses | undefined
): number {
  if (state === undefined) return 0;
  if ("preludeToOverallMessaging" in state) {
    return 8;
  }
  return 5;
}

export const followupSurveyIntervention = {
  aboutYou: {
    className: "grid grid-cols-4 gap-5",
    questions: [
      {
        key: "intro",
        type: "element",
        content: (
          <div className="col-span-4 mb-5">
            <legend className="mb-5 text-xl font-semibold">About you</legend>
            <span>
              <strong>
                In this section, please provide your name and date of birth.
              </strong>
            </span>
            <p>
              Remember, your responses to this survey are{" "}
              <strong>strictly confidential</strong>. Maroondah BreastScreen and
              Eastern Health will not have access to any information you
              provide.
            </p>
          </div>
        ),
      },
      {
        key: "first_name",
        type: "input",
        label: "First name *",
        className: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
      },
      {
        key: "last_name",
        type: "input",
        label: "Last name *",
        className: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
      },
      {
        key: "dob",
        type: "datepicker",
        className: "col-span-full sm:col-span-3 appearance-none",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
        label: "Date of birth *",
      },
    ] as const,
  },
  preludeToPastMonthBehaviour: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We would now like to understand your lifestyle{" "}
                <span className="text-lg font-bold italic md:text-2xl">
                  behaviours
                </span>{" "}
                in the
                {"  "}
                <span className="text-lg font-bold underline md:text-2xl">
                  past month
                </span>
                .
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  pastMonthBehaviour: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "bhvfreq_phys",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>
              In the <strong>past month</strong>, how often did you engage in
              physical activity? *
            </p>
            <p>
              (For example, brisk walking, jogging, swimming, tennis, gardening
              or work around the house, riding a bicycle)
            </p>
          </div>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Occasionally, but not in the past month",
            value: "7",
          },
          { label: "Do not / cannot exercise", value: "8" },
        ] as const,
      },
      {
        key: "bhvquant_phys_mins",
        type: "input",
        label: (
          <span>
            In the <strong>past month</strong>, on a <em> typical</em> day that
            you engaged in physical activity, how many <u>minutes</u> did you
            do? *
          </span>
        ),
        placeholder: "Please type how many minutes",
        inputType: "text",
        className: "col-span-full sm:col-span-2",
        labelClassName:
          "flex items-center col-span-full sm:col-span-full mt-2 sm:mt-10",
        notRenderedConds: [["pastMonthBehaviour", "bhvfreq_phys", "8"]],
      },
      {
        key: "bhvfreq_sun",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            <p>
              In the <strong>past month</strong>, how often did you engage in
              sun protection behaviours? *
            </p>
            <p>
              (For example, wearing sunscreen, a hat, or other sun-protective
              clothing)
            </p>
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Less often",
            value: "7",
          },
        ] as const,
      },
    ] as const,
  },
  pastMonthAlcohol: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "bhvfreq_alc",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            In the <strong>past month</strong>, how often did you have an
            alcoholic drink of any kind? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "About 1 day per month",
            value: "6",
          },
          {
            label: "Occasionally, but not in the past month",
            value: "7",
          },
          {
            label: "Do not drink / no longer drink",
            value: "8",
          },
        ] as const,
      },
      {
        key: "bhvDrinkOnDay",
        type: "image-counter",
        notRenderedConds: [["pastMonthAlcohol", "bhvfreq_alc", "8"]],
        label: (
          <span>
            In the <strong>past month</strong>, on a <em>typical</em> day that
            you had an alcoholic drink, how many drinks did you have? *
          </span>
        ),
        options: [
          {
            value: "bhvquant_avred",
            image: "/images/drinks/RedWine_WideBig.png",
            label: "Average serve of red wine (150ml)",
          },
          {
            value: "bhvquant_avwhite",
            image: "/images/drinks/WhiteWine_WideBig.png",
            label: "Average serve of white wine (150ml)",
          },
          {
            value: "bhvquant_avsparkle",
            image: "/images/drinks/Champagne1.png",
            label: "Average serve of sparkling wine (150ml)",
          },
          {
            value: "bhvquant_smlred",
            image: "/images/drinks/RedWine_NarrowSmall.png",
            label: "Small serve of red wine (100ml)",
          },
          {
            value: "bhvquant_smlwhite",
            image: "/images/drinks/WhiteWine_NarrowSmall.png",
            label: "Small serve of white wine (100ml)",
          },
          {
            value: "bhvquant_shot",
            image: "/images/drinks/Scotch.png",
            label: "Shot/nip spirit (30ml) – with or without mixer",
          },
          {
            value: "bhvquant_cocktail",
            image: "/images/drinks/Cocktail1.png",
            label: "Cocktail",
          },
          {
            value: "bhvquant_rtd",
            image: "/images/drinks/Scotch.png",
            label: "Ready-to-drink spirit (275ml)",
          },
          {
            value: "bhvquant_beer",
            image: "/images/drinks/BeerPot.png",
            label: "Beer or cider (375ml)",
          },
        ] as const,
      },
    ] as const,
  },
  pastMonthSecondaryBehaviour: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "bhvfreq_cig",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            In the <strong>past month</strong>, have you used any tobacco or
            e-cigarette products? *
          </span>
        ),
        options: [
          {
            label: "No",
            value: "0",
          },
          {
            label: "Yes, regularly (at least once per day)",
            value: "1",
          },
          {
            label: "Yes, not regularly (less than once per day)",
            value: "2",
          },
        ] as const,
      },
      {
        key: "bhvfreq_veg",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            In the <strong>past month</strong>, how often did you include
            vegetables as part of a meal? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "Less often",
            value: "6",
          },
        ] as const,
      },
    ] as const,
  },
  preludeToOutcomes: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We would now like to understand your behaviour-related{" "}
                <span className="text-lg font-bold italic md:text-2xl">
                  intentions
                </span>{" "}
                over the{" "}
                <span className="text-lg font-bold underline md:text-2xl">
                  next month
                </span>
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  outcomes: {
    className: "grid gap-5 w-full mt-5",
    questions: [
      {
        key: "int_phys",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to increase the amount of exercise you do? *
          </span>
        ),
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_sun",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to increase your sun protection practices? *
          </span>
        ),

        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_alc",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to reduce the amount of alcohol you consume? *
          </span>
        ),
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_cig",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to reduce any tobacco or e-cigarette use? *
          </span>
        ),
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_veg",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to increase the amount of vegetables you consume? *
          </span>
        ),

        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
    ] as const,
  },
  preludeToSecondaryOutcomes: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We will now ask you some further questions about your
                behaviour-related{" "}
                <span className="text-lg font-bold italic md:text-2xl">
                  intentions
                </span>{" "}
                over the{" "}
                <span className="text-lg font-bold underline md:text-2xl">
                  next month
                </span>
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  secondaryOutcomes: {
    questions: [
      {
        key: "intfreq_phys",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>
              Over the <strong>next month</strong>, how often do you intend to
              engage in physical activity? *
            </p>
            <p>
              (For example, brisk walking, jogging, swimming, tennis, gardening
              or work around the house, riding a bicycle)
            </p>
          </div>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Occasionally, but not in the next month",
            value: "7",
          },
          { label: "Do not / cannot exercise", value: "8" },
        ] as const,
      },
      {
        key: "intquant_phys_mins",
        type: "input",
        label: (
          <span>
            Over the <strong>next month</strong>, on a <em>typical</em> day that
            you engage in physical activity, how many <u>minutes</u> do you
            intend to do? *
          </span>
        ),
        placeholder: "Please type how many minutes",
        inputType: "text",
        className: "col-span-full sm:col-span-2",
        labelClassName: "flex items-center col-span-full sm:col-span-full",
        notRenderedConds: [["secondaryOutcomes", "intfreq_phys", "8"]],
      },
      {
        key: "intfreq_sun",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            <p>
              Over the <strong>next month</strong>, how often do you intend to
              engage in sun protection behaviours? *
            </p>
            <p>
              (For example, wearing sunscreen, a hat, or other sun-protective
              clothing)
            </p>
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Less often",
            value: "7",
          },
        ] as const,
      },
    ] as const,
  },
  secondaryOutcomesPart2: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "intfreq_alc",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            Over the <strong>next month</strong>, how often do you intend to
            have an alcoholic drink of any kind? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "About 1 day per month",
            value: "6",
          },
          {
            label: "Occasionally, but not in the next month",
            value: "7",
          },
          {
            label: "Do not drink / no longer drink",
            value: "8",
          },
        ] as const,
      },
      {
        key: "intDrinkOnDay",
        type: "image-counter",
        notRenderedConds: [
          ["secondaryOutcomesPart2", "intfreq_alc", "8"],
        ] as const,
        label: (
          <span>
            Over the <strong>next month</strong>, on a <em>typical </em> day
            that you have an alcoholic drink, how many drinks do you intend to
            have? *
          </span>
        ),
        options: [
          {
            value: "intquant_avred",
            image: "/images/drinks/RedWine_WideBig.png",
            label: "Average serve of red wine (150ml)",
          },
          {
            value: "intquant_avwhite",
            image: "/images/drinks/WhiteWine_WideBig.png",
            label: "Average serve of white wine (150ml)",
          },
          {
            value: "intquant_avsparkle",
            image: "/images/drinks/Champagne1.png",
            label: "Average serve of sparkling wine (150ml)",
          },
          {
            value: "intquant_smlred",
            image: "/images/drinks/RedWine_NarrowSmall.png",
            label: "Small serve of red wine (100ml)",
          },
          {
            value: "intquant_smlwhite",
            image: "/images/drinks/WhiteWine_NarrowSmall.png",
            label: "Small serve of white wine (100ml)",
          },
          {
            value: "intquant_shot",
            image: "/images/drinks/Scotch.png",
            label: "Shot/nip spirit (30ml) – with or without mixer",
          },
          {
            value: "intquant_cocktail",
            image: "/images/drinks/Cocktail1.png",
            label: "Cocktail",
          },
          {
            value: "intquant_rtd",
            image: "/images/drinks/pre-mix.png",
            label: "Ready-to-drink spirit (275ml)",
          },
          {
            value: "intquant_beer",
            image: "/images/drinks/BeerPot.png",
            label: "Beer or cider (375ml)",
          },
        ] as const,
      },
    ] as const,
  },
  secondaryOutcomesPart3: {
    questions: [
      {
        key: "intfreq_cig",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            Over the <strong>next month</strong>, do you intend to use any{" "}
            tobacco or e-cigarette products ? *
          </span>
        ),
        options: [
          {
            label: "No",
            value: "0",
          },
          {
            label: "Yes, regularly (at least once per day)",
            value: "1",
          },
          {
            label: "Yes, not regularly (less than once per day)",
            value: "2",
          },
        ] as const,
      },
      {
        key: "intfreq_veg",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            Over the <strong>next month</strong>, how often do you intend to
            include vegetables as part of a meal? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "Less often",
            value: "6",
          },
        ] as const,
      },
    ] as const,
  },
  preludeToKnowledge: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We would now like to understand your current knowledge of breast
                cancer risk factors. It&apos;s okay to be uncertain, just select
                &quot;unsure&quot;.
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  riskKnowledge: {
    className: "grid lg:grid-cols-6 gap-3 w-full lg:max-w-6xl",
    questions: [
      {
        key: "secondaryOutcomesPart3Intro",
        type: "element",
        content: (
          <>
            <div className="col-span-full">
              <div className="space-y-3">
                <p>
                  To what extent do you consider the following factors to be
                  associated with an increased risk of breast cancer?
                </p>
                <div className="flex flex-col gap-3 rounded-lg border p-2 lg:flex-row">
                  <div className="mb-2 grid grid-flow-col-dense p-2">
                    <p className="pl-3">
                      <span className="text-red-600">
                        <strong>Clear risk factor</strong>
                      </span>
                      : there is strong, consistent evidence that this factor
                      increases risk of breast cancer.
                    </p>
                  </div>
                  <div className="grid grid-flow-col-dense p-2">
                    <p className="pl-3">
                      <span className="text-light-pink-700">
                        <strong>Possible risk factor</strong>
                      </span>
                      : there is some evidence that this factor increases risk
                      of breast cancer, but not enough to be certain.
                    </p>
                  </div>
                  <div className="grid grid-flow-col-dense p-2">
                    <p className="pl-3">
                      <span>
                        <strong className="text-green-600">
                          Not a proven risk factor
                        </strong>
                      </span>
                      : the evidence is too limited to determine whether this
                      factor increases risk of breast cancer.
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="hidden lg:col-span-2 lg:block" />
            <div className="hidden text-center text-red-600 before:-translate-x-1 lg:flex lg:items-center lg:justify-center">
              Clear risk factor
            </div>
            <div className="text-light-pink-700 hidden text-center lg:flex lg:items-center lg:justify-center">
              Possible risk factor
            </div>
            <div className="hidden text-left text-green-600  lg:flex lg:items-center lg:justify-center">
              Not a proven risk factor
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Unsure
            </div>
          </>
        ),
      },
      {
        key: "bcrisk_famhist",
        label: "Family history of breast cancer *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_phys",
        label: "Physical inactivity *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_sunexpo",
        label: "Sun exposure *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_alc",
        label: "Drinking alcohol *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_cig",
        label: "Using tobacco products *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_ovweight",
        label: "Being overweight *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
    ] as const,
  },
  knowledge: {
    className: "grid lg:grid-cols-6 gap-3 w-full lg:max-w-6xl",
    questions: [
      {
        key: "knowl_phys",
        type: "button-group",
        label:
          "International guidelines recommend a minimum of _____ minutes of physical activity, five days a week. *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "30 minutes",
            value: "1",
          },
          {
            label: "45 minutes",
            value: "2",
          },
          {
            label: "60 minutes",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_sunscr",
        type: "button-group",
        label:
          "Sunscreen needs to be applied how many minutes before going outdoors? *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "20 minutes",
            value: "1",
          },
          {
            label: "30 minutes",
            value: "2",
          },
          {
            label: "60 minutes",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_sd",
        type: "button-group",
        label:
          "Drinking one average restaurant serve of wine a day increases breast cancer risk by... *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "4%",
            value: "1",
          },
          {
            label: "23%",
            value: "2",
          },
          {
            label: "61%",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_redwine",
        type: "button-group",
        label: "An average restaurant serve of red wine contains... *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "Less than 1 standard drink",
            value: "1",
          },
          {
            label: "1 standard drink",
            value: "2",
          },
          {
            label: "Closer to 2 standard drinks",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_alcguideline",
        type: "button-group",
        label:
          "Australian guidelines recommend that women should drink no more than _____ standard drinks per week *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "12 standard drinks",
            value: "1",
          },
          {
            label: "10 standard drinks",
            value: "2",
          },
          {
            label: "7 standard drinks",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_diet",
        type: "button-group",
        label:
          "Dietary guidelines advise that women consume more of which food group as they age?",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "Dairy",
            value: "1",
          },
          {
            label: "Vegetables",
            value: "2",
          },
          {
            label: "Protein",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
    ] as const,
  },
  programEval: {
    className: "grid lg:grid-cols-7 gap-3 w-full lg:max-w-6xl",
    questions: [
      {
        key: "program_eval_intro",
        type: "element",
        content: (
          <>
            <div className="col-span-full">
              <div className="space-y-3">
                <p>
                  <strong>
                    We&apos;d now like to ask you a few final questions about
                    your experience of Health4Her
                  </strong>
                </p>
                <p>
                  When thinking about your experience of participating in
                  Health4Her, to what extent do you AGREE or DISAGREE with the
                  following statements?
                </p>
              </div>
            </div>
            <div className="hidden lg:col-span-2 lg:block" />
            <div className="hidden text-center before:-translate-x-1 lg:flex lg:items-center lg:justify-center">
              Strongly agree
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Agree
            </div>
            <div className="hidden text-left lg:flex lg:items-center lg:justify-center">
              Neither agree nor disagree
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Disagree
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Strongly disagree
            </div>
          </>
        ),
      },
      {
        key: "h4h_infoaccept",
        label:
          "Being provided health promotion information by BreastScreen was acceptable to me. *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4h_stdservice",
        label:
          "Health4Her should be offered as part of the standard service offered by BreastScreen. *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4h_easyuse",
        label: "The Health4Her website was easy to use. *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4h_timeaccept",
        label:
          "Taking approximately 15 minutes following my BreastScreen appointment to receive some breast cancer risk reduction information was acceptable to me. *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4h_animaccept",
        label:
          "The use of animation was an acceptable way to inform women about breast cancer risks. *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
    ] as const,
  },
  programEvalSharing: {
    questions: [
      {
        key: "infoshare",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>
              Did you tell anyone about any of the information provided in
              Health4Her? *
            </p>
          </div>
        ),
        options: [
          {
            label: "Yes",
            value: "1",
          },
          {
            label: "No",
            value: "0",
          },
        ] as const,
      },
      {
        key: "infoshare_who",
        type: "checkbox-group",
        className: "border border-4 rounded-lg border-pink-600",
        notRenderedConds: [["programEvalSharing", "infoshare", "0"]],
        label: (
          <div>
            <p>
              With whom did you share this information? (please select all that
              apply) *
            </p>
          </div>
        ),
        options: [
          {
            label: "Family member",
            value: "1",
          },
          {
            label: "Friend",
            value: "2",
          },
          {
            label: "Colleague",
            value: "3",
          },
          {
            label: "Health Professional",
            value: "4",
          },
          {
            label: "Other",
            value: "5",
          },
        ] as const,
        freeText: {
          property: "infoshare_who_oth",
          value: "5",
          label: "Please specify *",
        },
      },
      {
        key: "infoshare_typ",
        type: "checkbox-group",
        className: "border border-4 rounded-lg border-pink-600",
        notRenderedConds: [["programEvalSharing", "infoshare", "0"]],
        label: (
          <div>
            <p>
              What information did you share? (please select all that apply) *
            </p>
          </div>
        ),
        options: [
          {
            label: "Physical activity",
            value: "1",
          },
          {
            label: "Sun protection",
            value: "2",
          },
          {
            label: "Alcohol",
            value: "3",
          },
          {
            label: "Tobacco or e-cigarette products",
            value: "4",
          },
          {
            label: "Diet",
            value: "5",
          },
        ] as const,
      },
    ],
  },
  programEvalMessaging: {
    className: "grid lg:grid-cols-7 gap-3 w-full lg:max-w-6xl",
    questions: [
      {
        key: "program_eval_messaging_intro",
        type: "element",
        content: (
          <>
            <div className="col-span-full">
              <div className="space-y-3">
                <p>
                  To what extent do you agree or disagree with the following
                  statements?
                </p>
              </div>
            </div>
            <div className="hidden lg:col-span-2 lg:block" />
            <div className="hidden text-center before:-translate-x-1 lg:flex lg:items-center lg:justify-center">
              Strongly agree
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Agree
            </div>
            <div className="hidden text-left lg:flex lg:items-center lg:justify-center">
              Neither agree nor disagree
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Disagree
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Strongly disagree
            </div>
          </>
        ),
      },
      {
        key: "h4hmsg_newinfo",
        label: "The messaging in Health4Her taught me something new *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4hmsg_easy",
        label: "The messaging in Health4Her was easy to understand *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4hmsg_trustw",
        label: "The messaging in Health4Her was trustworthy *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4hmsg_relevant",
        label: "The messaging in Health4Her was relevant to me * ",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4hmsg_useful",
        label: "The messaging in Health4Her has been useful to me *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
    ] as const,
  },
  programEvalMotivation: {
    questions: [
      {
        key: "motivbhv",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>Did you change your behaviour after participating in H4H? *</p>
          </div>
        ),
        options: [
          {
            label: "Yes",
            value: "1",
          },
          {
            label: "No",
            value: "0",
          },
        ] as const,
      },
      {
        key: "motivbhv_typ",
        type: "checkbox-group",
        className: "border border-4 rounded-lg border-pink-600",
        notRenderedConds: [["programEvalMotivation", "motivbhv", "0"]],
        label: (
          <div>
            <p>
              What behaviour/s did you change? (Please select all that apply) *
            </p>
          </div>
        ),
        options: [
          {
            label: "Physical activity",
            value: "1",
          },
          {
            label: "Sun protection",
            value: "2",
          },
          {
            label: "Alcohol",
            value: "3",
          },
          {
            label: "Tobacco or e-cigarette products",
            value: "4",
          },
          {
            label: "Diet",
            value: "5",
          },
        ] as const,
      },
      {
        key: "motivbhv_desc",
        type: "input",
        label: (
          <span>
            Please describe what changes you have made to your behaviour since
            participating in Health4Her.
          </span>
        ),
        placeholder: "Describe changes here",
        required: false,
        inputType: "text",
        className: "col-span-full sm:col-span-2",
        labelClassName: "flex items-center col-span-full sm:col-span-full",
        notRenderedConds: [["programEvalMotivation", "motivbhv", "0"]],
      },
      {
        key: "h4h_interest",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>
              Would you have been interested participating in Health4Her if it
              had not been part of a research study? *
            </p>
          </div>
        ),
        options: [
          {
            label: "Yes",
            value: "1",
          },
          {
            label: "No",
            value: "2",
          },
          { label: "Unsure", value: "3" },
        ] as const,
      },
      {
        key: "influ_screen",
        type: "dropdown",
        label:
          "Has receiving Health4Her at your last screening appointment influenced your likelihood of attending future BreastScreen appointments? *",
        options: [
          {
            label: "No difference",
            value: "1",
          },
          {
            label: "More likely",
            value: "2",
          },
          { label: "Less likely", value: "3" },
        ] as const,
        wrapperClassName: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-start col-span-full sm:col-span-1 mt-2 sm:mt-3",
        freeText: {
          property: "influ_screen_less",
          value: "3",
          label: "Please tell us why",
          required: false,
        },
      },
      {
        key: "msg_insenstv",
        type: "dropdown",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>Do you think the messaging in Health4Her was insensitive? *</p>
          </div>
        ),
        options: [
          {
            label: "No",
            value: "1",
          },
          {
            label: "Yes",
            value: "2",
          },
          { label: "Unsure", value: "3" },
        ] as const,
        freeText: {
          property: "msg_insenstv_desc",
          value: "2",
          label: "Please tell us why",
          required: false,
        },
      },
      {
        key: "msg_stigma",
        type: "dropdown",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>Do you think the messaging in Health4Her was stigmatising? *</p>
          </div>
        ),
        options: [
          {
            label: "No",
            value: "1",
          },
          {
            label: "Yes",
            value: "2",
          },
          { label: "Unsure", value: "3" },
        ] as const,
        freeText: {
          property: "msg_stigma_desc",
          value: "2",
          label: "Please tell us why",
          required: false,
        },
      },
    ],
  },
  preludeToOverallMessaging: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We’re seeking your feedback about the overall messaging in
                Health4Her. Here are some of the main messages below.
              </p>
            </div>
            <ul
              role="list"
              className="grid grid-cols-1 gap-x-4 gap-y-8 pt-5 sm:grid-cols-2 sm:gap-x-6 lg:grid-cols-3 xl:gap-x-8"
            >
              {messagingImages.map((file) => (
                <li key={file.src} className="relative">
                  <div className="group block w-full overflow-hidden rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-indigo-500 focus-within:ring-offset-2 focus-within:ring-offset-gray-100">
                    <img
                      src={file.src}
                      alt={file.title}
                      className="pointer-events-none object-cover"
                    />
                    <button
                      type="button"
                      className="absolute inset-0 focus:outline-none"
                    >
                      <span className="sr-only">
                        View details for {file.title}
                      </span>
                    </button>
                  </div>
                  <p className="pointer-events-none mt-2 block text-sm font-medium text-gray-900">
                    {file.text}
                  </p>
                </li>
              ))}
            </ul>
          </>
        ),
      },
    ] as const,
  },
  programEvalOverallMessaging: {
    className: "grid lg:grid-cols-7 gap-3 w-full lg:max-w-6xl",
    questions: [
      {
        key: "program_eval_msg_intro",
        type: "element",
        content: (
          <>
            <div className="col-span-full">
              <div className="space-y-3">
                <p>
                  Please indicate the extent to which you agree or disagree that
                  Health4Her…
                </p>
              </div>
            </div>
            <div className="hidden lg:col-span-2 lg:block" />
            <div className="hidden text-center before:-translate-x-1 lg:flex lg:items-center lg:justify-center">
              Strongly agree
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Agree
            </div>
            <div className="hidden text-left lg:flex lg:items-center lg:justify-center">
              Neither agree nor disagree
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Disagree
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Strongly disagree
            </div>
          </>
        ),
      },
      {
        key: "h4h_think",
        label: "Makes me stop and think about my drinking *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4h_persrelevant",
        label: "Is personally relevant *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4h_motivates",
        label: "Motivates me to reduce the amount of alcohol I drink. *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4h_concerned",
        label: "Makes me concerned about my drinking *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4h_easyundrstnd",
        label: "Is easy to understand. *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4h_believable",
        label: "Is believable. *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
    ] as const,
  },
  programEvalOpinion: {
    questions: [
      {
        key: "h4h_negattitudes",
        type: "dropdown",
        label:
          "Do you think the messaging in Health4Her promotes negative attitudes toward people who drink? *",
        options: [
          {
            label: "No",
            value: "1",
          },
          {
            label: "Yes",
            value: "2",
          },
          { label: "Unsure", value: "3" },
        ] as const,
        wrapperClassName: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-start col-span-full sm:col-span-1 mt-2 sm:mt-0",
        freeText: {
          property: "h4h_negattitudes_desc",
          value: "2",
          label: "Please tell us why",
          required: false,
        },
      },
      {
        key: "h4h_blame",
        type: "dropdown",
        label:
          "Do you think the messaging in Health4Her increases blame towards women who have been diagnosed with breast cancer? *",
        options: [
          {
            label: "No",
            value: "1",
          },
          {
            label: "Yes",
            value: "2",
          },
          { label: "Unsure", value: "3" },
        ] as const,
        wrapperClassName: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-start col-span-full sm:col-span-1 mt-2 sm:mt-0",
        freeText: {
          property: "h4h_blame_desc",
          value: "2",
          label: "Please tell us why",
          required: false,
        },
      },
      {
        key: "h4h_simple",
        type: "dropdown",
        label:
          "Do you think the messaging in Health4Her makes reducing alcohol consumption seem like a simpler thing to do than it really is? *",
        options: [
          {
            label: "No",
            value: "1",
          },
          {
            label: "Yes",
            value: "2",
          },
          { label: "Unsure", value: "3" },
        ] as const,
        wrapperClassName: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-start col-span-full sm:col-span-1 mt-2 sm:mt-0",
        freeText: {
          property: "h4h_simple_desc",
          value: "2",
          label: "Please tell us why",
          required: false,
        },
      },
    ],
  },
  programEvalFeedback: {
    questions: [
      {
        key: "h4h_suggest",
        type: "input",
        label:
          "Do you have any suggestions on how we could improve Health4Her?",
        required: false,
        placeholder: "Please write your suggestions here",
        className: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
      },
    ],
  },
  thankyou: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg md:text-2xl">
                Thank you for participating in the{" "}
                <span className="text-lg font-bold md:text-2xl">
                  Health4Her study
                </span>
                .
              </p>
              <p className="mt-10 text-lg md:text-2xl">
                We have pledged $10 to breast cancer research for receiving your
                completed survey.
              </p>
              <p className="mt-10 text-lg md:text-2xl">
                We thank you for your valuable contribution to this women&apos;s
                health research.
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
} satisfies Survey;
export const followupSurveyControl = {
  aboutYou: {
    className: "grid grid-cols-4 gap-5",
    questions: [
      {
        key: "intro",
        type: "element",
        content: (
          <div className="col-span-4 mb-5">
            <legend className="mb-5 text-xl font-semibold">About you</legend>
            <span>
              <strong>
                In this section, please provide your name and date of birth.
              </strong>
            </span>
            <p>
              Remember, your responses to this survey are{" "}
              <strong>strictly confidential</strong>. Maroondah BreastScreen and
              Eastern Health will not have access to any information you
              provide.
            </p>
          </div>
        ),
      },
      {
        key: "first_name",
        type: "input",
        label: "First name *",
        className: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
      },
      {
        key: "last_name",
        type: "input",
        label: "Last name *",
        className: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
      },
      {
        key: "dob",
        type: "datepicker",
        className: "col-span-full sm:col-span-3 appearance-none",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
        label: "Date of birth *",
      },
    ] as const,
  },
  preludeToPastMonthBehaviour: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We would now like to understand your lifestyle{" "}
                <span className="text-lg font-bold italic md:text-2xl">
                  behaviours
                </span>{" "}
                in the
                {"  "}
                <span className="text-lg font-bold underline md:text-2xl">
                  past month
                </span>
                .
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  pastMonthBehaviour: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "bhvfreq_phys",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>
              In the <strong>past month</strong>, how often did you engage in
              physical activity? *
            </p>
            <p>
              (For example, brisk walking, jogging, swimming, tennis, gardening
              or work around the house, riding a bicycle)
            </p>
          </div>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Occasionally, but not in the past month",
            value: "7",
          },
          { label: "Do not / cannot exercise", value: "8" },
        ] as const,
      },
      {
        key: "bhvquant_phys_mins",
        type: "input",
        label: (
          <span>
            In the <strong>past month</strong>, on a <em> typical</em> day that
            you engaged in physical activity, how many <u>minutes</u> did you
            do? *
          </span>
        ),
        placeholder: "Please type how many minutes",
        inputType: "text",
        className: "col-span-full sm:col-span-2",
        labelClassName:
          "flex items-center col-span-full sm:col-span-full mt-2 sm:mt-10",
        notRenderedConds: [["pastMonthBehaviour", "bhvfreq_phys", "8"]],
      },
      {
        key: "bhvfreq_sun",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            <p>
              In the <strong>past month</strong>, how often did you engage in
              sun protection behaviours? *
            </p>
            <p>
              (For example, wearing sunscreen, a hat, or other sun-protective
              clothing)
            </p>
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Less often",
            value: "7",
          },
        ] as const,
      },
    ] as const,
  },
  pastMonthAlcohol: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "bhvfreq_alc",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            In the <strong>past month</strong>, how often did you have an
            alcoholic drink of any kind? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "About 1 day per month",
            value: "6",
          },
          {
            label: "Occasionally, but not in the past month",
            value: "7",
          },
          {
            label: "Do not drink / no longer drink",
            value: "8",
          },
        ] as const,
      },
      {
        key: "bhvDrinkOnDay",
        type: "image-counter",
        notRenderedConds: [["pastMonthAlcohol", "bhvfreq_alc", "8"]],
        label: (
          <span>
            In the <strong>past month</strong>, on a <em>typical</em> day that
            you had an alcoholic drink, how many drinks did you have? *
          </span>
        ),
        options: [
          {
            value: "bhvquant_avred",
            image: "/images/drinks/RedWine_WideBig.png",
            label: "Average serve of red wine (150ml)",
          },
          {
            value: "bhvquant_avwhite",
            image: "/images/drinks/WhiteWine_WideBig.png",
            label: "Average serve of white wine (150ml)",
          },
          {
            value: "bhvquant_avsparkle",
            image: "/images/drinks/Champagne1.png",
            label: "Average serve of sparkling wine (150ml)",
          },
          {
            value: "bhvquant_smlred",
            image: "/images/drinks/RedWine_NarrowSmall.png",
            label: "Small serve of red wine (100ml)",
          },
          {
            value: "bhvquant_smlwhite",
            image: "/images/drinks/WhiteWine_NarrowSmall.png",
            label: "Small serve of white wine (100ml)",
          },
          {
            value: "bhvquant_shot",
            image: "/images/drinks/Scotch.png",
            label: "Shot/nip spirit (30ml) – with or without mixer",
          },
          {
            value: "bhvquant_cocktail",
            image: "/images/drinks/Cocktail1.png",
            label: "Cocktail",
          },
          {
            value: "bhvquant_rtd",
            image: "/images/drinks/Scotch.png",
            label: "Ready-to-drink spirit (275ml)",
          },
          {
            value: "bhvquant_beer",
            image: "/images/drinks/BeerPot.png",
            label: "Beer or cider (375ml)",
          },
        ] as const,
      },
    ] as const,
  },
  pastMonthSecondaryBehaviour: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "bhvfreq_cig",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            In the <strong>past month</strong>, have you used any tobacco or
            e-cigarette products? *
          </span>
        ),
        options: [
          {
            label: "No",
            value: "0",
          },
          {
            label: "Yes, regularly (at least once per day)",
            value: "1",
          },
          {
            label: "Yes, not regularly (less than once per day)",
            value: "2",
          },
        ] as const,
      },
      {
        key: "bhvfreq_veg",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            In the <strong>past month</strong>, how often did you include
            vegetables as part of a meal? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "Less often",
            value: "6",
          },
        ] as const,
      },
    ] as const,
  },
  preludeToOutcomes: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We would now like to understand your behaviour-related{" "}
                <span className="text-lg font-bold italic md:text-2xl">
                  intentions
                </span>{" "}
                over the{" "}
                <span className="text-lg font-bold underline md:text-2xl">
                  next month
                </span>
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  outcomes: {
    className: "grid gap-5 w-full mt-5",
    questions: [
      {
        key: "int_phys",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to increase the amount of exercise you do? *
          </span>
        ),
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_sun",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to increase your sun protection practices? *
          </span>
        ),

        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_alc",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to reduce the amount of alcohol you consume? *
          </span>
        ),
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_cig",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to reduce any tobacco or e-cigarette use? *
          </span>
        ),
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_veg",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to increase the amount of vegetables you consume? *
          </span>
        ),

        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
    ] as const,
  },
  preludeToSecondaryOutcomes: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We will now ask you some further questions about your
                behaviour-related{" "}
                <span className="text-lg font-bold italic md:text-2xl">
                  intentions
                </span>{" "}
                over the{" "}
                <span className="text-lg font-bold underline md:text-2xl">
                  next month
                </span>
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  secondaryOutcomes: {
    questions: [
      {
        key: "intfreq_phys",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>
              Over the <strong>next month</strong>, how often do you intend to
              engage in physical activity? *
            </p>
            <p>
              (For example, brisk walking, jogging, swimming, tennis, gardening
              or work around the house, riding a bicycle)
            </p>
          </div>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Occasionally, but not in the next month",
            value: "7",
          },
          { label: "Do not / cannot exercise", value: "8" },
        ] as const,
      },
      {
        key: "intquant_phys_mins",
        type: "input",
        label: (
          <span>
            Over the <strong>next month</strong>, on a <em>typical</em> day that
            you engage in physical activity, how many <u>minutes</u> do you
            intend to do? *
          </span>
        ),
        placeholder: "Please type how many minutes",
        inputType: "text",
        className: "col-span-full sm:col-span-2",
        labelClassName: "flex items-center col-span-full sm:col-span-full",
        notRenderedConds: [["secondaryOutcomes", "intfreq_phys", "8"]],
      },
      {
        key: "intfreq_sun",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            <p>
              Over the <strong>next month</strong>, how often do you intend to
              engage in sun protection behaviours? *
            </p>
            <p>
              (For example, wearing sunscreen, a hat, or other sun-protective
              clothing)
            </p>
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Less often",
            value: "7",
          },
        ] as const,
      },
    ] as const,
  },
  secondaryOutcomesPart2: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "intfreq_alc",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            Over the <strong>next month</strong>, how often do you intend to
            have an alcoholic drink of any kind? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "About 1 day per month",
            value: "6",
          },
          {
            label: "Occasionally, but not in the next month",
            value: "7",
          },
          {
            label: "Do not drink / no longer drink",
            value: "8",
          },
        ] as const,
      },
      {
        key: "intDrinkOnDay",
        type: "image-counter",
        notRenderedConds: [
          ["secondaryOutcomesPart2", "intfreq_alc", "8"],
        ] as const,
        label: (
          <span>
            Over the <strong>next month</strong>, on a <em>typical </em> day
            that you have an alcoholic drink, how many drinks do you intend to
            have? *
          </span>
        ),
        options: [
          {
            value: "intquant_avred",
            image: "/images/drinks/RedWine_WideBig.png",
            label: "Average serve of red wine (150ml)",
          },
          {
            value: "intquant_avwhite",
            image: "/images/drinks/WhiteWine_WideBig.png",
            label: "Average serve of white wine (150ml)",
          },
          {
            value: "intquant_avsparkle",
            image: "/images/drinks/Champagne1.png",
            label: "Average serve of sparkling wine (150ml)",
          },
          {
            value: "intquant_smlred",
            image: "/images/drinks/RedWine_NarrowSmall.png",
            label: "Small serve of red wine (100ml)",
          },
          {
            value: "intquant_smlwhite",
            image: "/images/drinks/WhiteWine_NarrowSmall.png",
            label: "Small serve of white wine (100ml)",
          },
          {
            value: "intquant_shot",
            image: "/images/drinks/Scotch.png",
            label: "Shot/nip spirit (30ml) – with or without mixer",
          },
          {
            value: "intquant_cocktail",
            image: "/images/drinks/Cocktail1.png",
            label: "Cocktail",
          },
          {
            value: "intquant_rtd",
            image: "/images/drinks/pre-mix.png",
            label: "Ready-to-drink spirit (275ml)",
          },
          {
            value: "intquant_beer",
            image: "/images/drinks/BeerPot.png",
            label: "Beer or cider (375ml)",
          },
        ] as const,
      },
    ] as const,
  },
  secondaryOutcomesPart3: {
    questions: [
      {
        key: "intfreq_cig",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            Over the <strong>next month</strong>, do you intend to use any{" "}
            tobacco or e-cigarette products ? *
          </span>
        ),
        options: [
          {
            label: "No",
            value: "0",
          },
          {
            label: "Yes, regularly (at least once per day)",
            value: "1",
          },
          {
            label: "Yes, not regularly (less than once per day)",
            value: "2",
          },
        ] as const,
      },
      {
        key: "intfreq_veg",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            Over the <strong>next month</strong>, how often do you intend to
            include vegetables as part of a meal? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "Less often",
            value: "6",
          },
        ] as const,
      },
    ] as const,
  },
  preludeToKnowledge: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We would now like to understand your current knowledge of breast
                cancer risk factors. It&apos;s okay to be uncertain, just select
                &quot;unsure&quot;.
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  riskKnowledge: {
    className: "grid lg:grid-cols-6 gap-3 w-full lg:max-w-6xl",
    questions: [
      {
        key: "secondaryOutcomesPart3Intro",
        type: "element",
        content: (
          <>
            <div className="col-span-full">
              <div className="space-y-3">
                <p>
                  To what extent do you consider the following factors to be
                  associated with an increased risk of breast cancer?
                </p>
                <div className="flex flex-col gap-3 rounded-lg border p-2 lg:flex-row">
                  <div className="mb-2 grid grid-flow-col-dense p-2">
                    <p className="pl-3">
                      <span className="text-red-600">
                        <strong>Clear risk factor</strong>
                      </span>
                      : there is strong, consistent evidence that this factor
                      increases risk of breast cancer.
                    </p>
                  </div>
                  <div className="grid grid-flow-col-dense p-2">
                    <p className="pl-3">
                      <span className="text-light-pink-700">
                        <strong>Possible risk factor</strong>
                      </span>
                      : there is some evidence that this factor increases risk
                      of breast cancer, but not enough to be certain.
                    </p>
                  </div>
                  <div className="grid grid-flow-col-dense p-2">
                    <p className="pl-3">
                      <span>
                        <strong className="text-green-600">
                          Not a proven risk factor
                        </strong>
                      </span>
                      : the evidence is too limited to determine whether this
                      factor increases risk of breast cancer.
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="hidden lg:col-span-2 lg:block" />
            <div className="hidden text-center text-red-600 before:-translate-x-1 lg:flex lg:items-center lg:justify-center">
              Clear risk factor
            </div>
            <div className="text-light-pink-700 hidden text-center lg:flex lg:items-center lg:justify-center">
              Possible risk factor
            </div>
            <div className="hidden text-left text-green-600  lg:flex lg:items-center lg:justify-center">
              Not a proven risk factor
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Unsure
            </div>
          </>
        ),
      },
      {
        key: "bcrisk_famhist",
        label: "Family history of breast cancer *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_phys",
        label: "Physical inactivity *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_sunexpo",
        label: "Sun exposure *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_alc",
        label: "Drinking alcohol *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_cig",
        label: "Using tobacco products *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_ovweight",
        label: "Being overweight *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
    ] as const,
  },
  knowledge: {
    className: "grid lg:grid-cols-6 gap-3 w-full lg:max-w-6xl",
    questions: [
      {
        key: "knowl_phys",
        type: "button-group",
        label:
          "International guidelines recommend a minimum of _____ minutes of physical activity, five days a week. *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "30 minutes",
            value: "1",
          },
          {
            label: "45 minutes",
            value: "2",
          },
          {
            label: "60 minutes",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_sunscr",
        type: "button-group",
        label:
          "Sunscreen needs to be applied how many minutes before going outdoors? *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "20 minutes",
            value: "1",
          },
          {
            label: "30 minutes",
            value: "2",
          },
          {
            label: "60 minutes",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_sd",
        type: "button-group",
        label:
          "Drinking one average restaurant serve of wine a day increases breast cancer risk by... *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "4%",
            value: "1",
          },
          {
            label: "23%",
            value: "2",
          },
          {
            label: "61%",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_redwine",
        type: "button-group",
        label: "An average restaurant serve of red wine contains... *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "Less than 1 standard drink",
            value: "1",
          },
          {
            label: "1 standard drink",
            value: "2",
          },
          {
            label: "Closer to 2 standard drinks",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_alcguideline",
        type: "button-group",
        label:
          "Australian guidelines recommend that women should drink no more than _____ standard drinks per week *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "12 standard drinks",
            value: "1",
          },
          {
            label: "10 standard drinks",
            value: "2",
          },
          {
            label: "7 standard drinks",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_diet",
        type: "button-group",
        label:
          "Dietary guidelines advise that women consume more of which food group as they age?",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "Dairy",
            value: "1",
          },
          {
            label: "Vegetables",
            value: "2",
          },
          {
            label: "Protein",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
    ] as const,
  },
  programEval: {
    className: "grid lg:grid-cols-7 gap-3 w-full lg:max-w-6xl",
    questions: [
      {
        key: "program_eval_intro",
        type: "element",
        content: (
          <>
            <div className="col-span-full">
              <div className="space-y-3">
                <p>
                  <strong>
                    We&apos;d now like to ask you a few final questions about
                    your experience of Health4Her
                  </strong>
                </p>
                <p>
                  When thinking about your experience of participating in
                  Health4Her, to what extent do you AGREE or DISAGREE with the
                  following statements?
                </p>
              </div>
            </div>
            <div className="hidden lg:col-span-2 lg:block" />
            <div className="hidden text-center before:-translate-x-1 lg:flex lg:items-center lg:justify-center">
              Strongly agree
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Agree
            </div>
            <div className="hidden text-left lg:flex lg:items-center lg:justify-center">
              Neither agree nor disagree
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Disagree
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Strongly disagree
            </div>
          </>
        ),
      },
      {
        key: "h4h_infoaccept",
        label:
          "Being provided health promotion information by BreastScreen was acceptable to me. *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4h_stdservice",
        label:
          "Health4Her should be offered as part of the standard service offered by BreastScreen. *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4h_easyuse",
        label: "The Health4Her website was easy to use. *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4h_timeaccept",
        label:
          "Taking approximately 15 minutes following my BreastScreen appointment to receive some breast cancer risk reduction information was acceptable to me. *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4h_animaccept",
        label:
          "The use of animation was an acceptable way to inform women about breast cancer risks. *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
    ] as const,
  },
  programEvalSharing: {
    questions: [
      {
        key: "infoshare",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>
              Did you tell anyone about any of the information provided in
              Health4Her? *
            </p>
          </div>
        ),
        options: [
          {
            label: "Yes",
            value: "1",
          },
          {
            label: "No",
            value: "0",
          },
        ] as const,
      },
      {
        key: "infoshare_who",
        type: "checkbox-group",
        className: "border border-4 rounded-lg border-pink-600",
        notRenderedConds: [["programEvalSharing", "infoshare", "0"]],
        label: (
          <div>
            <p>
              With whom did you share this information? (please select all that
              apply) *
            </p>
          </div>
        ),
        options: [
          {
            label: "Family member",
            value: "1",
          },
          {
            label: "Friend",
            value: "2",
          },
          {
            label: "Colleague",
            value: "3",
          },
          {
            label: "Health Professional",
            value: "4",
          },
          {
            label: "Other",
            value: "5",
          },
        ] as const,
        freeText: {
          property: "infoshare_who_oth",
          value: "5",
          label: "Please specify *",
        },
      },
      {
        key: "infoshare_typ",
        type: "checkbox-group",
        className: "border border-4 rounded-lg border-pink-600",
        notRenderedConds: [["programEvalSharing", "infoshare", "0"]],
        label: (
          <div>
            <p>
              What information did you share? (please select all that apply) *
            </p>
          </div>
        ),
        options: [
          {
            label: "Physical activity",
            value: "1",
          },
          {
            label: "Sun protection",
            value: "2",
          },
          {
            label: "Alcohol",
            value: "3",
          },
          {
            label: "Tobacco or e-cigarette products",
            value: "4",
          },
          {
            label: "Diet",
            value: "5",
          },
        ] as const,
      },
    ],
  },
  programEvalMessaging: {
    className: "grid lg:grid-cols-7 gap-3 w-full lg:max-w-6xl",
    questions: [
      {
        key: "program_eval_messaging_intro",
        type: "element",
        content: (
          <>
            <div className="col-span-full">
              <div className="space-y-3">
                <p>
                  To what extent do you agree or disagree with the following
                  statements?
                </p>
              </div>
            </div>
            <div className="hidden lg:col-span-2 lg:block" />
            <div className="hidden text-center before:-translate-x-1 lg:flex lg:items-center lg:justify-center">
              Strongly agree
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Agree
            </div>
            <div className="hidden text-left lg:flex lg:items-center lg:justify-center">
              Neither agree nor disagree
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Disagree
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Strongly disagree
            </div>
          </>
        ),
      },
      {
        key: "h4hmsg_newinfo",
        label: "The messaging in Health4Her taught me something new *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4hmsg_easy",
        label: "The messaging in Health4Her was easy to understand *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4hmsg_trustw",
        label: "The messaging in Health4Her was trustworthy *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4hmsg_relevant",
        label: "The messaging in Health4Her was relevant to me * ",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "h4hmsg_useful",
        label: "The messaging in Health4Her has been useful to me *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Strongly agree",
            value: "5",
            labelClassName: "lg:hidden",
          },
          {
            label: "Agree",
            value: "4",
            labelClassName: "lg:hidden",
          },
          {
            label: "Neither agree nor disagree",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Disagree",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Strongly disagree",
            value: "1",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
    ] as const,
  },
  programEvalMotivation: {
    questions: [
      {
        key: "motivbhv",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>Did you change your behaviour after participating in H4H? *</p>
          </div>
        ),
        options: [
          {
            label: "Yes",
            value: "1",
          },
          {
            label: "No",
            value: "0",
          },
        ] as const,
      },
      {
        key: "motivbhv_typ",
        type: "checkbox-group",
        className: "border border-4 rounded-lg border-pink-600",
        notRenderedConds: [["programEvalMotivation", "motivbhv", "0"]],
        label: (
          <div>
            <p>
              What behaviour/s did you change? (Please select all that apply) *
            </p>
          </div>
        ),
        options: [
          {
            label: "Physical activity",
            value: "1",
          },
          {
            label: "Sun protection",
            value: "2",
          },
          {
            label: "Alcohol",
            value: "3",
          },
          {
            label: "Tobacco or e-cigarette products",
            value: "4",
          },
          {
            label: "Diet",
            value: "5",
          },
        ] as const,
      },
      {
        key: "motivbhv_desc",
        type: "input",
        label: (
          <span>
            Please describe what changes you have made to your behaviour since
            participating in Health4Her.
          </span>
        ),
        placeholder: "Describe changes here",
        required: false,
        inputType: "text",
        className: "col-span-full sm:col-span-2",
        labelClassName: "flex items-center col-span-full sm:col-span-full",
        notRenderedConds: [["programEvalMotivation", "motivbhv", "0"]],
      },
      {
        key: "h4h_interest",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>
              Would you have been interested participating in Health4Her if it
              had not been part of a research study? *
            </p>
          </div>
        ),
        options: [
          {
            label: "Yes",
            value: "1",
          },
          {
            label: "No",
            value: "2",
          },
          { label: "Unsure", value: "3" },
        ] as const,
      },
      {
        key: "influ_screen",
        type: "dropdown",
        label:
          "Has receiving Health4Her at your last screening appointment influenced your likelihood of attending future BreastScreen appointments? *",
        options: [
          {
            label: "No difference",
            value: "1",
          },
          {
            label: "More likely",
            value: "2",
          },
          { label: "Less likely", value: "3" },
        ] as const,
        wrapperClassName: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-start col-span-full sm:col-span-1 mt-2 sm:mt-3",
        freeText: {
          property: "influ_screen_less",
          value: "3",
          label: "Please tell us why",
          required: false,
        },
      },
      {
        key: "msg_insenstv",
        type: "dropdown",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>Do you think the messaging in Health4Her was insensitive? *</p>
          </div>
        ),
        options: [
          {
            label: "No",
            value: "1",
          },
          {
            label: "Yes",
            value: "2",
          },
          { label: "Unsure", value: "3" },
        ] as const,
        freeText: {
          property: "msg_insenstv_desc",
          value: "2",
          label: "Please tell us why",
          required: false,
        },
      },
      {
        key: "msg_stigma",
        type: "dropdown",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>Do you think the messaging in Health4Her was stigmatising? *</p>
          </div>
        ),
        options: [
          {
            label: "No",
            value: "1",
          },
          {
            label: "Yes",
            value: "2",
          },
          { label: "Unsure", value: "3" },
        ] as const,
        freeText: {
          property: "msg_stigma_desc",
          value: "2",
          label: "Please tell us why",
          required: false,
        },
      },
    ],
  },
  programEvalFeedback: {
    questions: [
      {
        key: "h4h_suggest",
        type: "input",
        label:
          "Do you have any suggestions on how we could improve Health4Her?",
        required: false,
        placeholder: "Please write your suggestions here",
        className: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
      },
    ],
  },
  thankyou: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg md:text-2xl">
                Thank you for participating in the{" "}
                <span className="text-lg font-bold md:text-2xl">
                  Health4Her study
                </span>
                .
              </p>
              <p className="mt-10 text-lg md:text-2xl">
                We have pledged $10 to breast cancer research for receiving your
                completed survey.
              </p>
              <p className="mt-10 text-lg md:text-2xl">
                We thank you for your valuable contribution to this women&apos;s
                health research.
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
} satisfies Survey;

// const followupSurveyDefaultState: SurveyState<typeof followupSurvey> = {
//   aboutYou: {
//     first_name: "Chris",
//     last_name: "Prawira",
//     comm_pref: { label: "Email and SMS", value: "1" },
//     email: "a@a.com",
//     ph_mobile: "0452664517",
//     ph_home: "23132121",
//     postcode: "3000",
//     dob: "",
//     recruit: { label: "My appointment at Maroondah Breastscreen", value: "1" },
//     pilot_no: "1234",
//     prev_part: {
//       label: "No",
//       value: "0",
//     },
//   },
//   demographic: {
//     birthplace: "Australia",
//     lang: "0",
//     atsi: "0",
//     gender: "1",
//     sex: "1",
//     lgbt: "0",
//     edu: "1",
//     disable: "0",
//   },
//   preludeToPastMonthBehaviour: {},
//   pastMonthBehaviour: {
//     bhvfreq_phys: "1",
//     bhvquant_phys_mins: "1",
//     bhvfreq_sun: "1",
//   },
//   pastMonthAlcohol: {
//     bhvfreq_alc: "1",
//     drinkOnDay: {
//       bhvquant_avred: 1,
//       bhvquant_avwhite: 0,
//       bhvquant_avsparkle: 0,
//       bhvquant_smlred: 0,
//       bhvquant_smlwhite: 0,
//       bhvquant_shot: 0,
//       bhvquant_cocktail: 0,
//       bhvquant_rtd: 0,
//       bhvquant_beer: 0,
//     },
//   },
//   pastMonthSecondaryBehaviour: {
//     bhvfreq_cig: "1",
//     bhvfreq_veg: "1",
//   },
//   preludeToOutcomes: {},
//   outcomes: {
//     int_phys: "1",
//     int_sun: "1",
//     int_alc: "1",
//     int_cig: "1",
//     int_veg: "1",
//   },
//   preludeToSecondaryOutcomes: {},
//   secondaryOutcomes: {
//     intfreq_phys: "1",
//     intquant_phys_mins: "1",
//     intfreq_sun: "1",
//   },
//   secondaryOutcomesPart2: {
//     intfreq_alc: "1",
//     drinkOnDay: {
//       intquant_avred: 1,
//       intquant_avwhite: 0,
//       intquant_avsparkle: 0,
//       intquant_smlred: 0,
//       intquant_smlwhite: 0,
//       intquant_shot: 0,
//       intquant_cocktail: 0,
//       intquant_rtd: 0,
//       intquant_beer: 0,
//     },
//   },
//   secondaryOutcomesPart3: {
//     intfreq_cig: "1",
//     intfreq_veg: "1",
//   },
//   preludeToKnowledge: {},
//   riskKnowledge: {
//     bcrisk_famhist: "1",
//     bcrisk_phys: "1",
//     bcrisk_sunexpo: "1",
//     bcrisk_alc: "1",
//     bcrisk_cig: "1",
//     bcrisk_ovweight: "1",
//   },
//   knowledge: {
//     knowl_phys: "1",
//     knowl_sunscr: "1",
//     knowl_sd: "1",
//     knowl_redwine: "1",
//     knowl_alcguideline: "1",
//     knowl_diet: "1",
//   },
//   preludeToActivities: {},
// };

const followupSurveyDefaultStateIntervention: SurveyState<
  typeof followupSurveyIntervention
> = {
  aboutYou: {
    first_name: "",
    last_name: "",
    dob: "",
  },
  preludeToPastMonthBehaviour: {},
  pastMonthBehaviour: {
    bhvfreq_phys: null,
    bhvquant_phys_mins: "",
    bhvfreq_sun: null,
  },
  pastMonthAlcohol: {
    bhvfreq_alc: null,
    bhvDrinkOnDay: {
      bhvquant_avred: 0,
      bhvquant_avwhite: 0,
      bhvquant_avsparkle: 0,
      bhvquant_smlred: 0,
      bhvquant_smlwhite: 0,
      bhvquant_shot: 0,
      bhvquant_cocktail: 0,
      bhvquant_rtd: 0,
      bhvquant_beer: 0,
    },
  },
  pastMonthSecondaryBehaviour: {
    bhvfreq_cig: null,
    bhvfreq_veg: null,
  },
  preludeToOutcomes: {},
  outcomes: {
    int_phys: null,
    int_sun: null,
    int_alc: null,
    int_cig: null,
    int_veg: null,
  },
  preludeToSecondaryOutcomes: {},
  secondaryOutcomes: {
    intfreq_phys: null,
    intquant_phys_mins: "",
    intfreq_sun: null,
  },
  secondaryOutcomesPart2: {
    intfreq_alc: null,
    intDrinkOnDay: {
      intquant_avred: 0,
      intquant_avwhite: 0,
      intquant_avsparkle: 0,
      intquant_smlred: 0,
      intquant_smlwhite: 0,
      intquant_shot: 0,
      intquant_cocktail: 0,
      intquant_rtd: 0,
      intquant_beer: 0,
    },
  },
  secondaryOutcomesPart3: {
    intfreq_cig: null,
    intfreq_veg: null,
  },
  preludeToKnowledge: {},
  riskKnowledge: {
    bcrisk_famhist: null,
    bcrisk_phys: null,
    bcrisk_sunexpo: null,
    bcrisk_alc: null,
    bcrisk_cig: null,
    bcrisk_ovweight: null,
  },
  knowledge: {
    knowl_phys: null,
    knowl_sunscr: null,
    knowl_sd: null,
    knowl_redwine: null,
    knowl_alcguideline: null,
    knowl_diet: null,
  },
  programEval: {
    h4h_infoaccept: null,
    h4h_stdservice: null,
    h4h_easyuse: null,
    h4h_timeaccept: null,
    h4h_animaccept: null,
  },
  programEvalSharing: {
    infoshare: null,
    infoshare_who: null,
    infoshare_typ: null,
  },
  programEvalMessaging: {
    h4hmsg_newinfo: null,
    h4hmsg_easy: null,
    h4hmsg_trustw: null,
    h4hmsg_relevant: null,
    h4hmsg_useful: null,
  },
  programEvalMotivation: {
    motivbhv: null,
    motivbhv_typ: null,
    motivbhv_desc: "",
    h4h_interest: null,
    influ_screen: "Please select",
    influ_screen_less: "",
    msg_insenstv: "Please select",
    msg_insenstv_desc: "",
    msg_stigma: "Please select",
    msg_stigma_desc: "",
  },
  preludeToOverallMessaging: {},
  programEvalOverallMessaging: {
    h4h_think: null,
    h4h_persrelevant: null,
    h4h_motivates: null,
    h4h_concerned: null,
    h4h_easyundrstnd: null,
    h4h_believable: null,
  },
  programEvalOpinion: {
    h4h_negattitudes: "Please select",
    h4h_negattitudes_desc: "",
    h4h_blame: "Please select",
    h4h_blame_desc: "",
    h4h_simple: "Please select",
    h4h_simple_desc: "",
  },
  programEvalFeedback: {
    h4h_suggest: "",
  },
  thankyou: {},
};

const followupSurveyDefaultStateControl: SurveyState<
  typeof followupSurveyControl
> = {
  aboutYou: {
    first_name: "",
    last_name: "",
    dob: "",
  },
  preludeToPastMonthBehaviour: {},
  pastMonthBehaviour: {
    bhvfreq_phys: null,
    bhvquant_phys_mins: "",
    bhvfreq_sun: null,
  },
  pastMonthAlcohol: {
    bhvfreq_alc: null,
    bhvDrinkOnDay: {
      bhvquant_avred: 0,
      bhvquant_avwhite: 0,
      bhvquant_avsparkle: 0,
      bhvquant_smlred: 0,
      bhvquant_smlwhite: 0,
      bhvquant_shot: 0,
      bhvquant_cocktail: 0,
      bhvquant_rtd: 0,
      bhvquant_beer: 0,
    },
  },
  pastMonthSecondaryBehaviour: {
    bhvfreq_cig: null,
    bhvfreq_veg: null,
  },
  preludeToOutcomes: {},
  outcomes: {
    int_phys: null,
    int_sun: null,
    int_alc: null,
    int_cig: null,
    int_veg: null,
  },
  preludeToSecondaryOutcomes: {},
  secondaryOutcomes: {
    intfreq_phys: null,
    intquant_phys_mins: "",
    intfreq_sun: null,
  },
  secondaryOutcomesPart2: {
    intfreq_alc: null,
    intDrinkOnDay: {
      intquant_avred: 0,
      intquant_avwhite: 0,
      intquant_avsparkle: 0,
      intquant_smlred: 0,
      intquant_smlwhite: 0,
      intquant_shot: 0,
      intquant_cocktail: 0,
      intquant_rtd: 0,
      intquant_beer: 0,
    },
  },
  secondaryOutcomesPart3: {
    intfreq_cig: null,
    intfreq_veg: null,
  },
  preludeToKnowledge: {},
  riskKnowledge: {
    bcrisk_famhist: null,
    bcrisk_phys: null,
    bcrisk_sunexpo: null,
    bcrisk_alc: null,
    bcrisk_cig: null,
    bcrisk_ovweight: null,
  },
  knowledge: {
    knowl_phys: null,
    knowl_sunscr: null,
    knowl_sd: null,
    knowl_redwine: null,
    knowl_alcguideline: null,
    knowl_diet: null,
  },
  programEval: {
    h4h_infoaccept: null,
    h4h_stdservice: null,
    h4h_easyuse: null,
    h4h_timeaccept: null,
    h4h_animaccept: null,
  },
  programEvalSharing: {
    infoshare: null,
    infoshare_who: null,
    infoshare_typ: null,
  },
  programEvalMessaging: {
    h4hmsg_newinfo: null,
    h4hmsg_easy: null,
    h4hmsg_trustw: null,
    h4hmsg_relevant: null,
    h4hmsg_useful: null,
  },
  programEvalMotivation: {
    motivbhv: null,
    motivbhv_typ: null,
    motivbhv_desc: "",
    h4h_interest: null,
    influ_screen: "Please select",
    influ_screen_less: "",
    msg_insenstv: "Please select",
    msg_insenstv_desc: "",
    msg_stigma: "Please select",
    msg_stigma_desc: "",
  },
  programEvalFeedback: {
    h4h_suggest: "",
  },
  thankyou: {},
};

const defaultFollowupProgress = {
  sectionTimestamps: {
    aboutYou: { startedAt: Timestamp.now(), completedAt: null },
  },
  page: "aboutYou" as keyof typeof followupSurveyIntervention,
};

export const FollowupSurveyCard = (props: {
  appEnv: AppEnv;
  userId: string;
  progress: FollowupSurveyProgress | undefined;
  state: FollowupSurveyResponses | undefined;
  metadata: SurveyMetadata;
  signoutMutation: UseMutationResult<void, unknown, void, unknown>;
  referenceId: string;
  arm: Arm;
}) => {
  const queryClient = useQueryClient();
  const saveFollowupMutation = useSaveFollowup({
    onSuccess: () => {
      invalidateFollowupSurveyProgress(
        envToCollectionTypes[props.appEnv].FollowupSurveyProgress,
        queryClient,
        props.userId
      );
    },
  });

  useEffect(() => {
    if (props.userId && !props.referenceId) {
      props.signoutMutation.mutate();
    }
  }, []);

  const followupSurvey =
    props.arm === "intervention"
      ? followupSurveyIntervention
      : followupSurveyControl;
  const followupSurveyState =
    props.arm === "intervention"
      ? followupSurveyDefaultStateIntervention
      : followupSurveyDefaultStateControl;

  // Followup Survey Section Progress
  const followupSurveyProgressSections = [
    { id: 1, label: "About you", count: 1 },
    { id: 2, label: "Past month", count: 4 },
    { id: 3, label: "Next month", count: 6 },
    { id: 4, label: "Current knowledge", count: 3 },
    {
      id: 5,
      label: "Program Evaluation",
      count: getPageCountForProgramEvaluationSection(followupSurveyState),
    },
    { id: 6, label: "Done", count: 1 },
  ];

  return (
    <Card className="max-w-6xl">
      <FollowupSurveyForm
        survey={followupSurvey}
        defaultStateWithoutProgress={followupSurveyState}
        defaultState={props.state ?? followupSurveyState}
        defaultProgress={props.progress ?? defaultFollowupProgress}
        progressSections={followupSurveyProgressSections}
        saving={saveFollowupMutation.isLoading}
        onComplete={(s, isCompleted, progress) => {
          if (progress?.page !== "aboutYou") {
            saveFollowupMutation.mutate({
              followupSurveyCollectionType:
                envToCollectionTypes[props.appEnv].FollowupSurvey,
              followupSurveyProgressCollectionType:
                envToCollectionTypes[props.appEnv].FollowupSurveyProgress,
              userId: props.userId,
              responses: s,
              isCompleted: isCompleted,
              progress: progress,
              metadata: props.metadata,
              referenceId: props.referenceId,
            });
          }

          if (isCompleted) {
            props.signoutMutation.mutate();
          }
        }}
      />
    </Card>
  );
};
