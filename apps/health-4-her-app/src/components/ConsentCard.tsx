import { useCreateAnonymousUser } from "@/mutations/useCreateAnonymousUser";
import { Button } from "./Button";
import { Card } from "./Card";
import { useQueryClient } from "react-query";
import { invalidateAuth } from "@/queries/useAuth";
import { Loading } from "./Loading";
import { ErrorToast } from "./ErrorToast";
import { ReactNode, useState } from "react";
import Image from "next/image";
import { Modal } from "./Modal";
import PISForm from "./PISForm";
import { AppEnv } from "@/types";

const info = [
  {
    title: "Project number",
    text: "LR22-071-91112 (Eastern Health Human Research Ethics Committee)",
  },
  {
    title: "Principal Investigator",
    text: "Dr Jasmin Grigg",
  },
  {
    title: "Associate Investigators",
    text: "Ms Peta Stragalinos, Dr Darren Lockie, Ms Michelle Giles, Prof Robin Bell, Dr Alex Waddell, Mr Joshua Seguin, Dr Ling Wu, Dr Jue Xie, Dr Chris Greenwood, Dr Bosco Rowland, A/Prof Victoria Manning, Prof Dan Lubman",
  },
  {
    title: "Location",
    text: "Maroondah BreastScreen",
  },
];

type Props = {
  onNextClick: () => void;
};

export const ConsentCard: React.FC<Props> = ({ onNextClick }) => {
  const queryClient = useQueryClient();
  const createAnonymousUser = useCreateAnonymousUser({
    onSuccess: () => {
      invalidateAuth(queryClient);
    },
  });
  const [openPIS, setOpenPIS] = useState(false);

  const handleSubmit = async () => {
    createAnonymousUser.mutate();
    onNextClick();
  };

  const description: ReactNode = (
    <div className="space-y-5">
      <p>
        <strong>Hello!</strong>
      </p>
      <p>
        The Health4Her study is comparing two types of health promotion offered
        to women attending breast screening.
      </p>
      <p>
        Today, we will ask you some questions about <strong>yourself</strong>,
        your <strong>past-month lifestyle behaviours</strong>, your{" "}
        <strong>next-month behaviour-related intentions</strong>, and some{" "}
        <strong>knowledge</strong> questions. This will take about 15 minutes to
        complete.
      </p>
      <p>
        We will then show you an <strong>animation</strong>, with some{" "}
        <strong>brief activities</strong> and <strong>questions</strong>.
      </p>
      <p>
        Then, in four weeks’ time, we will send you some further questions to
        complete.
      </p>
      <p>
        Any information that we collect which can identify you will remain{" "}
        <strong>strictly confidential</strong>. Maroondah BreastScreen and
        Eastern Health will not have access to any information you provide.
      </p>
      <p>
        Participation is <strong>entirely voluntary</strong>. If you don’t wish
        to take part, you don’t have to. Whether or not you choose to take part
        will not affect your relationship with Maroondah BreastScreen, Eastern
        Health or Monash University.
      </p>
      <p>
        You can view the full{" "}
        <span className="underline-pink text-blue-600 underline">
          <a onClick={() => setOpenPIS(true)} className="cursor-pointer">
            Participant Information Sheet
          </a>
        </span>
        . We will also send you a copy.
      </p>
      <p>
        Thank you for participating in Health4Her, for your contribution we will
        pledge $10 to breast cancer research.
      </p>

      <div className="flex flex-wrap justify-center gap-5 py-5">
        <Image
          src="/images/Logos.png"
          alt="Organisation Logos"
          width={1000}
          height={1000}
        />
      </div>
    </div>
  );

  return (
    <>
      <Modal
        open={openPIS}
        setOpen={setOpenPIS}
        className="w-5/6 bg-white sm:max-w-none"
      >
        <PISForm setShowPIS={setOpenPIS} />
      </Modal>
      <Card className="max-w-6xl">
        {createAnonymousUser.isError ? (
          <ErrorToast>Something went wrong, please try again</ErrorToast>
        ) : null}
        <div className="flex flex-col gap-5 p-5">
          {/* <div className="inline-flex flex-col gap-y-5 border-2 p-5">
          {info.map((item) => (
            <div key={item.title} className="grid grid-cols-3 gap-y-2">
              <p className="col-span-full font-bold md:col-span-1">
                {item.title}:
              </p>
              <p className="col-span-full md:col-span-2">{item.text}</p>
            </div>
          ))}
        </div> */}
          <div>{description}</div>
        </div>
        <div className="flex w-full gap-2 px-5">
          <Button
            onClick={handleSubmit}
            disabled={createAnonymousUser.isLoading}
            variant="filled"
            colour="pink"
            className="flex w-full justify-center"
          >
            {createAnonymousUser.isLoading ? (
              <Loading className="mx-3 text-white" />
            ) : (
              <span className="my-2">Next</span>
            )}
          </Button>
        </div>
      </Card>
    </>
  );
};
