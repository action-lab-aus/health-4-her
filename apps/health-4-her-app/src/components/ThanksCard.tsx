import { Card } from "./Card";
import { Button } from "./Button";
import { Loading } from "./Loading";
import { UseMutationResult, UseQueryResult } from "react-query";
import { AppEnv } from "@/types";
import { useEffect } from "react";

export const ThanksCard = (props: {
  appEnv: AppEnv;
  signOutMutation: UseMutationResult<void, unknown, void, unknown>;
  scheduledNotificationsQuery: UseQueryResult<
    | {
        referenceId: string;
      }
    | undefined,
    unknown
  >;
}) => {
  const handleDoneButtonClick = () => {
    props.signOutMutation.mutate();
  };

  useEffect(() => {
    props.scheduledNotificationsQuery.refetch();
  }, [props.scheduledNotificationsQuery]);

  useEffect(() => {
    const handleBeforeUnload = () => {
      props.signOutMutation.mutate();
    };

    window.addEventListener("beforeunload", handleBeforeUnload);

    return () => {
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };
  }, [props.signOutMutation]);

  return (
    <>
      <Card className="flex items-center justify-center bg-current">
        <div className="flex flex-col items-center text-center text-pink-500">
          <p className="mt-24 text-lg font-bold md:text-2xl">
            Thank you for taking part in Health4Her.
          </p>
          <p className="text-lg md:text-2xl">
            You are making a valuable contribution to women&apos;s health
            research.
          </p>
          <p className="text-lg md:text-2xl">
            We will send you the final survey to complete for this research in
            4-weeks time.
          </p>
          {props.appEnv !== AppEnv.PILOT && (
            <p className="text-md mt-10 md:text-xl">
              Please return this device to the researcher.
            </p>
          )}
          {props.appEnv === AppEnv.DEV && (
            <p>
              Reference ID:{" "}
              {props.scheduledNotificationsQuery.data?.referenceId}
            </p>
          )}
        </div>
        <div>
          <Button
            onClick={handleDoneButtonClick}
            variant="filled"
            className="mt-5 cursor-pointer border-0 p-5 text-white"
            colour="pink"
            type="button"
          >
            {props.signOutMutation.isLoading ? (
              <Loading />
            ) : (
              <span className="mx-auto flex gap-2">Done</span>
            )}
          </Button>
        </div>
      </Card>
    </>
  );
};
