import { ColourPrefixesWithVariant } from "@/theme";
import { useState } from "react";
import { twMerge } from "tailwind-merge";
import { Button } from "./Button";
import { Chevron } from "./icons/Chevron";

export type InputColours =
  | (ColourPrefixesWithVariant<"500"> & ColourPrefixesWithVariant<"600">)
  | undefined;

const colourStyles = {
  pink: "border-pink-500 focus:outline-pink-600",
  black: "border-black-500 focus:outline-black-600",
  "dark-pink": "border-dark-pink-500 focus:outline-dark-pink-600",
  "light-pink": "border-light-pink-500 focus:outline-light-pink-600",
} satisfies Record<Exclude<InputColours, undefined>, string>;

export const NumberInput = (props: {
  colour: InputColours;
  id?: string;
  className?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement> | undefined;
  onArrowsClick: () => {
    up: () => void;
    down: () => void;
  };
  value?: string | undefined;
  pattern?: string;
  disabled?: boolean | undefined;
  placeholder?: string;
  required?: boolean;
  min?: number;
  max?: number;
}) => {
  const [isUpBtnClicked, setIsUpBtnClicked] = useState(false);
  const [isDownBtnClicked, setIsDownBtnClicked] = useState(false);

  const handleUpClick = () => {
    props.onArrowsClick().up();
    setIsUpBtnClicked(true);
    setTimeout(() => setIsUpBtnClicked(false), 200); // reset after 200ms
  };
  const handleDownClick = () => {
    props.onArrowsClick().down();
    setIsDownBtnClicked(true);
    setTimeout(() => setIsDownBtnClicked(false), 200); // reset after 200ms
  };

  return (
    <div className={twMerge(props.className, "relative")}>
      <div className="z-10 flex flex-col items-center rounded-xl border border-pink-600">
        <Button
          type="button"
          colour="dark-pink"
          variant="filled"
          className={`rounded-full transition-transform duration-200 ${
            isUpBtnClicked ? "scale-110" : ""
          }`}
          onClick={handleUpClick}
        >
          <Chevron direction="up" className="h-8 w-8" />
        </Button>
        <input
          className={twMerge(
            "w-full border-b border-t p-2",
            props.colour ? colourStyles[props.colour] : undefined
          )}
          onChange={props.onChange}
          value={props.value}
          id={props.id}
          type="number"
          inputMode="numeric"
          pattern={props.pattern}
          name={props.id}
          disabled={props.disabled}
          placeholder={props.placeholder}
          required={props.required}
          min={props.min}
          max={props.max}
        />
        <Button
          type="button"
          colour="dark-pink"
          variant="filled"
          className={`rounded-full transition-transform duration-200 ${
            isDownBtnClicked ? "scale-110" : ""
          }`}
          onClick={handleDownClick}
        >
          <Chevron direction="down" className="h-8 w-8" />
        </Button>
      </div>
    </div>
  );
};
