import { useEffect, useState } from "react";
import { twJoin } from "tailwind-merge";

const CheckMark = () => {
  return (
    <svg
      className="h-3 w-3 text-white dark:text-blue-300 lg:h-3 lg:w-3"
      aria-hidden="true"
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 16 12"
    >
      <path
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M1 5.917 5.724 10.5 15 1.5"
      />
    </svg>
  );
};

export const ProgressIndicator = (props: {
  progress: number;
  progressType?: "baseline" | "followup";
  step?: number;
  currentSectionIndex?: number;
  progressSections?: { id: number; label: string; count: number }[];
  indeterminate?: boolean;
  className?: string;
}) => {
  const [hideLinearProgressBar, setHideLinearProgressBar] = useState(
    !!props.progressSections
  );

  useEffect(() => {
    setHideLinearProgressBar(!!props.progressSections);
  }, [props.progressSections]);

  const calculateProgress = (
    currentSectionIndex: number,
    progressSections: number[]
  ): number => {
    if (props.step == undefined) return 0;

    const previousSectionsCount = progressSections
      .slice(0, props.step)
      .reduce((sum, num) => sum + num, 0);

    const progressInCurrentSection =
      currentSectionIndex - previousSectionsCount;

    return (progressInCurrentSection / progressSections[props.step]!) * 100;
  };

  const NOWRAP_LENGTH = 15;

  return (
    <>
      {props.progressSections != undefined &&
      props.currentSectionIndex != undefined &&
      props.step != undefined ? (
        <ol className="flex w-full overflow-hidden pb-8 pt-16">
          {props.progressSections?.map((step, index) => {
            const isFirstItem = index === 0;
            const isLastItem = index === props.progressSections!.length - 1;
            const isSmallerThanSM = window.innerWidth <= 768;
            const progress = calculateProgress(
              props.currentSectionIndex ?? 0,
              props.progressSections?.map((p) => p.count) ?? []
            );

            return (
              <li
                key={step.id}
                className={`text-light-pink-600 relative flex w-full translate-x-1/3 items-center`}
              >
                <span className="relative flex flex-col-reverse items-center text-center">
                  <span className="text-dark-pink-600 flex h-5 w-5 shrink-0 items-center justify-center rounded-full bg-pink-600">
                    {props.step && index < props.step ? (
                      <CheckMark />
                    ) : (
                      <span
                        className={`mx-auto h-3 w-3 rounded-full ${
                          props.step != undefined && index <= props.step
                            ? "bg-light-pink-600"
                            : "bg-white"
                        }`}
                      />
                    )}
                  </span>
                  <p
                    className={`absolute -top-14 ${
                      props.step === index
                        ? "text-dark-pink-600"
                        : "hidden md:block"
                    } ${isFirstItem && isSmallerThanSM ? "ml-14" : ""} ${
                      isLastItem && isSmallerThanSM ? "mr-14" : ""
                    } ${
                      step.label.length > NOWRAP_LENGTH
                        ? "whitespace-normal"
                        : "whitespace-nowrap"
                    } text-left text-sm`}
                  >
                    {step.label}
                  </p>
                </span>

                {/* Progress line */}
                {index < props.progressSections!.length - 1 && (
                  <div className="bg-light-pink-500 relative h-1 w-full">
                    {props.step != undefined &&
                      props.currentSectionIndex != undefined &&
                      props.progressSections != undefined &&
                      index <= props.step && (
                        <div
                          style={{
                            width: `${
                              props.indeterminate
                                ? 100
                                : index < props.step
                                ? 100
                                : calculateProgress(
                                    props.currentSectionIndex,
                                    props.progressSections?.map((p) => p.count)
                                  )
                            }%`,
                          }}
                          className="transition-width absolute h-1 origin-top-left bg-pink-500 ease-in-out"
                        />
                      )}
                  </div>
                )}
              </li>
            );
          })}
        </ol>
      ) : undefined}
      {/* Old Progress Indicator */}
      {!hideLinearProgressBar ? (
        <div
          className={twJoin(
            "bg-light-pink-500 h-2 overflow-hidden rounded-md",
            props.className
          )}
        >
          <div
            style={{
              width: `${props.indeterminate ? 100 : props.progress * 100}%`,
            }}
            className={twJoin(
              "bg-dark-pink-500 transition-width z-10 h-2 origin-top-left rounded-md duration-200 ease-in-out",
              props.indeterminate
                ? "animate-indeterminate-animation"
                : undefined
            )}
          />
        </div>
      ) : undefined}
    </>
  );
};
