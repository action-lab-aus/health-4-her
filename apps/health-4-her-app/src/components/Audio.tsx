import { useEffect, useState } from "react";
import { Button } from "./Button";
import { usePlayPauseState } from "@/usePlayPauseState";

export const Audio = (props: {
  replay: React.ReactNode;
  playing: React.ReactNode;
  paused: React.ReactNode;
  src: string;
}) => {
  const { playerRef, playing } = usePlayPauseState();
  const [ended, setEnded] = useState(false);

  const handleClick = () => {
    if (playerRef.current?.ended) {
      playerRef.current.currentTime = 0;
      playerRef.current?.play();
      setEnded(false);
    } else if (playerRef.current?.paused) {
      playerRef.current?.play();
    } else {
      playerRef.current?.pause();
    }
  };

  // const handleClick = () => {
  //   if (playerRef.current?.paused) {
  //     playerRef.current?.play();
  //   } else {
  //     playerRef.current?.pause();
  //   }
  // };

  useEffect(() => {
    playerRef.current?.load();
    // This will likely fail on first load, but will succeed if the src changes
    // Let's just swallow the error - it's just because chrome/safari/etc. don't
    // like us trying to play audio before the user interacts with the page

    const handleEnded = () => {
      setEnded(true);
    };

    playerRef.current?.addEventListener("ended", handleEnded);
    playerRef.current?.play().catch(() => {});

    // Clean up the event listener when the component is unmounted
    return () => {
      playerRef.current?.removeEventListener("ended", handleEnded);
    };
  }, [playerRef, props.src]);

  return (
    <>
      <audio ref={playerRef}>
        <source src={props.src} type="audio/mp3" />
      </audio>
      <Button
        onClick={handleClick}
        className="text-3xl"
        unstyled
        variant="filled"
        colour="pink"
      >
        {/* {playing ? props.playing : props.paused} */}
        {ended ? props.replay : playing ? props.playing : props.paused}
      </Button>
    </>
  );
};
