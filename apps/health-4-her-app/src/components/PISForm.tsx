import React, { Fragment, ReactNode, useEffect, useState } from "react";
import Image from "next/image";
import { Button } from "./Button";
import { Consent } from "@/types";
import { Timestamp } from "firebase/firestore";
import { useSaveConsent } from "@/mutations/useSaveConsent";

type Props = {
  hasConsented?: boolean;
  setHasConsented?: React.Dispatch<React.SetStateAction<boolean>>;
  setShowPIS?: React.Dispatch<React.SetStateAction<boolean>>;
  consent?: Consent;
  setConsent?: React.Dispatch<React.SetStateAction<Consent>>;
};

const summary = [
  {
    id: 1,
    title: "Title",
    value:
      "Randomised controlled trial of an iPad-delivered health promotion activity for women attending breast screening services",
  },
  {
    id: 2,
    title: "Project ID",
    value: "91112",
  },
  {
    id: 3,
    title: "Project Sponsor",
    value: "Eastern Health",
  },
  {
    id: 4,
    title: "Principal Investigator",
    value: "Dr Jasmin Grigg",
  },
  {
    id: 5,
    title: "Co-investigators",
    value:
      "Ms Peta Stragalinos, Dr Darren Lockie, Ms Michelle Giles, Prof Robin Bell, Dr Alex Waddell, Mr Joshua Seguin, Dr Ling Wu, Dr Jue Xie, Mr Christopher Prawira, Dr Bosco Rowland, Dr Chris Greenwood, Prof Victoria Manning, Prof Dan Lubman",
  },
  { id: 9, title: "Location", value: "Maroondah BreastScreen" },
];

const faq: { question: string; answer: ReactNode }[] = [
  {
    question: "What is the study about?",
    answer: (
      <span>
        You are invited to take part in a study comparing two types of
        iPad-delivered health promotion received by women in the context of
        their breast screening appointment. The health promotion is brief and
        will be provided to you after your scheduled appointment.
        <br />
        <br />
        This Participant Information Sheet tells you about the study. It
        explains what is involved in taking part. Knowing what is involved will
        help you decide if you want to take part.
      </span>
    ),
  },
  {
    question: "Who is conducting this study?",
    answer: (
      <span>
        The study is being conducted by researchers from Monash University and
        Eastern Health, and is funded by Shades of Pink. This study has been
        approved by the Eastern Health Human Research Ethics Committee.
      </span>
    ),
  },
  {
    question: "Who is being included in this study?",
    answer: (
      <span>
        Women attending Maroondah BreastScreen for routine mammography, aged 40+
        years, and who have access to computer, tablet or smartphone (to
        complete a follow-up online survey 4-weeks later) will be invited to
        participate in the study. Participation is entirely voluntary. If you
        don’t wish to take part, you don&apos;t have to. Whether or not you
        choose to take part will not affect your relationship with Maroondah
        BreastScreen, Eastern Health or Monash University.
      </span>
    ),
  },
  {
    question: "What does participation in this study involve?",
    answer: (
      <span>
        If you choose to take part, after your screening appointment you will
        self-complete some brief baseline questions about yourself (e.g. age,
        education level) and your lifestyle (e.g. health behaviours) before
        assigned by chance to one of two groups, who receive a slightly
        different women&apos;s health promotion activity. You will not know
        which group you are allocated to. The results for each group will be
        compared to see if one health promotion is more helpful to women
        attending the breast screening setting.{" "}
        <i>(Participation time: approximately 15 minutes)</i>.
        <br />
        <br />
        Four weeks later, you will be sent a link via email or SMS (as you
        prefer) to a brief online survey to complete. This will involve you
        self-completing some further brief questions about your lifestyle (e.g.
        health behaviours) and the health promotion activity you received. We
        will follow up with you by email or telephone if we haven’t received a
        response from you, and can provide you with assistance to complete the
        survey as needed. (Participation time: approximately 15 minutes).
        <br />
        <br />
        You can also elect to provide more in-depth feedback about the health
        promotion activity you received, if you are selected to do so. This
        interview will occur by telephone and will be audio-recorded.
        (Participation time: approximately 30 minutes)
      </span>
    ),
  },
];

const faq1: { question: string; answer: ReactNode }[] = [
  {
    question: "Reimbursement",
    answer: (
      <span>
        For your participation in this study, we will pledge $10 to breast
        cancer research (and an additional $10 for participation in the
        additional telephone interview).
      </span>
    ),
  },
  {
    question: "What are the possible benefits and risks of taking part?",
    answer: (
      <span>
        We cannot guarantee or promise that participants will experience any
        benefits from this research. However, brief health promotion has been
        shown to be effective in improving health behaviours, and as such, we
        are trying to understand their potential benefits when implemented in a
        breast screen setting.
        <br />
        <br />
        The risks of harm or discomfort to participants are anticipated to be
        minor and transient (e.g. some distress or discomfort may be experienced
        discussing health or health behaviours). If you wish to discontinue the
        study due to feelings of discomfort or distress you can stop
        participating at any time. Below are a list of support services that you
        can contact if you need.
      </span>
    ),
  },
  {
    question: "What if I want to withdraw from the evaluation?",
    answer: (
      <span>
        If you do consent to participate, you may withdraw at any time. You can
        withdraw by not completing the surveys or informing us of your
        withdrawal from the study by contacting a member of the research team of
        trial manager (contact details provided below). If you decide to
        withdraw from the evaluation, no further information will be collected
        from you, although the information already collected will be retained.
        If you do not want your data to be included, you must tell the
        researchers when you withdraw from the research project.
      </span>
    ),
  },
  {
    question: "What will happen to information about me?",
    answer: (
      <span>
        By providing consent, you consent to the research team collecting and
        using personal information about you for the research project. Any
        information that we collect which can identify you will remain
        <strong> strictly confidential</strong>.
        <br />
        <br />
        If you agree to the optional telephone feedback interview and are
        selected, your audio-recording will be transcribed, and the
        transcription will be anonymised – this means that any identifying
        information from the interview (e.g. your name) will be deleted from the
        transcripts of the audio-recordings. Quotes may be used to illustrate
        themes from the data but will be chosen carefully and used in
        conjunction with a pseudonym to maintain your anonymity. Demographic
        data will be reported only in aggregate form (e.g. frequencies,
        averages, proportions) to preserve your anonymity. Data will be
        presented anonymously in publications/presentations arising from this
        project.
        <br />
        <br />
        All of your information will be kept strictly confidential and will only
        be disclosed with your permission, except when it is subject to
        inspection (for the purposes of verifying the procedures and the data)
        by the authorised representatives of the Human Research Ethics Committee
        (HREC) of Eastern Health, or as required by law (e.g. in response to a
        court order). You will be notified if such disclosure is ever required.
        You can always request a copy of the information we have about you. Your
        audio-recording and associated data (e.g. transcription of the
        audio-recording) will be stored securely for at least 5 years after the
        end of the project, after which time it will be destroyed.
      </span>
    ),
  },
  {
    question: "Complaints",
    answer: (
      <span>
        If you have any complaints about any aspect of the study or the way in
        which it is being conducted, you may contact the Eastern Health Office
        of Research and Ethics on (03) 9895 3398 or &nbsp;
        <a
          className="text-blue-600 underline"
          href="mailto:ethics@easternhealth.org.au"
        >
          ethics@easternhealth.org.au
        </a>
      </span>
    ),
  },
  {
    question: "Further information and who to contact",
    answer: (
      <span>
        If you want any further information concerning this project or if you
        have any problems which may be related to your involvement in the
        project, you may contact the Principal Investigator, Dr Jasmin Grigg on
        (03) 8413 8723 or{" "}
        <a
          href="mailto:jasmin.grigg@monash.edu"
          className="text-blue-600 underline"
        >
          jasmin.grigg@monash.edu
        </a>
      </span>
    ),
  },
  {
    question: "Support services that you can contact if you need at any time",
    answer: (
      <span>
        Lifeline 24-hour crisis support: 13 11 14
        <br />
        Beyond Blue 24-hour support for depression and anxiety: 1300 224 636
        <br />
        1800RESPECT 24-hour support for people impacted by family or sexual
        violence: 1800 737 732
        <br />
        Blue Knot Trauma Helpline: 1300 657 380
        <br />
        DirectLine 24-hour alcohol and drug support: 1800 888 236
      </span>
    ),
  },
];

const CloseBtn: React.FC<Pick<Props, "setShowPIS">> = ({ setShowPIS }) => (
  <Button
    variant="filled"
    onClick={() => setShowPIS!(false)}
    colour="light-pink"
    className="text-white"
  >
    <span>Close</span>
  </Button>
);

const PISForm: React.FC<Props> = ({
  hasConsented,
  setHasConsented,
  setShowPIS,
  consent,
  setConsent,
}) => {
  const saveConsentMutation = useSaveConsent();
  const [surveyConsent, setSurveyConsent] = useState<boolean | undefined>(
    undefined
  );
  const [postInterventionSurveyConsent, setPostInterventionSurveyConsent] =
    useState<boolean | undefined>(undefined);
  const [telephoneInterviewConsent, setTelephoneInterviewConsent] = useState<
    boolean | undefined
  >(undefined);

  useEffect(() => {
    window.scrollTo({ left: 0, top: 0, behavior: "smooth" });
  }, []);

  useEffect(() => {
    const isConsentValid =
      surveyConsent &&
      postInterventionSurveyConsent &&
      telephoneInterviewConsent !== undefined;
    if (isConsentValid && setHasConsented && setConsent) {
      setHasConsented(true);

      const newConsent = {
        surveyAndActivity: surveyConsent,
        followUpSurvey: postInterventionSurveyConsent,
        telephoneInterview: telephoneInterviewConsent,
        consentedAt: Timestamp.now(),
      };
      setConsent(newConsent);
    }
  }, [
    surveyConsent,
    setSurveyConsent,
    postInterventionSurveyConsent,
    setPostInterventionSurveyConsent,
    telephoneInterviewConsent,
    setTelephoneInterviewConsent,
    setHasConsented,
    setConsent,
  ]);

  return (
    <div className="p-10 text-center">
      <div className="text-right">
        <CloseBtn setShowPIS={setShowPIS} />
      </div>
      <h1 className="text-xl font-bold">Participant Information Sheet</h1>
      <p>
        <span className="font-bold">Health4Her study</span> - Adult providing
        own consent
      </p>
      <div className="mt-5 border">
        <div className="grid grid-cols-2 justify-items-start gap-3 p-3 md:grid-cols-4">
          {summary.map((item) => (
            <Fragment key={item.id}>
              <div className="col-span-full text-left font-bold md:col-span-1">
                {item.title}
              </div>
              <div className="col-span-3 text-left">{item.value}</div>
            </Fragment>
          ))}
        </div>
      </div>
      <div className="mt-14">
        {faq.map((item) => (
          <div key={item.question} className="mb-10 mt-5 text-left">
            <div className="font-bold">{item.question}</div>
            <div className="mt-3">{item.answer}</div>
          </div>
        ))}
      </div>
      <div className="flex justify-center">
        <Image
          src="/images/pis-image.png"
          width={1500}
          height={1500}
          alt="Study workflow"
        />
      </div>
      <div className="mt-14">
        {faq1.map((item) => (
          <div key={item.question} className="mb-10 mt-5 text-left">
            <div className="font-bold">{item.question}</div>
            <div className="mt-3">{item.answer}</div>
          </div>
        ))}
      </div>
      <div className="mt-5">
        <p className="text-left">
          I have been asked to take part in the Eastern Health research project
          specified above. I have read and understood the explanatory statement,
          and I hereby consent to participate in this project.
        </p>
      </div>
      <div className="-mx-4 mt-10 border sm:mx-0">
        <table className="min-w-full table-auto divide-y divide-gray-300 px-5 text-left">
          <thead>
            <tr>
              <th className="pl-5">I consent to the following:</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="pl-5">
                Taking part in a brief online survey and health promotion
                activity
              </td>
            </tr>
            <tr className="border-t pl-5">
              <td className="pl-5">
                Taking part in an online survey 4-weeks’ later
              </td>
            </tr>
            <tr className="border-t">
              <td className="pl-5">
                Being contacted for an (optional) telephone interview, if I am
                selected
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div className="mt-5 text-right">
        <CloseBtn setShowPIS={setShowPIS} />
      </div>
    </div>
  );
};

export default PISForm;
