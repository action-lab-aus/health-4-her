import { use, useEffect, useState } from "react";
import { Card } from "@/components/Card";
import { SurveyForm } from "./SurveyForm";
import {
  Survey,
  SurveyState,
  BaselineSurveyProgress,
  SurveyMetadata,
} from "@/surveys/types";
import { useSaveBaseline } from "@/mutations/useSaveBaseline";
import { UseMutationResult, useQueryClient } from "react-query";
import {
  AppEnv,
  Consent,
  ScheduledNotificationsCollectionType,
  envToCollectionTypes,
} from "@/types";
import { Timestamp } from "firebase/firestore";
import { BaselineSurveyResponses } from "@/services/firestore";
import { invalidateBaselineSurveyProgress } from "@/queries/baselineSurveyProgress";
import { formatToAUPrefix } from "@/utils";
import { invalidateUserProgress } from "@/queries/userProgress";
import { getScreeningNumber } from "@/services/functions";

const today = new Date();
const minYears = 40;
const maxYears = 110;

export const baselineSurveyPilot = {
  aboutYou: {
    className: "grid grid-cols-4 gap-5",
    questions: [
      {
        key: "intro",
        type: "element",
        content: (
          <div className="col-span-4 mb-5">
            <legend className="mb-5 text-xl font-semibold">About you</legend>
            <span>
              <strong>
                In this section, we will ask you some questions about yourself.
              </strong>
            </span>
            <p>
              Remember, your responses are confidential. Maroondah BreastScreen
              and Eastern Health will not have access to any information you
              provide.
            </p>
          </div>
        ),
      },
      {
        key: "first_name",
        type: "input",
        label: "First name *",
        className: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
      },
      {
        key: "last_name",
        type: "input",
        label: "Last name *",
        className: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
      },
      {
        key: "comm_pref",
        type: "dropdown",
        label: "Communication preferences *",
        options: [
          { label: "Email and SMS", value: "1" },
          { label: "Email only", value: "2" },
          { label: "SMS only", value: "3" },
        ] as const,
        wrapperClassName: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-start col-span-full sm:col-span-1 mt-2 sm:mt-0",
      },
      {
        key: "email",
        type: "input",
        label: "Email address *",
        inputType: "email",
        className: "col-span-full sm:col-span-3",
        notRenderedConds: [
          ["aboutYou", "comm_pref", "Please select"],
          ["aboutYou", "comm_pref", "3"],
        ] as const,
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
      },
      {
        key: "ph_mobile",
        type: "input",
        label: "Mobile number (AU) *",
        inputType: "tel",
        className: "col-span-full sm:col-span-3",
        notRenderedConds: [
          ["aboutYou", "comm_pref", "Please select"],
          ["aboutYou", "comm_pref", "2"],
        ] as const,
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0",
      },
      {
        key: "ph_home",
        type: "input",
        placeholder: "Please type",
        label: (
          <p>
            Home phone number{" "}
            <em>(just in case we need to get in touch with you)</em> *
          </p>
        ),
        inputType: "tel",
        className: "col-span-full sm:col-span-3",
        notRenderedConds: [
          ["aboutYou", "comm_pref", "Please select"],
          ["aboutYou", "comm_pref", "3"],
          ["aboutYou", "comm_pref", "1"],
        ] as const,
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 w-full",
      },
      {
        key: "postcode",
        type: "input",
        label: "Postcode *",
        inputType: "number",
        min: 1000,
        max: 9999,
        className: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
      },
      {
        key: "dob",
        type: "datepicker",
        className: "col-span-full sm:col-span-3 appearance-none",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
        label: "Date of birth *",
      },
      {
        key: "recruit",
        type: "dropdown",
        label: "How did you hear about health4her? *",
        options: [
          { label: "My appointment at Maroondah Breastscreen", value: "1" },
          { label: "Recommended by a friend, family or colleague", value: "2" },
          { label: "Search engine (e.g. Google)", value: "3" },
          { label: "Other", value: "4" },
        ] as const,
        wrapperClassName: "col-span-full sm:col-span-3",
        labelClassName: "flex col-span-full sm:col-span-1 mt-2 sm:mt-0 ",
        freeText: {
          property: "recruit_oth",
          value: "4",
          className: "col-span-full sm:col-span-3",
          label: "If other, please specify *",
        },
      },
      {
        key: "pilot_no",
        type: "input",
        label: "Please enter the code listed on your health4her card",
        placeholder: "Please type",
        required: false,
        className: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0",
      },
      {
        key: "prev_part",
        type: "dropdown",
        label: "Have you participated in the health4her study before? *",
        options: [
          { label: "No", value: "0" },
          { label: "Yes", value: "2" },
        ] as const,
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0",
        wrapperClassName: "col-span-full sm:col-span-3",
      },
    ] as const,
  },
  demographic: {
    questions: [
      {
        key: "demographicIntro",
        type: "element",
        content: (
          <>
            <legend className="mb-2 text-xl font-semibold">About You</legend>
            <span>
              We&apos;re interested to know whether we are reaching diverse
              groups, so the following questions help us with this. Your
              responses will be kept confidential and secure, and will not
              change your participation in any way.
            </span>
          </>
        ),
      },
      {
        key: "birthplace",
        type: "dropdown",
        label: "In which country were you born? *",
        options: [
          { label: "Australia", value: "0" },
          { label: "Other", value: "1" },
        ] as const,
        labelClassName: "mt-5",
        freeText: {
          property: "birthplace_oth",
          value: "1",
          label: "If other, please specify *",
        },
      },
      {
        key: "lang",
        type: "dropdown",
        label: "Do you speak a language other than English at home? *",
        options: [
          { label: "No, English only", value: "0" },
          { label: "Yes", value: "1" },
        ] as const,
        labelClassName: "mt-5",
        freeText: {
          property: "lang_oth",
          value: "1",
          label:
            "If yes, which language do you mainly speak at home (If more than one, indicate the one spoken most often) *",
        },
      },
      {
        key: "atsi",
        label: "Are you of Aboriginal or Torres Strait Islander origin? *",
        type: "dropdown",
        labelClassName: "mt-5",
        options: [
          { label: "No", value: "0" },
          { label: "Aboriginal", value: "1" },
          { label: "Torres Strait Islander", value: "2" },
          { label: "Both Aboriginal and Torres Strait Islander", value: "3" },
        ] as const,
      },
      {
        key: "gender",
        label: "What gender do you identify as? *",
        type: "dropdown",
        labelClassName: "mt-5",
        options: [
          { label: "Woman", value: "1" },
          { label: "Man", value: "2" },
          { label: "I use a different term", value: "3" },
          { label: "Prefer not to answer", value: "4" },
        ] as const,
        freeText: {
          property: "gender_oth",
          value: "3",
          label: "Please specify *",
        },
      },
      {
        key: "sex",
        label: "What was your sex recorded at birth *",
        type: "dropdown",
        labelClassName: "mt-5",
        options: [
          { label: "Female", value: "1" },
          { label: "Male", value: "2" },
          { label: "I use a different term", value: "3" },
          { label: "Prefer not to answer", value: "4" },
        ] as const,
        freeText: {
          property: "sex_oth",
          value: "3",
          label: "Please specify *",
        },
      },
      {
        key: "lgbt",
        label: "Do you identify as part of the LGBTIQA+ community? *",
        type: "dropdown",
        labelClassName: "mt-5",
        options: [
          { label: "No", value: "0" },
          { label: "Yes", value: "1" },
          { label: "Prefer not to answer", value: "2" },
        ] as const,
      },
      {
        key: "edu",
        label: "What is the highest level of education you have completed *",
        type: "dropdown",
        labelClassName: "mt-5",
        options: [
          { label: "Year 11 or below", value: "1" },
          { label: "Year 12 or equivalent", value: "2" },
          { label: "Trade/apprenticeship", value: "3" },
          { label: "Associate/undergraduate diploma", value: "4" },
          { label: "Bachelor degree or higher", value: "5" },
        ] as const,
      },
      {
        key: "disable",
        label:
          "For at least the last 6-months, have you been limited because of a health problem (physical or mental health) in activities people usually do? *",
        type: "dropdown",
        labelClassName: "mt-5",
        options: [
          { label: "No, not limited", value: "0" },
          { label: "Yes, limited", value: "1" },
          { label: "Yes, strongly limited", value: "2" },
        ] as const,
      },
    ] as const,
  },
  preludeToPastMonthBehaviour: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We would now like to understand your lifestyle{" "}
                <span className="text-lg font-bold italic md:text-2xl">
                  behaviours
                </span>{" "}
                in the
                {"  "}
                <span className="text-lg font-bold underline md:text-2xl">
                  past month
                </span>
                .
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  pastMonthBehaviour: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "bhvfreq_phys",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>
              In the <strong>past month</strong>, how often did you engage in
              physical activity? *
            </p>
            <p>
              (For example, brisk walking, jogging, swimming, tennis, gardening
              or work around the house, riding a bicycle)
            </p>
          </div>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Occasionally, but not in the past month",
            value: "7",
          },
          { label: "Do not / cannot exercise", value: "8" },
        ] as const,
      },
      {
        key: "bhvquant_phys_mins",
        type: "input",
        label: (
          <span>
            In the <strong>past month</strong>, on a <em> typical</em> day that
            you engaged in physical activity, how many <u>minutes</u> did you
            do? *
          </span>
        ),
        placeholder: "Please type how many minutes",
        inputType: "text",
        className: "col-span-full sm:col-span-2",
        labelClassName:
          "flex items-center col-span-full sm:col-span-full mt-2 sm:mt-10",
        notRenderedConds: [["pastMonthBehaviour", "bhvfreq_phys", "8"]],
      },
      {
        key: "bhvfreq_sun",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            <p>
              In the <strong>past month</strong>, how often did you engage in
              sun protection behaviours? *
            </p>
            <p>
              (For example, wearing sunscreen, a hat, or other sun-protective
              clothing)
            </p>
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Less often",
            value: "7",
          },
        ] as const,
      },
    ] as const,
  },
  pastMonthAlcohol: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "bhvfreq_alc",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            In the <strong>past month</strong>, how often did you have an
            alcoholic drink of any kind? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "About 1 day per month",
            value: "6",
          },
          {
            label: "Occasionally, but not in the past month",
            value: "7",
          },
          {
            label: "Do not drink / no longer drink",
            value: "8",
          },
        ] as const,
      },
      {
        key: "bhvDrinkOnDay",
        type: "image-counter",
        notRenderedConds: [["pastMonthAlcohol", "bhvfreq_alc", "8"]],
        label: (
          <span>
            In the <strong>past month</strong>, on a <em>typical</em> day that
            you had an alcoholic drink, how many drinks did you have? *
          </span>
        ),
        options: [
          {
            value: "bhvquant_avred",
            image: "/images/drinks/RedWine_WideBig.png",
            label: "Average serve of red wine (150ml)",
          },
          {
            value: "bhvquant_avwhite",
            image: "/images/drinks/WhiteWine_WideBig.png",
            label: "Average serve of white wine (150ml)",
          },
          {
            value: "bhvquant_avsparkle",
            image: "/images/drinks/Champagne1.png",
            label: "Average serve of sparkling wine (150ml)",
          },
          {
            value: "bhvquant_smlred",
            image: "/images/drinks/RedWine_NarrowSmall.png",
            label: "Small serve of red wine (100ml)",
          },
          {
            value: "bhvquant_smlwhite",
            image: "/images/drinks/WhiteWine_NarrowSmall.png",
            label: "Small serve of white wine (100ml)",
          },
          {
            value: "bhvquant_shot",
            image: "/images/drinks/Scotch.png",
            label: "Shot/nip spirit (30ml) – with or without mixer",
          },
          {
            value: "bhvquant_cocktail",
            image: "/images/drinks/Cocktail1.png",
            label: "Cocktail",
          },
          {
            value: "bhvquant_rtd",
            image: "/images/drinks/pre-mix.png",
            label: "Ready-to-drink spirit (275ml)",
          },
          {
            value: "bhvquant_beer",
            image: "/images/drinks/BeerPot.png",
            label: "Beer or cider (375ml)",
          },
        ] as const,
      },
    ] as const,
  },
  pastMonthSecondaryBehaviour: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "bhvfreq_cig",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            In the <strong>past month</strong>, have you used any tobacco or
            e-cigarette products? *
          </span>
        ),
        options: [
          {
            label: "No",
            value: "0",
          },
          {
            label: "Yes, regularly (at least once per day)",
            value: "1",
          },
          {
            label: "Yes, not regularly (less than once per day)",
            value: "2",
          },
        ] as const,
      },
      {
        key: "bhvfreq_veg",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            In the <strong>past month</strong>, how often did you include
            vegetables as part of a meal? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "Less often",
            value: "6",
          },
        ] as const,
      },
    ] as const,
  },
  preludeToOutcomes: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We would now like to understand your behaviour-related{" "}
                <span className="text-lg font-bold italic md:text-2xl">
                  intentions
                </span>{" "}
                over the{" "}
                <span className="text-lg font-bold underline md:text-2xl">
                  next month
                </span>
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  outcomes: {
    className: "grid gap-5 w-full mt-5",
    questions: [
      {
        key: "int_phys",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to increase the amount of exercise you do? *
          </span>
        ),
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_sun",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to increase your sun protection practices? *
          </span>
        ),

        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_alc",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to reduce the amount of alcohol you consume? *
          </span>
        ),
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_cig",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to reduce any tobacco or e-cigarette use? *
          </span>
        ),
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_veg",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to increase the amount of vegetables you consume? *
          </span>
        ),

        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
    ] as const,
  },
  preludeToSecondaryOutcomes: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We will now ask you some further questions about your
                behaviour-related{" "}
                <span className="text-lg font-bold italic md:text-2xl">
                  intentions
                </span>{" "}
                over the{" "}
                <span className="text-lg font-bold underline md:text-2xl">
                  next month
                </span>
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  secondaryOutcomes: {
    questions: [
      {
        key: "intfreq_phys",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>
              Over the <strong>next month</strong>, how often do you intend to
              engage in physical activity? *
            </p>
            <p>
              (For example, brisk walking, jogging, swimming, tennis, gardening
              or work around the house, riding a bicycle)
            </p>
          </div>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Occasionally, but not in the next month",
            value: "7",
          },
          { label: "Do not / cannot exercise", value: "8" },
        ] as const,
      },
      {
        key: "intquant_phys_mins",
        type: "input",
        label: (
          <span>
            Over the <strong>next month</strong>, on a <em>typical</em> day that
            you engage in physical activity, how many <u>minutes</u> do you
            intend to do? *
          </span>
        ),
        placeholder: "Please type how many minutes",
        inputType: "text",
        className: "col-span-full sm:col-span-2",
        labelClassName: "flex items-center col-span-full sm:col-span-full",
        notRenderedConds: [["secondaryOutcomes", "intfreq_phys", "8"]],
      },
      {
        key: "intfreq_sun",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            <p>
              Over the <strong>next month</strong>, how often do you intend to
              engage in sun protection behaviours? *
            </p>
            <p>
              (For example, wearing sunscreen, a hat, or other sun-protective
              clothing)
            </p>
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Less often",
            value: "7",
          },
        ] as const,
      },
    ] as const,
  },
  secondaryOutcomesPart2: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "intfreq_alc",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            Over the <strong>next month</strong>, how often do you intend to
            have an alcoholic drink of any kind? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "About 1 day per month",
            value: "6",
          },
          {
            label: "Occasionally, but not in the next month",
            value: "7",
          },
          {
            label: "Do not drink / no longer drink",
            value: "8",
          },
        ] as const,
      },
      {
        key: "intDrinkOnDay",
        type: "image-counter",
        notRenderedConds: [
          ["secondaryOutcomesPart2", "intfreq_alc", "8"],
        ] as const,
        label: (
          <span>
            Over the <strong>next month</strong>, on a <em>typical </em> day
            that you have an alcoholic drink, how many drinks do you intend to
            have? *
          </span>
        ),
        options: [
          {
            value: "intquant_avred",
            image: "/images/drinks/RedWine_WideBig.png",
            label: "Average serve of red wine (150ml)",
          },
          {
            value: "intquant_avwhite",
            image: "/images/drinks/WhiteWine_WideBig.png",
            label: "Average serve of white wine (150ml)",
          },
          {
            value: "intquant_avsparkle",
            image: "/images/drinks/Champagne1.png",
            label: "Average serve of sparkling wine (150ml)",
          },
          {
            value: "intquant_smlred",
            image: "/images/drinks/RedWine_NarrowSmall.png",
            label: "Small serve of red wine (100ml)",
          },
          {
            value: "intquant_smlwhite",
            image: "/images/drinks/WhiteWine_NarrowSmall.png",
            label: "Small serve of white wine (100ml)",
          },
          {
            value: "intquant_shot",
            image: "/images/drinks/Scotch.png",
            label: "Shot/nip spirit (30ml) – with or without mixer",
          },
          {
            value: "intquant_cocktail",
            image: "/images/drinks/Cocktail1.png",
            label: "Cocktail",
          },
          {
            value: "intquant_rtd",
            image: "/images/drinks/pre-mix.png",
            label: "Ready-to-drink spirit (275ml)",
          },
          {
            value: "intquant_beer",
            image: "/images/drinks/BeerPot.png",
            label: "Beer or cider (375ml)",
          },
        ] as const,
      },
    ] as const,
  },
  secondaryOutcomesPart3: {
    questions: [
      {
        key: "intfreq_cig",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            Over the <strong>next month</strong>, do you intend to use any{" "}
            tobacco or e-cigarette products ? *
          </span>
        ),
        options: [
          {
            label: "No",
            value: "0",
          },
          {
            label: "Yes, regularly (at least once per day)",
            value: "1",
          },
          {
            label: "Yes, not regularly (less than once per day)",
            value: "2",
          },
        ] as const,
      },
      {
        key: "intfreq_veg",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            Over the <strong>next month</strong>, how often do you intend to
            include vegetables as part of a meal? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "Less often",
            value: "6",
          },
        ] as const,
      },
    ] as const,
  },
  preludeToKnowledge: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We would now like to understand your current knowledge of breast
                cancer risk factors. It&apos;s okay to be uncertain, just select
                &quot;unsure&quot;.
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  riskKnowledge: {
    className: "grid lg:grid-cols-6 gap-3 w-full lg:max-w-6xl",
    questions: [
      {
        key: "secondaryOutcomesPart3Intro",
        type: "element",
        content: (
          <>
            <div className="col-span-full">
              <div className="space-y-3">
                <p>
                  To what extent do you consider the following factors to be
                  associated with an increased risk of breast cancer?
                </p>
                <div className="flex flex-col gap-3 rounded-lg border p-2 lg:flex-row">
                  <div className="mb-2 grid grid-flow-col-dense p-2">
                    <p className="pl-3">
                      <span className="text-red-600">
                        <strong>Clear risk factor</strong>
                      </span>
                      : there is strong, consistent evidence that this factor
                      increases risk of breast cancer.
                    </p>
                  </div>
                  <div className="grid grid-flow-col-dense p-2">
                    <p className="pl-3">
                      <span className="text-light-pink-700">
                        <strong>Possible risk factor</strong>
                      </span>
                      : there is some evidence that this factor increases risk
                      of breast cancer, but not enough to be certain.
                    </p>
                  </div>
                  <div className="grid grid-flow-col-dense p-2">
                    <p className="pl-3">
                      <span>
                        <strong className="text-green-600">
                          Not a proven risk factor
                        </strong>
                      </span>
                      : the evidence is too limited to determine whether this
                      factor increases risk of breast cancer.
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="hidden lg:col-span-2 lg:block" />
            <div className="hidden text-center text-red-600 before:-translate-x-1 lg:flex lg:items-center lg:justify-center">
              Clear risk factor
            </div>
            <div className="text-light-pink-700 hidden text-center lg:flex lg:items-center lg:justify-center">
              Possible risk factor
            </div>
            <div className="hidden text-left text-green-600  lg:flex lg:items-center lg:justify-center">
              Not a proven risk factor
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Unsure
            </div>
          </>
        ),
      },
      {
        key: "bcrisk_famhist",
        label: "Family history of breast cancer *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_phys",
        label: "Physical inactivity *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_sunexpo",
        label: "Sun exposure *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_alc",
        label: "Drinking alcohol *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_cig",
        label: "Using tobacco products *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_ovweight",
        label: "Being overweight *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
    ] as const,
  },
  knowledge: {
    className: "grid lg:grid-cols-6 gap-3 w-full lg:max-w-6xl",
    questions: [
      {
        key: "knowl_phys",
        type: "button-group",
        label:
          "International guidelines recommend a minimum of _____ minutes of physical activity, five days a week. *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "30 minutes",
            value: "1",
          },
          {
            label: "45 minutes",
            value: "2",
          },
          {
            label: "60 minutes",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_sunscr",
        type: "button-group",
        label:
          "Sunscreen needs to be applied how many minutes before going outdoors? *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "20 minutes",
            value: "1",
          },
          {
            label: "30 minutes",
            value: "2",
          },
          {
            label: "60 minutes",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_sd",
        type: "button-group",
        label:
          "Drinking one average restaurant serve of wine a day increases breast cancer risk by... *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "4%",
            value: "1",
          },
          {
            label: "23%",
            value: "2",
          },
          {
            label: "61%",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_redwine",
        type: "button-group",
        label: "An average restaurant serve of red wine contains... *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "Less than 1 standard drink",
            value: "1",
          },
          {
            label: "1 standard drink",
            value: "2",
          },
          {
            label: "Closer to 2 standard drinks",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_alcguideline",
        type: "button-group",
        label:
          "Australian guidelines recommend that women should drink no more than _____ standard drinks per week *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "12 standard drinks",
            value: "1",
          },
          {
            label: "10 standard drinks",
            value: "2",
          },
          {
            label: "7 standard drinks",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_diet",
        type: "button-group",
        label:
          "Dietary guidelines advise that women consume more of which food group as they age?",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "Dairy",
            value: "1",
          },
          {
            label: "Vegetables",
            value: "2",
          },
          {
            label: "Protein",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
    ] as const,
  },
  preludeToActivities: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                Thank you for completing the survey. We&apos;d now like to show
                you some important health information.
              </p>
              <p className="mt-10 text-lg font-bold md:text-2xl">
                Please ensure your device has audio and volume enabled to watch
                the videos.
              </p>
              <p className="mt-10 text-lg font-bold md:text-2xl">
                You&apos;ll be able to pause the video at any time. Click
                refresh on your browser to restart the video.
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
} satisfies Survey;

export const baselineSurveyClinical = {
  aboutYou: {
    className: "grid grid-cols-4 gap-5",
    questions: [
      {
        key: "intro",
        type: "element",
        content: (
          <div className="col-span-4 mb-5">
            <legend className="mb-5 text-xl font-semibold">About you</legend>
            <span>
              <strong>
                In this section, we will ask you some questions about yourself.
              </strong>
            </span>
            <p>
              Remember, your responses are confidential. Maroondah BreastScreen
              and Eastern Health will not have access to any information you
              provide.
            </p>
          </div>
        ),
      },
      {
        key: "first_name",
        type: "input",
        label: "First name *",
        className: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
      },
      {
        key: "last_name",
        type: "input",
        label: "Last name *",
        className: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
      },
      {
        key: "comm_pref",
        type: "dropdown",
        label: "Communication preferences *",
        options: [
          { label: "Email and SMS", value: "1" },
          { label: "Email only", value: "2" },
          { label: "SMS only", value: "3" },
        ] as const,
        wrapperClassName: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-start col-span-full sm:col-span-1 mt-2 sm:mt-0",
      },
      {
        key: "email",
        type: "input",
        label: "Email address *",
        inputType: "email",
        className: "col-span-full sm:col-span-3",
        notRenderedConds: [
          ["aboutYou", "comm_pref", "Please select"],
          ["aboutYou", "comm_pref", "3"],
        ] as const,
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
      },
      {
        key: "ph_mobile",
        type: "input",
        label: "Mobile number (AU) *",
        inputType: "tel",
        className: "col-span-full sm:col-span-3",
        notRenderedConds: [
          ["aboutYou", "comm_pref", "Please select"],
          ["aboutYou", "comm_pref", "2"],
        ] as const,
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0",
      },
      {
        key: "ph_home",
        type: "input",
        placeholder: "Please type",
        label: (
          <p>
            Home phone number{" "}
            <em>(just in case we need to get in touch with you)</em> *
          </p>
        ),
        inputType: "tel",
        className: "col-span-full sm:col-span-3",
        notRenderedConds: [
          ["aboutYou", "comm_pref", "Please select"],
          ["aboutYou", "comm_pref", "3"],
          ["aboutYou", "comm_pref", "1"],
        ] as const,
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 w-full",
      },
      {
        key: "postcode",
        type: "input",
        label: "Postcode *",
        inputType: "number",
        min: 1000,
        max: 9999,
        className: "col-span-full sm:col-span-3",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
      },
      {
        key: "dob",
        type: "datepicker",
        className: "col-span-full sm:col-span-3 appearance-none",
        label: "Date of birth *",
        labelClassName:
          "flex items-center col-span-full sm:col-span-1 mt-2 sm:mt-0 whitespace-nowrap",
      },
    ] as const,
  },
  demographic: {
    questions: [
      {
        key: "demographicIntro",
        type: "element",
        content: (
          <>
            <legend className="mb-2 text-xl font-semibold">About You</legend>
            <span>
              We&apos;re interested to know whether we are reaching diverse
              groups, so the following questions help us with this. Your
              responses will be kept confidential and secure, and will not
              change your participation in any way.
            </span>
          </>
        ),
      },
      {
        key: "birthplace",
        type: "dropdown",
        label: "In which country were you born? *",
        options: [
          { label: "Australia", value: "0" },
          { label: "Other", value: "1" },
        ] as const,
        labelClassName: "mt-5",
        freeText: {
          property: "birthplace_oth",
          value: "1",
          label: "If other, please specify *",
        },
      },
      {
        key: "lang",
        type: "dropdown",
        label: "Do you speak a language other than English at home? *",
        options: [
          { label: "No, English only", value: "0" },
          { label: "Yes", value: "1" },
        ] as const,
        labelClassName: "mt-5",
        freeText: {
          property: "lang_oth",
          value: "1",
          label:
            "If yes, which language do you mainly speak at home (If more than one, indicate the one spoken most often) *",
        },
      },
      {
        key: "atsi",
        label: "Are you of Aboriginal or Torres Strait Islander origin? *",
        type: "dropdown",
        labelClassName: "mt-5",
        options: [
          { label: "No", value: "0" },
          { label: "Aboriginal", value: "1" },
          { label: "Torres Strait Islander", value: "2" },
          { label: "Both Aboriginal and Torres Strait Islander", value: "3" },
        ] as const,
      },
      {
        key: "gender",
        label: "What gender do you identify as? *",
        type: "dropdown",
        labelClassName: "mt-5",
        options: [
          { label: "Woman", value: "1" },
          { label: "Man", value: "2" },
          { label: "I use a different term", value: "3" },
          { label: "Prefer not to answer", value: "4" },
        ] as const,
        freeText: {
          property: "gender_oth",
          value: "3",
          label: "Please specify *",
        },
      },
      {
        key: "sex",
        label: "What was your sex recorded at birth *",
        type: "dropdown",
        labelClassName: "mt-5",
        options: [
          { label: "Female", value: "1" },
          { label: "Male", value: "2" },
          { label: "I use a different term", value: "3" },
          { label: "Prefer not to answer", value: "4" },
        ] as const,
        freeText: {
          property: "sex_oth",
          value: "3",
          label: "Please specify *",
        },
      },
      {
        key: "lgbt",
        label: "Do you identify as part of the LGBTIQA+ community? *",
        type: "dropdown",
        labelClassName: "mt-5",
        options: [
          { label: "No", value: "0" },
          { label: "Yes", value: "1" },
          { label: "Prefer not to answer", value: "2" },
        ] as const,
      },
      {
        key: "edu",
        label: "What is the highest level of education you have completed *",
        type: "dropdown",
        labelClassName: "mt-5",
        options: [
          { label: "Year 11 or below", value: "1" },
          { label: "Year 12 or equivalent", value: "2" },
          { label: "Trade/apprenticeship", value: "3" },
          { label: "Associate/undergraduate diploma", value: "4" },
          { label: "Bachelor degree or higher", value: "5" },
        ] as const,
      },
      {
        key: "disable",
        label:
          "For at least the last 6-months, have you been limited because of a health problem (physical or mental health) in activities people usually do? *",
        type: "dropdown",
        labelClassName: "mt-5",
        options: [
          { label: "No, not limited", value: "0" },
          { label: "Yes, limited", value: "1" },
          { label: "Yes, strongly limited", value: "2" },
        ] as const,
      },
    ] as const,
  },
  preludeToPastMonthBehaviour: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We would now like to understand your lifestyle{" "}
                <span className="text-lg font-bold italic md:text-2xl">
                  behaviours
                </span>{" "}
                in the
                {"  "}
                <span className="text-lg font-bold underline md:text-2xl">
                  past month
                </span>
                .
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  pastMonthBehaviour: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "bhvfreq_phys",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>
              In the <strong>past month</strong>, how often did you engage in
              physical activity? *
            </p>
            <p>
              (For example, brisk walking, jogging, swimming, tennis, gardening
              or work around the house, riding a bicycle)
            </p>
          </div>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Occasionally, but not in the past month",
            value: "7",
          },
          { label: "Do not / cannot exercise", value: "8" },
        ] as const,
      },
      {
        key: "bhvquant_phys_mins",
        type: "input",
        label: (
          <span>
            In the <strong>past month</strong>, on a <em> typical</em> day that
            you engaged in physical activity, how many <u>minutes</u> did you
            do? *
          </span>
        ),
        placeholder: "Please type how many minutes",
        inputType: "text",
        className: "col-span-full sm:col-span-2",
        labelClassName:
          "flex items-center col-span-full sm:col-span-full mt-2 sm:mt-10",
        notRenderedConds: [["pastMonthBehaviour", "bhvfreq_phys", "8"]],
      },
      {
        key: "bhvfreq_sun",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            <p>
              In the <strong>past month</strong>, how often did you engage in
              sun protection behaviours? *
            </p>
            <p>
              (For example, wearing sunscreen, a hat, or other sun-protective
              clothing)
            </p>
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Less often",
            value: "7",
          },
        ] as const,
      },
    ] as const,
  },
  pastMonthAlcohol: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "bhvfreq_alc",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            In the <strong>past month</strong>, how often did you have an
            alcoholic drink of any kind? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "About 1 day per month",
            value: "6",
          },
          {
            label: "Occasionally, but not in the past month",
            value: "7",
          },
          {
            label: "Do not drink / no longer drink",
            value: "8",
          },
        ] as const,
      },
      {
        key: "bhvDrinkOnDay",
        type: "image-counter",
        notRenderedConds: [["pastMonthAlcohol", "bhvfreq_alc", "8"]],
        label: (
          <span>
            In the <strong>past month</strong>, on a <em>typical</em> day that
            you had an alcoholic drink, how many drinks did you have? *
          </span>
        ),
        options: [
          {
            value: "bhvquant_avred",
            image: "/images/drinks/RedWine_WideBig.png",
            label: "Average serve of red wine (150ml)",
          },
          {
            value: "bhvquant_avwhite",
            image: "/images/drinks/WhiteWine_WideBig.png",
            label: "Average serve of white wine (150ml)",
          },
          {
            value: "bhvquant_avsparkle",
            image: "/images/drinks/Champagne1.png",
            label: "Average serve of sparkling wine (150ml)",
          },
          {
            value: "bhvquant_smlred",
            image: "/images/drinks/RedWine_NarrowSmall.png",
            label: "Small serve of red wine (100ml)",
          },
          {
            value: "bhvquant_smlwhite",
            image: "/images/drinks/WhiteWine_NarrowSmall.png",
            label: "Small serve of white wine (100ml)",
          },
          {
            value: "bhvquant_shot",
            image: "/images/drinks/Scotch.png",
            label: "Shot/nip spirit (30ml) – with or without mixer",
          },
          {
            value: "bhvquant_cocktail",
            image: "/images/drinks/Cocktail1.png",
            label: "Cocktail",
          },
          {
            value: "bhvquant_rtd",
            image: "/images/drinks/pre-mix.png",
            label: "Ready-to-drink spirit (275ml)",
          },
          {
            value: "bhvquant_beer",
            image: "/images/drinks/BeerPot.png",
            label: "Beer or cider (375ml)",
          },
        ] as const,
      },
    ] as const,
  },
  pastMonthSecondaryBehaviour: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "bhvfreq_cig",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            In the <strong>past month</strong>, have you used any tobacco or
            e-cigarette products? *
          </span>
        ),
        options: [
          {
            label: "No",
            value: "0",
          },
          {
            label: "Yes, regularly (at least once per day)",
            value: "1",
          },
          {
            label: "Yes, not regularly (less than once per day)",
            value: "2",
          },
        ] as const,
      },
      {
        key: "bhvfreq_veg",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            In the <strong>past month</strong>, how often did you include
            vegetables as part of a meal? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "Less often",
            value: "6",
          },
        ] as const,
      },
    ] as const,
  },
  preludeToOutcomes: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We would now like to understand your behaviour-related{" "}
                <span className="text-lg font-bold italic md:text-2xl">
                  intentions
                </span>{" "}
                over the{" "}
                <span className="text-lg font-bold underline md:text-2xl">
                  next month
                </span>
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  outcomes: {
    className: "grid gap-5 w-full mt-5",
    questions: [
      {
        key: "int_phys",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to increase the amount of exercise you do? *
          </span>
        ),
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_sun",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to increase your sun protection practices? *
          </span>
        ),

        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_alc",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to reduce the amount of alcohol you consume? *
          </span>
        ),
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_cig",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to reduce any tobacco or e-cigarette use? *
          </span>
        ),
        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
      {
        key: "int_veg",
        label: (
          <span>
            Over the <strong>next month</strong>, to what extent do you intend
            to increase the amount of vegetables you consume? *
          </span>
        ),

        type: "radio-group",
        options: [
          {
            label: "Not at all",
            value: "1",
          },
          {
            label: "To little extent",
            value: "2",
          },
          {
            label: "To some extent",
            value: "3",
          },
          {
            label: "To a large extent",
            value: "4",
          },
          {
            label: "To a very large extent",
            value: "5",
          },
          {
            label: "Not applicable to me",
            value: "6",
          },
        ] as const,
      },
    ] as const,
  },
  preludeToSecondaryOutcomes: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We will now ask you some further questions about your
                behaviour-related{" "}
                <span className="text-lg font-bold italic md:text-2xl">
                  intentions
                </span>{" "}
                over the{" "}
                <span className="text-lg font-bold underline md:text-2xl">
                  next month
                </span>
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  secondaryOutcomes: {
    questions: [
      {
        key: "intfreq_phys",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <div>
            <p>
              Over the <strong>next month</strong>, how often do you intend to
              engage in physical activity? *
            </p>
            <p>
              (For example, brisk walking, jogging, swimming, tennis, gardening
              or work around the house, riding a bicycle)
            </p>
          </div>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Occasionally, but not in the next month",
            value: "7",
          },
          { label: "Do not / cannot exercise", value: "8" },
        ] as const,
      },
      {
        key: "intquant_phys_mins",
        type: "input",
        label: (
          <span>
            Over the <strong>next month</strong>, on a <em>typical</em> day that
            you engage in physical activity, how many <u>minutes</u> do you
            intend to do? *
          </span>
        ),
        placeholder: "Please type how many minutes",
        inputType: "text",
        className: "col-span-full sm:col-span-2",
        labelClassName: "flex items-center col-span-full sm:col-span-full",
        notRenderedConds: [["secondaryOutcomes", "intfreq_phys", "8"]],
      },
      {
        key: "intfreq_sun",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            <p>
              Over the <strong>next month</strong>, how often do you intend to
              engage in sun protection behaviours? *
            </p>
            <p>
              (For example, wearing sunscreen, a hat, or other sun-protective
              clothing)
            </p>
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          { label: "About 1 day per month", value: "6" },
          {
            label: "Less often",
            value: "7",
          },
        ] as const,
      },
    ] as const,
  },
  secondaryOutcomesPart2: {
    questions: [
      {
        type: "element",
        key: "intro",
        content: <></>,
      },
      {
        key: "intfreq_alc",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            Over the <strong>next month</strong>, how often do you intend to
            have an alcoholic drink of any kind? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "About 1 day per month",
            value: "6",
          },
          {
            label: "Occasionally, but not in the next month",
            value: "7",
          },
          {
            label: "Do not drink / no longer drink",
            value: "8",
          },
        ] as const,
      },
      {
        key: "intDrinkOnDay",
        type: "image-counter",
        notRenderedConds: [
          ["secondaryOutcomesPart2", "intfreq_alc", "8"],
        ] as const,
        label: (
          <span>
            Over the <strong>next month</strong>, on a <em>typical </em> day
            that you have an alcoholic drink, how many drinks do you intend to
            have? *
          </span>
        ),
        options: [
          {
            value: "intquant_avred",
            image: "/images/drinks/RedWine_WideBig.png",
            label: "Average serve of red wine (150ml)",
          },
          {
            value: "intquant_avwhite",
            image: "/images/drinks/WhiteWine_WideBig.png",
            label: "Average serve of white wine (150ml)",
          },
          {
            value: "intquant_avsparkle",
            image: "/images/drinks/Champagne1.png",
            label: "Average serve of sparkling wine (150ml)",
          },
          {
            value: "intquant_smlred",
            image: "/images/drinks/RedWine_NarrowSmall.png",
            label: "Small serve of red wine (100ml)",
          },
          {
            value: "intquant_smlwhite",
            image: "/images/drinks/WhiteWine_NarrowSmall.png",
            label: "Small serve of white wine (100ml)",
          },
          {
            value: "intquant_shot",
            image: "/images/drinks/Scotch.png",
            label: "Shot/nip spirit (30ml) – with or without mixer",
          },
          {
            value: "intquant_cocktail",
            image: "/images/drinks/Cocktail1.png",
            label: "Cocktail",
          },
          {
            value: "intquant_rtd",
            image: "/images/drinks/pre-mix.png",
            label: "Ready-to-drink spirit (275ml)",
          },
          {
            value: "intquant_beer",
            image: "/images/drinks/BeerPot.png",
            label: "Beer or cider (375ml)",
          },
        ] as const,
      },
    ] as const,
  },
  secondaryOutcomesPart3: {
    questions: [
      {
        key: "intfreq_cig",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            Over the <strong>next month</strong>, do you intend to use any{" "}
            tobacco or e-cigarette products ? *
          </span>
        ),
        options: [
          {
            label: "No",
            value: "0",
          },
          {
            label: "Yes, regularly (at least once per day)",
            value: "1",
          },
          {
            label: "Yes, not regularly (less than once per day)",
            value: "2",
          },
        ] as const,
      },
      {
        key: "intfreq_veg",
        type: "button-group",
        className: "border border-4 rounded-lg border-pink-600",
        label: (
          <span>
            Over the <strong>next month</strong>, how often do you intend to
            include vegetables as part of a meal? *
          </span>
        ),
        options: [
          {
            label: "Every day",
            value: "1",
          },
          {
            label: "5-6 days per week",
            value: "2",
          },
          {
            label: "3-4 days per week",
            value: "3",
          },
          {
            label: "1-2 days per week",
            value: "4",
          },
          {
            label: "2-3 days per month",
            value: "5",
          },
          {
            label: "Less often",
            value: "6",
          },
        ] as const,
      },
    ] as const,
  },
  preludeToKnowledge: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                We would now like to understand your current knowledge of breast
                cancer risk factors. It&apos;s okay to be uncertain, just select
                &quot;unsure&quot;.
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
  riskKnowledge: {
    className: "grid lg:grid-cols-6 gap-3 w-full lg:max-w-6xl",
    questions: [
      {
        key: "secondaryOutcomesPart3Intro",
        type: "element",
        content: (
          <>
            <div className="col-span-full">
              <div className="space-y-3">
                <p>
                  To what extent do you consider the following factors to be
                  associated with an increased risk of breast cancer?
                </p>
                <div className="flex flex-col gap-3 rounded-lg border p-2 lg:flex-row">
                  <div className="mb-2 grid grid-flow-col-dense p-2">
                    <p className="pl-3">
                      <span className="text-red-600">
                        <strong>Clear risk factor</strong>
                      </span>
                      : there is strong, consistent evidence that this factor
                      increases risk of breast cancer.
                    </p>
                  </div>
                  <div className="grid grid-flow-col-dense p-2">
                    <p className="pl-3">
                      <span className="text-light-pink-700">
                        <strong>Possible risk factor</strong>
                      </span>
                      : there is some evidence that this factor increases risk
                      of breast cancer, but not enough to be certain.
                    </p>
                  </div>
                  <div className="grid grid-flow-col-dense p-2">
                    <p className="pl-3">
                      <span>
                        <strong className="text-green-600">
                          Not a proven risk factor
                        </strong>
                      </span>
                      : the evidence is too limited to determine whether this
                      factor increases risk of breast cancer.
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="hidden lg:col-span-2 lg:block" />
            <div className="hidden text-center text-red-600 before:-translate-x-1 lg:flex lg:items-center lg:justify-center">
              Clear risk factor
            </div>
            <div className="text-light-pink-700 hidden text-center lg:flex lg:items-center lg:justify-center">
              Possible risk factor
            </div>
            <div className="hidden text-left text-green-600  lg:flex lg:items-center lg:justify-center">
              Not a proven risk factor
            </div>
            <div className="hidden text-center lg:flex lg:items-center lg:justify-center">
              Unsure
            </div>
          </>
        ),
      },
      {
        key: "bcrisk_famhist",
        label: "Family history of breast cancer *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_phys",
        label: "Physical inactivity *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_sunexpo",
        label: "Sun exposure *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_alc",
        label: "Drinking alcohol *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_cig",
        label: "Using tobacco products *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
      {
        key: "bcrisk_ovweight",
        label: "Being overweight *",
        labelClassName: "mt-4 lg:mt-0 lg:col-span-2",
        type: "radio-group",
        className: "lg:justify-center",
        options: [
          {
            label: "Clear risk factor",
            value: "1",
            labelClassName: "lg:hidden",
          },
          {
            label: "Possible risk factor",
            value: "2",
            labelClassName: "lg:hidden",
          },
          {
            label: "Not a proven risk factor",
            value: "3",
            labelClassName: "lg:hidden",
          },
          {
            label: "Unsure",
            value: "4",
            labelClassName: "lg:hidden",
          },
        ] as const,
      },
    ] as const,
  },
  knowledge: {
    className: "grid lg:grid-cols-6 gap-3 w-full lg:max-w-6xl",
    questions: [
      {
        key: "knowl_phys",
        type: "button-group",
        label:
          "International guidelines recommend a minimum of _____ minutes of physical activity, five days a week. *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "30 minutes",
            value: "1",
          },
          {
            label: "45 minutes",
            value: "2",
          },
          {
            label: "60 minutes",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_sunscr",
        type: "button-group",
        label:
          "Sunscreen needs to be applied how many minutes before going outdoors? *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "20 minutes",
            value: "1",
          },
          {
            label: "30 minutes",
            value: "2",
          },
          {
            label: "60 minutes",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_sd",
        type: "button-group",
        label:
          "Drinking one average restaurant serve of wine a day increases breast cancer risk by... *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "4%",
            value: "1",
          },
          {
            label: "23%",
            value: "2",
          },
          {
            label: "61%",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_redwine",
        type: "button-group",
        label: "An average restaurant serve of red wine contains... *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "Less than 1 standard drink",
            value: "1",
          },
          {
            label: "1 standard drink",
            value: "2",
          },
          {
            label: "Closer to 2 standard drinks",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_alcguideline",
        type: "button-group",
        label:
          "Australian guidelines recommend that women should drink no more than _____ standard drinks per week *",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "12 standard drinks",
            value: "1",
          },
          {
            label: "10 standard drinks",
            value: "2",
          },
          {
            label: "7 standard drinks",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
      {
        key: "knowl_diet",
        type: "button-group",
        label:
          "Dietary guidelines advise that women consume more of which food group as they age?",
        labelClassName: "col-span-full",
        className:
          "grid grid-cols-1 col-span-full border border-4 rounded-lg border-pink-600",
        options: [
          {
            label: "Dairy",
            value: "1",
          },
          {
            label: "Vegetables",
            value: "2",
          },
          {
            label: "Protein",
            value: "3",
          },
          {
            label: "Unsure",
            value: "4",
          },
        ] as const,
      },
    ] as const,
  },
  preludeToActivities: {
    questions: [
      {
        type: "element",
        key: "transition-intro",
        content: (
          <>
            <div className="flex flex-col text-center text-pink-500">
              <p className="mt-10 text-lg font-bold md:text-2xl">
                Thank you for completing the survey. We&apos;d now like to show
                you some important health information.
              </p>
              <p className="mt-10 text-lg font-bold md:text-2xl">
                Please ensure you have your earphones plugged in to watch the
                videos.
              </p>
              <p className="mt-10 text-lg font-bold md:text-2xl">
                You&apos;ll be able to pause the video at any time. Click
                refresh on your browser to restart the video.
              </p>
            </div>
          </>
        ),
      },
    ] as const,
  },
} satisfies Survey;

// const baselineSurveyDefaultState: SurveyState<typeof baselineSurveyClinical> = {
//   aboutYou: {
//     first_name: "Chris",
//     last_name: "Prawira",
//     comm_pref: "1",
//     email: "a@a.com",
//     ph_mobile: "0452664517",
//     ph_home: "23132121",
//     postcode: "3000",
//     dob: new Date(),
//     recruit: "1",
//     pilot_no: "1234",
//     prev_part: "0",
//   },
//   demographic: {
//     birthplace: "Australia",
//     lang: "0",
//     atsi: "0",
//     gender: "1",
//     sex: "1",
//     lgbt: "0",
//     edu: "1",
//     disable: "0",
//   },
//   preludeToPastMonthBehaviour: {},
//   pastMonthBehaviour: {
//     bhvfreq_phys: "1",
//     bhvquant_phys_mins: "1",
//     bhvfreq_sun: "1",
//   },
//   pastMonthAlcohol: {
//     bhvfreq_alc: "1",
//     bhvDrinkOnDay: {
//       bhvquant_avred: 1,
//       bhvquant_avwhite: 0,
//       bhvquant_avsparkle: 0,
//       bhvquant_smlred: 0,
//       bhvquant_smlwhite: 0,
//       bhvquant_shot: 0,
//       bhvquant_cocktail: 0,
//       bhvquant_rtd: 0,
//       bhvquant_beer: 0,
//     },
//   },
//   pastMonthSecondaryBehaviour: {
//     bhvfreq_cig: "1",
//     bhvfreq_veg: "1",
//   },
//   preludeToOutcomes: {},
//   outcomes: {
//     int_phys: "1",
//     int_sun: "1",
//     int_alc: "1",
//     int_cig: "1",
//     int_veg: "1",
//   },
//   preludeToSecondaryOutcomes: {},
//   secondaryOutcomes: {
//     intfreq_phys: "1",
//     intquant_phys_mins: "1",
//     intfreq_sun: "1",
//   },
//   secondaryOutcomesPart2: {
//     intfreq_alc: "1",
//     intDrinkOnDay: {
//       intquant_avred: 1,
//       intquant_avwhite: 0,
//       intquant_avsparkle: 0,
//       intquant_smlred: 0,
//       intquant_smlwhite: 0,
//       intquant_shot: 0,
//       intquant_cocktail: 0,
//       intquant_rtd: 0,
//       intquant_beer: 0,
//     },
//   },
//   secondaryOutcomesPart3: {
//     intfreq_cig: "1",
//     intfreq_veg: "1",
//   },
//   preludeToKnowledge: {},
//   riskKnowledge: {
//     bcrisk_famhist: "1",
//     bcrisk_phys: "1",
//     bcrisk_sunexpo: "1",
//     bcrisk_alc: "1",
//     bcrisk_cig: "1",
//     bcrisk_ovweight: "1",
//   },
//   knowledge: {
//     knowl_phys: "1",
//     knowl_sunscr: "1",
//     knowl_sd: "1",
//     knowl_redwine: "1",
//     knowl_alcguideline: "1",
//     knowl_diet: "1",
//   },
//   preludeToActivities: {},
// };

const baselineSurveyDefaultState: SurveyState<typeof baselineSurveyPilot> = {
  aboutYou: {
    first_name: "",
    last_name: "",
    comm_pref: "Please select",
    email: "",
    ph_mobile: "",
    ph_home: "",
    postcode: "",
    dob: "",
    recruit: "Please select",
    pilot_no: "",
    prev_part: "Please select",
  },
  demographic: {
    birthplace: "Please select",
    lang: "Please select",
    atsi: "Please select",
    gender: "Please select",
    sex: "Please select",
    lgbt: "Please select",
    edu: "Please select",
    disable: "Please select",
  },
  preludeToPastMonthBehaviour: {},
  pastMonthBehaviour: {
    bhvfreq_phys: null,
    bhvquant_phys_mins: "",
    bhvfreq_sun: null,
  },
  pastMonthAlcohol: {
    bhvfreq_alc: null,
    bhvDrinkOnDay: {
      bhvquant_avred: 0,
      bhvquant_avwhite: 0,
      bhvquant_avsparkle: 0,
      bhvquant_smlred: 0,
      bhvquant_smlwhite: 0,
      bhvquant_shot: 0,
      bhvquant_cocktail: 0,
      bhvquant_rtd: 0,
      bhvquant_beer: 0,
    },
  },
  pastMonthSecondaryBehaviour: {
    bhvfreq_cig: null,
    bhvfreq_veg: null,
  },
  preludeToOutcomes: {},
  outcomes: {
    int_phys: null,
    int_sun: null,
    int_alc: null,
    int_cig: null,
    int_veg: null,
  },
  preludeToSecondaryOutcomes: {},
  secondaryOutcomes: {
    intfreq_phys: null,
    intquant_phys_mins: "",
    intfreq_sun: null,
  },
  secondaryOutcomesPart2: {
    intfreq_alc: null,
    intDrinkOnDay: {
      intquant_avred: 0,
      intquant_avwhite: 0,
      intquant_avsparkle: 0,
      intquant_smlred: 0,
      intquant_smlwhite: 0,
      intquant_shot: 0,
      intquant_cocktail: 0,
      intquant_rtd: 0,
      intquant_beer: 0,
    },
  },
  secondaryOutcomesPart3: {
    intfreq_cig: null,
    intfreq_veg: null,
  },
  preludeToKnowledge: {},
  riskKnowledge: {
    bcrisk_famhist: null,
    bcrisk_phys: null,
    bcrisk_sunexpo: null,
    bcrisk_alc: null,
    bcrisk_cig: null,
    bcrisk_ovweight: null,
  },
  knowledge: {
    knowl_phys: null,
    knowl_sunscr: null,
    knowl_sd: null,
    knowl_redwine: null,
    knowl_alcguideline: null,
    knowl_diet: null,
  },
  preludeToActivities: {},
};

const defaultBaselineProgress = {
  sectionTimestamps: {
    aboutYou: { startedAt: Timestamp.now(), completedAt: null },
  },
  page: "aboutYou" as keyof typeof baselineSurveyClinical,
};

export const BaselineSurveyCard = (props: {
  appEnv: AppEnv;
  userId: string;
  consent: Consent;
  progress: BaselineSurveyProgress | undefined;
  state: BaselineSurveyResponses | undefined;
  metadata: SurveyMetadata;
  signoutMutation: UseMutationResult<void, unknown, void, unknown>;
}) => {
  const queryClient = useQueryClient();
  const saveBaselineMutation = useSaveBaseline({
    onSuccess: () => {
      invalidateUserProgress(
        queryClient,
        props.userId,
        envToCollectionTypes[props.appEnv].UserProgress
      );
      invalidateBaselineSurveyProgress(
        envToCollectionTypes[props.appEnv].BaselineSurveyProgress,
        queryClient,
        props.userId
      );
    },
  });

  // Baseline Survey Section Progress
  const baselineSurveyProgressSections = [
    { id: 1, label: "About you", count: 2 },
    { id: 2, label: "Past month", count: 4 },
    { id: 3, label: "Next month", count: 6 },
    { id: 4, label: "Current knowledge", count: 3 },
    { id: 5, label: "Health4Her", count: 1 },
  ];

  // useEffect(() => {
  //   if (!props.metadata.screeningNumber) {
  //     getScreeningNumber!({
  //       client: props.appEnv,
  //     });
  //   }
  // }, []);

  return (
    <Card className="max-w-6xl">
      <SurveyForm
        survey={
          props.appEnv === AppEnv.PILOT
            ? baselineSurveyPilot
            : baselineSurveyClinical
        }
        defaultState={props.state ?? baselineSurveyDefaultState}
        defaultProgress={props.progress ?? defaultBaselineProgress}
        progressSections={baselineSurveyProgressSections}
        saving={saveBaselineMutation.isLoading}
        onComplete={(s, isCompleted, progress) => {
          if (s.aboutYou.ph_mobile) {
            try {
              s.aboutYou.ph_mobile = formatToAUPrefix(s.aboutYou.ph_mobile);
            } catch (error) {}
          }

          if (progress?.page !== "aboutYou") {
            saveBaselineMutation.mutate({
              baselineSurveyCollectionType:
                envToCollectionTypes[props.appEnv].BaselineSurvey,
              baselineSurveyProgressCollectionType:
                envToCollectionTypes[props.appEnv].BaselineSurveyProgress,
              userProgressCollectionType:
                envToCollectionTypes[props.appEnv].UserProgress,
              userId: props.userId,
              responses: s,
              isCompleted: isCompleted,
              progress: progress,
              metadata: props.metadata,
              appEnv: props.appEnv,
            });
          }
        }}
      />
    </Card>
  );
};
