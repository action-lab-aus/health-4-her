import React, { useEffect, useRef, useState } from "react";
import { PauseIcon } from "./PauseIcon";
import { PlayIcon } from "./PlayIcon";
import ReactPlayer from "react-player";

export const Video = (props: {
  src: string;
  subtitleSrc: string;
  vimeoSrc: string;
  onEnded: () => void;
}) => {
  const [loadingFromAssets, setLoadingFromAssets] = useState(false);
  const [played, setPlayed] = useState(false);
  const [idle, setIdle] = useState(false);
  const [ready, setReady] = useState(false);

  // Default to vimeo, fallback to CDN, fallback to props.ended (go to next section)
  const [videoSrc, setVideoSrc] = useState(props.vimeoSrc);

  let idleTimeout = useRef<NodeJS.Timeout | undefined>(undefined);

  useEffect(() => {
    return () => {
      clearTimeout(idleTimeout.current);
    };
  }, []);

  const handlePlayPauseClick = () => {
    if (ready) {
      setPlayed((prev) => !prev);
      handleMouseMove();
    }
  };

  const handleMouseMove = () => {
    setIdle(false);
    clearTimeout(idleTimeout.current);
    idleTimeout.current = setTimeout(() => setIdle(true), 1500);
  };

  const handleVideoError = (err: any) => {
    if (!loadingFromAssets) {
      // If vimeo doesn't load, try fetching from assets.
      setLoadingFromAssets(true);
      setVideoSrc(props.src);
    } else {
      // If already tried loading from assets, call the onEnded prop.
      props.onEnded();
    }
  };

  const Icon = played ? PauseIcon : PlayIcon;
  const iconClassName =
    "ml-[2px] h-12 w-12 rounded-full bg-pink-500 p-3 text-white";

  return (
    <div className="relative flex justify-center" onMouseMove={handleMouseMove}>
      <div className="aspect-video w-4/5">
        <ReactPlayer
          muted={!played}
          playing={played}
          // controls
          url={videoSrc}
          playsinline
          width="100%"
          height="100%"
          onReady={() => setReady(true)}
          onError={handleVideoError}
          onEnded={props.onEnded}
        />
        <button
          onClick={handlePlayPauseClick}
          className={`absolute inset-0 flex items-center justify-center ${
            played && idle ? "opacity-0" : "opacity-80"
          }`}
          disabled={!ready}
        >
          <Icon className={iconClassName} />
        </button>
      </div>
    </div>
  );
};
