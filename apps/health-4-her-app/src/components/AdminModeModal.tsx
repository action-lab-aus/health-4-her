import { Dispatch, SetStateAction, useState } from "react";
import { LockIcon } from "./LockIcon";
import { Modal } from "./Modal";
import { Button } from "./Button";

export const AdminModeModal = (props: {
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  enterAdminMode: (email: string) => void;
}) => {
  const [adminEmail, setAdminEmail] = useState("");
  const handleSubmit: React.FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();
    props.setOpen(false);
    props.enterAdminMode(adminEmail);
  };

  return (
    <Modal open={props.open} setOpen={props.setOpen} className="bg-white p-4">
      <Modal.Title className="flex gap-2">
        <LockIcon fill="black" />
        Enter Admin Mode
      </Modal.Title>
      <Modal.Description>
        Go to admin mode by entering the registered admin email.
      </Modal.Description>
      <form onSubmit={handleSubmit}>
        <div>
          <input
            className="border-dark-pink-500 my-1 w-full rounded-md border p-1 text-center"
            placeholder="Admin email"
            value={adminEmail}
            onChange={(e) => setAdminEmail(e.target.value)}
          />
        </div>
        <div className="mt-3 flex w-full flex-wrap justify-end gap-3">
          <Button
            variant="outlined"
            colour="black"
            className="w-full sm:w-auto"
            onClick={() => props.setOpen(false)}
          >
            Cancel
          </Button>
          <Button
            type="submit"
            className="w-full sm:w-auto"
            variant="filled"
            colour="dark-pink"
            onClick={() => props.setOpen(false)}
          >
            Enter
          </Button>
        </div>
      </form>
    </Modal>
  );
};
