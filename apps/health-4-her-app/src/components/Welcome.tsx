import Image from "next/image";
import { useEffect, useRef } from "react";

export const Welcome = (props: {
  children: React.ReactNode;
  image: "baseline" | "followup";
  closingDate: Date;
}) => {
  const imageRef = useRef<HTMLImageElement>(null);

  useEffect(() => {
    if (imageRef.current) {
      imageRef.current.scrollIntoView({ behavior: "smooth" });
    }
  }, []);

  const isPastClosingDate = () => new Date() >= props.closingDate;
  const getHeroImage = () => {
    if (props.image === "baseline") {
      if (isPastClosingDate()) {
        return "/images/cover-without-text.jpg";
      }
      return "/images/cover-image.jpeg";
    } else {
      return "/images/follow-up.png";
    }
  };
  console.log(isPastClosingDate());

  return (
    <div className="relative flex h-full w-full flex-col items-center justify-center gap-7 text-center">
      <Image
        width={800}
        height={600}
        src={getHeroImage()}
        alt="Health4Her welcome image"
        className="-z-10 rounded-lg bg-cover"
        ref={imageRef}
      ></Image>
      {isPastClosingDate() && (
        <div className="bg-light-pink-500/80 w-full rounded-lg p-4 md:w-1/2">
          Thank you for your interest in Health4Her. We’re sorry the study is
          now closed. We are appreciative of the contribution you were able to
          make.
        </div>
      )}
      {!isPastClosingDate() && props.children}
    </div>
  );
};
