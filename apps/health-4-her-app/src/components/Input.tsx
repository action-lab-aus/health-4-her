import { ColourPrefixesWithVariant } from "@/theme";
import { useState } from "react";
import { twMerge } from "tailwind-merge";

export type InputColours =
  | (ColourPrefixesWithVariant<"500"> & ColourPrefixesWithVariant<"600">)
  | undefined;

const colourStyles = {
  pink: "border-pink-500 focus:outline-pink-600",
  black: "border-black-500 focus:outline-black-600",
  "dark-pink": "border-dark-pink-500 focus:outline-dark-pink-600",
  "light-pink": "border-light-pink-500 focus:outline-light-pink-600",
} satisfies Record<Exclude<InputColours, undefined>, string>;

export const Input = (props: {
  colour: InputColours;
  id?: string;
  className?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement> | undefined;
  onKeyDown?: React.KeyboardEventHandler<HTMLInputElement> | undefined;
  value?: string | number | readonly string[] | undefined;
  type?: React.HTMLInputTypeAttribute | undefined;
  inputmode?: React.HTMLAttributes<HTMLInputElement>["inputMode"];
  pattern?: string;
  disabled?: boolean | undefined;
  placeholder?: string;
  required?: boolean;
  min?: number | string;
  max?: number | string;
  minlength?: number;
  maxlength?: number;
  controlled?: boolean;
  autoFocus?: boolean;
  customErrMsg?: string;
}) => {
  const [value, setValue] = useState("");

  return (
    <input
      className={twMerge(
        "rounded-md border p-2",
        props.colour ? colourStyles[props.colour] : undefined,
        props.className
      )}
      onChange={
        props.controlled ? (e) => setValue(e.target.value) : props.onChange
      }
      onKeyDown={props.onKeyDown}
      value={props.controlled ? value : props.value}
      pattern={props.pattern}
      id={props.id}
      name={props.id}
      type={props.type}
      disabled={props.disabled}
      placeholder={props.placeholder}
      required={props.required ?? true}
      minLength={props.minlength}
      maxLength={props.maxlength}
      min={props.min}
      max={props.max}
      autoComplete="off"
      autoFocus={props.autoFocus}
    />
  );
};
