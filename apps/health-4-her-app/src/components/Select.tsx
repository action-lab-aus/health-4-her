import { InputColours } from "./Input";
import { Listbox, Transition } from "@headlessui/react";
import { Chevron } from "./icons/Chevron";

export const Select = (props: {
  children: React.ReactNode;
  id?: string;
  disabled?: boolean | undefined;
  colour: InputColours;
  wrapperClassName?: string;
  className?: string;
  label?: string;
  value?: string;
  onChange?: (value: string) => void;
}) => {
  return (
    <div className={props.wrapperClassName}>
      <Listbox value={props.value} onChange={props.onChange}>
        {({ open }) => (
          <>
            <Listbox.Button className="flex w-full justify-between rounded-md border border-pink-500 bg-white p-2 text-start">
              {props.label}
              <Chevron
                direction={`${open ? "up" : "down"}`}
                className="h-8 w-8"
              />
            </Listbox.Button>
            <Transition
              enter="transition duration-100 ease-out"
              enterFrom="transform scale-95 opacity-0"
              enterTo="transform scale-100 opacity-100"
              leave="transition duration-75 ease-out"
              leaveFrom="transform scale-100 opacity-100"
              leaveTo="transform scale-95 opacity-0"
              dir="rtl"
            >
              <Listbox.Options
                dir="rtl"
                className="border-dark-pink-600 w-full rounded-lg border text-left"
              >
                {props.children}
              </Listbox.Options>
            </Transition>
          </>
        )}
      </Listbox>
    </div>
  );
};
