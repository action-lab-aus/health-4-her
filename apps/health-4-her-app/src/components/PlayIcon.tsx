export const PlayIcon = (props: { className?: string }) => {
  return (
    <svg
      className={props.className}
      fill="currentColor"
      height="100%"
      width="100%"
      version="1.1"
      id="Capa_1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 210 210"
      xmlSpace="preserve"
    >
      <g id="SVGRepo_bgCarrier" strokeWidth="0"></g>
      <g
        id="SVGRepo_tracerCarrier"
        strokeLinecap="round"
        strokeLinejoin="round"
      ></g>
      <g id="SVGRepo_iconCarrier">
        <path d="M179.07,105L30.93,210V0L179.07,105z"></path>{" "}
      </g>
    </svg>
  );
};
