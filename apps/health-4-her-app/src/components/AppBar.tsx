import Image from "next/image";
import { ReactNode } from "react";
import { twMerge } from "tailwind-merge";

export const AppBar = (props: { className?: string; children?: ReactNode }) => {
  return (
    <header
      className={twMerge(
        "text-lg flex items-center justify-between bg-light-pink-500 p-3 text-white drop-shadow-md",
        props.className
      )}
    >
      <Image
        src="/logo_medium.png"
        alt="Health4Her Logo"
        className="-z-50"
        width={150}
        height={32}
        priority
      />
      {props.children}
    </header>
  );
};
