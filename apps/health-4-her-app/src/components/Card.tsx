import { twMerge } from "tailwind-merge";

export const Card = (props: {
  children: React.ReactNode;
  className?: string;
}) => {
  return (
    <span
      className={twMerge(
        "flex w-full flex-grow flex-col justify-center gap-4 rounded-xl border-0 bg-current bg-opacity-5 p-4",
        props.className
      )}
    >
      {props.children}
    </span>
  );
};
