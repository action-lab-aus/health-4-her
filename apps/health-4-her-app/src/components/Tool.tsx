import {
  ActivityKeys,
  AppEnv,
  envToCollectionTypes,
  UserProgress,
  UserProgressCollectionType,
  VideoKeys,
  videos,
} from "@/types";
import { User } from "firebase/auth";
import { Card } from "./Card";
import { Video } from "./Video";
import { useUpdateUserProgress } from "@/mutations/useUpdateUserProgress";
import { useQueryClient } from "react-query";
import { invalidateUserProgress } from "@/queries/userProgress";
import { UpdateData, serverTimestamp } from "firebase/firestore";
import { ProgressIndicator } from "./ProgressIndicator";
import { Activity } from "./Activity";
import { activities } from "@/activities";

import { CSSTransition, TransitionGroup } from "react-transition-group";
import { useScheduledNotifications } from "@/queries/scheduledNotifications";

// The core tool
export const Tool = (props: {
  appEnv: AppEnv;
  progress: UserProgress;
  auth: User;
}) => {
  return (
    <VideoOrActivity
      appEnv={props.appEnv}
      progress={props.progress}
      userId={props.auth.uid}
    />
  );
};

const VideoOrActivity = (props: {
  appEnv: AppEnv;
  userId: string;
  progress: UserProgress;
}) => {
  const queryClient = useQueryClient();
  const scheduledNotificationsQuery = useScheduledNotifications(
    props.userId,
    envToCollectionTypes[props.appEnv].ScheduledNotifications
  );
  const updateUserProgressMutation = useUpdateUserProgress({
    onSuccess: () => {
      invalidateUserProgress(
        queryClient,
        props.userId,
        envToCollectionTypes[props.appEnv].UserProgress
      );
    },
  });
  const currentPageIndex = props.progress.journey.findIndex(
    (p) => p === props.progress.page
  );
  const nextPageIndex =
    currentPageIndex > -1 ? currentPageIndex + 1 : undefined;

  // The next page name is either the current page name if there is no next page, or the next page
  // in the user's journey
  const nextPageName =
    props.progress.journey[
      nextPageIndex !== undefined ? nextPageIndex : currentPageIndex
    ] ?? props.progress.page;

  const goToNextPage = () => {
    updateUserProgressMutation.mutate({
      collectionType: envToCollectionTypes[props.appEnv].UserProgress,
      userId: props.userId,
      progress: {
        page: nextPageName,
        [`${props.progress.page}.completedAt`]: serverTimestamp(),
        [`${nextPageName}.startedAt`]: serverTimestamp(),
      },
    });
    scheduledNotificationsQuery.refetch();
  };
  const handleSaveProgress = (progress: UpdateData<UserProgress>) => {
    updateUserProgressMutation.mutate({
      collectionType: envToCollectionTypes[props.appEnv].UserProgress,
      userId: props.userId,
      progress,
    });
  };

  const progress =
    props.progress.journey.length === 1
      ? 1
      : currentPageIndex / (props.progress.journey.length - 1);

  return (
    <Card
      className={`flex w-full p-10 xl:max-w-7xl ${
        props.progress.page in activities ? "h-full" : "h-5/6"
      }`}
    >
      <ProgressIndicator progress={progress} />
      <div
        className={`${
          props.progress.page in activities
            ? "h-full rounded-xl bg-[url('/images/activity-bg.gif')] bg-cover"
            : ""
        }`}
      >
        <TransitionGroup>
          <CSSTransition
            key={props.progress.page}
            timeout={500}
            classNames="slide"
          >
            <div className="h-full">
              {props.progress.page in videos ? (
                <Video
                  src={videos[props.progress.page as VideoKeys].videoSrc}
                  subtitleSrc={
                    videos[props.progress.page as VideoKeys].captionSrc
                  }
                  vimeoSrc={videos[props.progress.page as VideoKeys].vimeoSrc}
                  onEnded={goToNextPage}
                />
              ) : null}
              {props.progress.page in activities ? (
                <Activity
                  saveProgress={handleSaveProgress}
                  userId={props.userId}
                  activity={activities[props.progress.page as ActivityKeys]}
                  showCorrectAnswers={
                    activities[props.progress.page as ActivityKeys]
                      .showCorrectAnswers
                  }
                  progressSaving={updateUserProgressMutation.isLoading}
                  activityProgress={
                    props.progress[props.progress.page as ActivityKeys]
                  }
                  goToNextPage={goToNextPage}
                  className="h-full p-10"
                />
              ) : null}
            </div>
          </CSSTransition>
        </TransitionGroup>
      </div>
    </Card>
  );
};
