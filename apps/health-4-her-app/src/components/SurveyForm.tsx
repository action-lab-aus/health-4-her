import Image from "next/image";
import { Survey, SurveyState, BaselineSurveyProgress } from "@/surveys/types";
import { Fragment, HTMLInputTypeAttribute, useEffect, useState } from "react";
import { Select } from "./Select";
import { Input } from "./Input";
import { Button } from "./Button";
import { twJoin, twMerge } from "tailwind-merge";
import { getErrors } from "@/surveys/getErrors";
import { Chevron } from "./icons/Chevron";
import { ProgressIndicator } from "./ProgressIndicator";
import { NumberInput } from "./NumberInput";
import { Listbox } from "@headlessui/react";
import { Timestamp } from "firebase/firestore";
import { baselineSurveyClinical } from "./BaselineSurveyCard";
import AddCircle from "./icons/AddCircle";
import Close from "./icons/Close";
import Edit from "./icons/Edit";
import Done from "./icons/Done";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { subYears } from "date-fns";

const minYears = 30;
const maxYears = 110;

function getQuantityFromDrink(label: string) {
  const regex = /([^()]+)?\s?(?:\(([^)]+)\))?\s?([^()]+)?/;
  const match = label.match(regex);

  if (match) {
    const before = match[1] ? match[1].trim() : null;
    const quantity = match[2] ? match[2].trim() : null;
    const after = match[3] ? match[3].trim() : null;

    return { before, quantity, after };
  }
  return null;
}

export const SurveyForm = <TSurvey extends Survey>(props: {
  survey: TSurvey;
  defaultState: SurveyState<TSurvey>;
  defaultProgress: BaselineSurveyProgress | undefined;
  saving: boolean;
  progressSections?: { id: number; label: string; count: number }[];
  onComplete: (
    state: SurveyState<TSurvey>,
    isCompleted: boolean,
    progress?: BaselineSurveyProgress
  ) => void;
}) => {
  const [progressIndex, setProgressIndex] = useState(
    Object.keys(props.survey).indexOf(props.defaultProgress?.page ?? "")
  );
  const [currentSectionIndex, setCurrentSectionIndex] = useState(
    progressIndex !== -1 ? progressIndex : 0
  );
  const [showErrors, setShowErrors] = useState(false);
  const [mobileNumberValue, setMobileNumberValue] = useState<{
    key: string | undefined;
    value: string | undefined;
  }>({
    key: undefined,
    value: undefined,
  });

  const sectionCount = Object.keys(props.survey).length;
  const isFirstSection = currentSectionIndex === 0;
  const isLastSection = currentSectionIndex === sectionCount - 1;
  const [surveyState, setSurveyState] = useState(props.defaultState);
  const [surveyProgress, setSurveyProgress] = useState(props.defaultProgress);
  const [writableOptions, setWritableOptions] = useState<
    { label: string; image: string; value: string; isEditing: boolean }[]
  >([]);
  const [otherDrinkInputValue, setOtherDrinkInputValue] = useState<
    Record<string, string>
  >({});
  const [dateInputValue, setDateInputValue] = useState<Date | null>(null);

  const [sectionKeys] = useState(
    Object.keys(props.survey) as (keyof TSurvey)[]
  );
  const currentSectionKey = Object.keys(props.survey)[currentSectionIndex] as
    | undefined
    | keyof TSurvey;

  const errors = currentSectionKey
    ? getErrors(surveyState[currentSectionKey], props.survey[currentSectionKey])
    : undefined;

  const handleClick: React.FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();

    setTimeout(() => {
      window.scrollTo({ top: -1, left: 0, behavior: "smooth" });
    }, 100);

    let hasErrors: boolean;
    if (
      currentSectionKey === "aboutYou" &&
      mobileNumberValue.value !== undefined &&
      mobileNumberValue.key === "ph_mobile"
    ) {
      const isValidAUMobileNumber = (number: string) => {
        // Remove any spaces from the input.
        let sanitizedInput = number.replace(/\s/g, "");

        // Check the beginning of the input and adjust accordingly.
        if (sanitizedInput.startsWith("0")) {
          return sanitizedInput.length === 10;
        } else if (sanitizedInput.startsWith("+61")) {
          return sanitizedInput.length === 12;
        } else {
          return false; // Inputs that do not start with '0' or '+61' are considered invalid.
        }
      };

      const isNonAustralianMobile = !isValidAUMobileNumber(
        mobileNumberValue.value
      );

      hasErrors =
        ((errors && Boolean(errors.size)) || isNonAustralianMobile) ?? false;
      setShowErrors(hasErrors);
    } else {
      hasErrors = (errors && Boolean(errors.size)) ?? false;
      setShowErrors(hasErrors);
    }

    if (hasErrors) {
      return;
    }

    setCurrentSectionIndex((i) => {
      if (props.defaultProgress) {
        setSurveyProgress((prog) => {
          if (!prog) return;

          const nextPage = (Object.keys(props.survey)[i + 1] ??
            null) as keyof typeof baselineSurveyClinical;
          const newProgress = {
            sectionTimestamps: {
              ...prog.sectionTimestamps,
              [currentSectionKey as string]: {
                startedAt:
                  prog.sectionTimestamps[
                    currentSectionKey as keyof typeof baselineSurveyClinical
                  ]?.startedAt ?? null,
                completedAt: Timestamp.now(),
              },
              [nextPage as string]: {
                startedAt: Timestamp.now(),
                completedAt: null,
              },
            },
            page: nextPage,
          };

          // Replace other drink placeholders in surveyState with the actual values.
          let newSurveyState;

          if (
            (currentSectionKey as keyof typeof baselineSurveyClinical) ===
            "pastMonthAlcohol"
          ) {
            const newPastMonthAlcoholDrinkOnDay = Object.entries(
              (surveyState as SurveyState<typeof baselineSurveyClinical>)
                .pastMonthAlcohol.bhvDrinkOnDay
            ).reduce((acc, [key, value]) => {
              const newKey = otherDrinkInputValue[key] ?? key;

              if (otherDrinkInputValue.hasOwnProperty(key)) {
                acc[`${key}(${newKey})`] = value;
              } else {
                acc[key] = value;
              }

              return acc;
            }, {} as SurveyState<typeof baselineSurveyClinical>["pastMonthAlcohol"]["bhvDrinkOnDay"] & { [key: string]: number });

            newSurveyState = {
              ...surveyState,
              pastMonthAlcohol: {
                ...surveyState.pastMonthAlcohol,
                bhvDrinkOnDay: newPastMonthAlcoholDrinkOnDay,
              },
            };

            setSurveyState(newSurveyState);

            // Reset the state of the other drinks textboxes
            setOtherDrinkInputValue((values) =>
              Object.keys(values).reduce((acc, currentKey) => {
                acc[currentKey] = "";
                return acc;
              }, {} as typeof values)
            );
          }

          if (
            (currentSectionKey as keyof typeof baselineSurveyClinical) ===
            "secondaryOutcomesPart2"
          ) {
            const newIntentionDrinkOnDay = Object.entries(
              (surveyState as SurveyState<typeof baselineSurveyClinical>)
                .secondaryOutcomesPart2.intDrinkOnDay
            ).reduce((acc, [key, value]) => {
              const newKey = otherDrinkInputValue[key] ?? key;

              if (otherDrinkInputValue.hasOwnProperty(key)) {
                acc[`${key}(${newKey})`] = value;
              } else {
                acc[key] = value;
              }

              return acc;
            }, {} as SurveyState<typeof baselineSurveyClinical>["secondaryOutcomesPart2"]["intDrinkOnDay"] & { [key: string]: number });

            newSurveyState = {
              ...surveyState,
              secondaryOutcomesPart2: {
                ...surveyState.secondaryOutcomesPart2,
                intDrinkOnDay: newIntentionDrinkOnDay,
              },
            };

            setSurveyState(newSurveyState);
          }

          props.onComplete(
            newSurveyState ?? surveyState,
            isLastSection,
            newProgress
          );

          return newProgress;
        });
      } else {
        if (isLastSection) {
          props.onComplete(surveyState, isLastSection);
        }
      }

      return i + 1;
    });
    setWritableOptions([]);
  };

  const handleBackButtonClicked: React.MouseEventHandler<
    HTMLButtonElement
  > = () => {
    setCurrentSectionIndex((i) => i - 1);
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  const [progIndicatorSection, setProgIndicatorSection] = useState(0);

  useEffect(() => {
    if (!isLastSection && props.defaultProgress != undefined) {
      props.onComplete(surveyState, isLastSection, surveyProgress);
    }
  }, []);

  useEffect(() => {
    const preludeToPastMonthBehaviourIndex = sectionKeys.indexOf(
      "preludeToPastMonthBehaviour"
    );
    const preludeToOutcomesIndex = sectionKeys.indexOf("preludeToOutcomes");
    const preludeToKnowledgeIndex = sectionKeys.indexOf("preludeToKnowledge");
    const preludeToActivitiesIndex = sectionKeys.indexOf("preludeToActivities");

    if (currentSectionIndex >= preludeToActivitiesIndex) {
      setProgIndicatorSection(4);
    } else if (currentSectionIndex >= preludeToKnowledgeIndex) {
      setProgIndicatorSection(3);
    } else if (currentSectionIndex >= preludeToOutcomesIndex) {
      setProgIndicatorSection(2);
    } else if (currentSectionIndex >= preludeToPastMonthBehaviourIndex) {
      setProgIndicatorSection(1);
    } else {
      setProgIndicatorSection(0);
    }
  }, [currentSectionIndex, sectionKeys]);

  return (
    <form
      onSubmit={handleClick}
      className="flex w-full flex-col justify-between gap-3"
      autoComplete="off"
    >
      <div className="flex flex-col gap-3">
        {props.progressSections ? (
          <ProgressIndicator
            step={progIndicatorSection}
            currentSectionIndex={currentSectionIndex}
            progressSections={props.progressSections}
            progress={
              sectionCount === 0 ? 1 : currentSectionIndex / sectionCount
            }
            indeterminate={false}
          />
        ) : undefined}
        {props.saving ? (
          <span className="text-center text-2xl">
            Saving your response, please wait...
          </span>
        ) : null}
        <div
          className={
            (currentSectionKey
              ? props.survey[currentSectionKey]?.className
              : undefined) ?? "flex w-full flex-col gap-3"
          }
        >
          {currentSectionKey !== undefined
            ? props.survey[currentSectionKey]?.questions?.map((el) => {
                if (el.type === "element") {
                  return <Fragment key={el.key}>{el.content}</Fragment>;
                }
                if (!(el.key in surveyState[currentSectionKey])) {
                  console.error("Got bad survey key on survey question");
                  return null;
                }

                const questionKey =
                  el.key as keyof SurveyState<TSurvey>[typeof currentSectionKey];
                const questionValue =
                  surveyState[currentSectionKey][questionKey];

                const shouldRender = (): boolean => {
                  if (!el.notRenderedConds) {
                    return true;
                  }

                  const conditionIsMet = el.notRenderedConds.map(
                    (condition) => {
                      const [sectionKey, questionKey, questionValue] =
                        condition;
                      return (
                        surveyState[sectionKey]?.[
                          questionKey as keyof (typeof surveyState)[typeof sectionKey]
                        ] !== questionValue
                      );
                    }
                  );

                  return !conditionIsMet.includes(false);
                };
                // Date Picker Input
                if (el.type === "datepicker") {
                  const error = errors?.get(el.key);

                  return (
                    <Fragment key={el.key}>
                      <div
                        className={twMerge(
                          "mt-5 flex flex-col",
                          el.labelClassName,
                          "items-start"
                        )}
                      >
                        <label htmlFor={el.key}>{el.label}</label>
                        {showErrors ? (
                          <span className="text-error mb-2 font-semibold">
                            <small>{error}</small>
                          </span>
                        ) : null}
                      </div>
                      <div className="col-span-full appearance-none sm:col-span-3">
                        <DatePicker
                          selected={dateInputValue}
                          placeholderText="dd/mm/yyyy"
                          onChange={(date) => {
                            const dateVal =
                              date ?? subYears(new Date(), minYears);

                            const timezoneOffsetMinutes =
                              dateVal.getTimezoneOffset();
                            const adjustedDate = new Date(
                              dateVal.getTime() -
                                timezoneOffsetMinutes * 60 * 1000
                            );

                            setDateInputValue(dateVal);
                            setSurveyState((s) => ({
                              ...s,
                              [currentSectionKey]: {
                                ...s[currentSectionKey],
                                [el.key]: adjustedDate
                                  .toISOString()
                                  .split("T")[0],
                              },
                            }));
                          }}
                          minDate={subYears(new Date(), maxYears)}
                          maxDate={subYears(new Date(), minYears)}
                          dateFormat="dd/MM/yyyy"
                          className="col-span-full appearance-none rounded-md border border-pink-500 p-2 focus:outline-pink-600 sm:col-span-3"
                        />
                      </div>
                    </Fragment>
                  );
                }
                // Input
                if (el.type === "input" && typeof questionValue === "string") {
                  const error =
                    "Mobile number must be an Australian number such as 04xxxxxxxx or +61xxxxxxxxx";
                  return shouldRender() ? (
                    <Fragment key={el.key}>
                      <div className={twMerge("mt-5", el.labelClassName)}>
                        <label htmlFor={el.key}>{el.label}</label>
                        {showErrors &&
                        error &&
                        mobileNumberValue.key === el.key ? (
                          <span className="text-error mb-2 font-semibold">
                            <small>{error}</small>
                          </span>
                        ) : null}
                      </div>
                      <Input
                        id={el.key}
                        type={el.inputType}
                        inputmode={el.inputMode}
                        pattern={el.pattern}
                        placeholder={
                          el.placeholder
                            ? el.placeholder
                            : el.label && typeof el.label === "string"
                            ? el.label
                            : ""
                        }
                        required={el.required}
                        min={el.min}
                        max={el.max}
                        minlength={el.minlength}
                        maxlength={el.maxlength}
                        disabled={false}
                        className={el.className}
                        value={questionValue}
                        customErrMsg={el.customErrMsg}
                        onChange={(e) => {
                          setSurveyState((s) => ({
                            ...s,
                            [currentSectionKey]: {
                              ...s[currentSectionKey],
                              [el.key]: e.target.value,
                            },
                          }));

                          if (el.inputType === "tel") {
                            setMobileNumberValue({
                              key: el.key,
                              value: e.target.value,
                            });
                          }
                        }}
                        colour="pink"
                      />
                    </Fragment>
                  ) : null;
                }
                // Number Input
                if (
                  el.type === "number-input" &&
                  (typeof questionValue === "string" ||
                    typeof questionValue === "number")
                ) {
                  const handleArrowsClick = () => {
                    return {
                      up: () => {
                        setSurveyState((s) => ({
                          ...s,
                          [currentSectionKey]: {
                            ...s[currentSectionKey],
                            [el.key]: String(
                              Math.min(el.max ?? 120, Number(questionValue) + 1)
                            ),
                          },
                        }));
                      },
                      down: () => {
                        setSurveyState((s) => ({
                          ...s,
                          [currentSectionKey]: {
                            ...s[currentSectionKey],
                            [el.key]: String(
                              Math.max(el.min ?? 0, Number(questionValue) - 1)
                            ),
                          },
                        }));
                      },
                    };
                  };
                  return shouldRender() ? (
                    <Fragment key={el.key}>
                      <label
                        className={twMerge("mt-5", el.labelClassName)}
                        htmlFor={el.key}
                      >
                        {el.label}
                      </label>
                      <NumberInput
                        id={el.key}
                        pattern={el.pattern}
                        placeholder={
                          el.placeholder ??
                          (el.label && typeof el.label === "string")
                            ? (el.label as string)
                            : ""
                        }
                        required
                        min={el.min}
                        max={el.max}
                        disabled={false}
                        className={el.className}
                        value={questionValue.toString()}
                        onArrowsClick={handleArrowsClick}
                        onChange={(e) => {
                          setSurveyState((s) => ({
                            ...s,
                            [currentSectionKey]: {
                              ...s[currentSectionKey],
                              [el.key]: e.target.value,
                            },
                          }));
                        }}
                        colour="pink"
                      />
                    </Fragment>
                  ) : null;
                }
                // Dropdown (select)
                if (
                  el.type === "dropdown" &&
                  typeof questionValue === "string"
                ) {
                  const freeText = el.freeText as {
                    property: string &
                      keyof SurveyState<TSurvey>[typeof currentSectionKey];
                    value: string;
                    label: string;
                    inputType?: HTMLInputTypeAttribute;
                    className?: string;
                    required?: boolean;
                  };
                  const error = errors?.get(el.key);

                  const optionsWithDefault = (
                    [{ label: "Please select", value: "" }] as {
                      label: string;
                      value: string | number;
                    }[]
                  ).concat(el.options);

                  const selectedIndex = optionsWithDefault.findIndex(
                    (o) => o.value.toString() === questionValue
                  );
                  const isSelectedIndexNotFound = selectedIndex === -1;
                  const selectLabel = isSelectedIndexNotFound
                    ? questionValue
                    : optionsWithDefault[selectedIndex]?.label.toString();

                  const selectValue = isSelectedIndexNotFound
                    ? questionValue
                    : optionsWithDefault[selectedIndex]?.value.toString();

                  return shouldRender() ? (
                    <Fragment key={el.key}>
                      <label
                        className={twMerge(
                          "mt-5 flex flex-col",
                          el.labelClassName
                        )}
                        htmlFor={el.key}
                      >
                        {el.label}
                        {showErrors && error ? (
                          <span className="text-error mb-2 font-semibold">
                            {error}
                          </span>
                        ) : null}
                      </label>
                      <Select
                        id={el.key}
                        label={selectLabel}
                        value={selectValue}
                        onChange={(value) => {
                          setSurveyState((s) => ({
                            ...s,
                            [currentSectionKey]: {
                              ...s[currentSectionKey],
                              [el.key]: value,
                            },
                          }));
                        }}
                        className={el.className}
                        wrapperClassName={el.wrapperClassName}
                        colour="pink"
                      >
                        {optionsWithDefault.map((o, index) => (
                          <Listbox.Option
                            key={o.label}
                            value={
                              o.label === "Please select"
                                ? ""
                                : o.value.toString()
                            }
                            disabled={o.label === "Please select"}
                            className={` hover:bg-light-pink-500 ui-disabled:text-black-500
                            ui-disabled:opacity-50 ui-disabled:cursor-not-allowed mx-0 ${
                              index === el.options.length
                                ? "rounded-lg"
                                : "rounded-none border-b"
                            } bg-white p-2 hover:cursor-pointer`}
                          >
                            {o.label}
                          </Listbox.Option>
                        ))}
                      </Select>
                      {freeText &&
                      surveyState[currentSectionKey][questionKey] ===
                        freeText.value ? (
                        <>
                          <label htmlFor={freeText.property}>
                            {freeText.label}
                          </label>
                          <Input
                            id={el.key}
                            placeholder={freeText.label}
                            type={freeText.inputType ?? "text"}
                            className={freeText.className}
                            disabled={false}
                            value={
                              surveyState[currentSectionKey][freeText.property]
                                ? String(
                                    surveyState[currentSectionKey][
                                      freeText.property
                                    ]
                                  )
                                : ""
                            }
                            onChange={(e) =>
                              setSurveyState((s) => ({
                                ...s,
                                [currentSectionKey]: {
                                  ...s[currentSectionKey],
                                  [freeText.property]: e.target.value,
                                },
                              }))
                            }
                            colour="pink"
                          />
                        </>
                      ) : null}
                    </Fragment>
                  ) : null;
                }
                // Radio group
                if (
                  el.type === "radio-group" &&
                  (questionValue === null || typeof questionValue === "string")
                ) {
                  const error = errors?.get(el.key);
                  return shouldRender() ? (
                    <Fragment key={el.key}>
                      <div
                        className={twMerge(
                          props.survey[currentSectionKey]?.className,
                          "col-span-full rounded-lg border-4 border-pink-500 p-4"
                        )}
                      >
                        <label
                          className={twMerge(
                            "mt-5 flex flex-col",
                            el.labelClassName
                          )}
                        >
                          {el.label}
                          {showErrors && error ? (
                            <span className="text-error mb-2 font-semibold">
                              {error}
                            </span>
                          ) : null}
                        </label>
                        {el.options.map((o) => (
                          <button
                            type="button"
                            key={o.value}
                            className={twJoin(
                              "flex items-center gap-2",
                              el.className
                            )}
                            onClick={() =>
                              setSurveyState((s) => ({
                                ...s,
                                [currentSectionKey]: {
                                  ...s[currentSectionKey],
                                  [el.key]: o.value,
                                },
                              }))
                            }
                          >
                            <input
                              type="radio"
                              value={o.value}
                              id={`${el.key}${o.value}`}
                              className="border-dark-pink-500 before:bg-dark-pink-500 flex h-[24px] w-[24px] shrink-0 cursor-pointer appearance-none items-center justify-center rounded-full border-4 border-solid before:block before:h-[12px] before:w-[12px] before:scale-0 before:rounded-full before:transition-transform before:duration-75 before:ease-in before:content-[''] checked:before:scale-100"
                              checked={questionValue === o.value}
                              readOnly
                            />
                            <label
                              className={twMerge(
                                "mt-0 text-left",
                                o.labelClassName
                              )}
                              htmlFor={`${el.key}${o.value}`}
                            >
                              {o.label}
                            </label>
                          </button>
                        ))}
                      </div>
                    </Fragment>
                  ) : null;
                }
                // Button group
                if (
                  el.type === "button-group" &&
                  (questionValue === null || typeof questionValue === "string")
                ) {
                  const error = errors?.get(el.key);

                  return shouldRender() ? (
                    <Fragment key={el.key}>
                      <label
                        htmlFor={el.key}
                        className={twJoin(
                          el.labelClassName,
                          "mt-5 flex flex-col"
                        )}
                      >
                        {el.label}
                        {showErrors && error ? (
                          <span className="text-error mb-2 font-semibold">
                            {error}
                          </span>
                        ) : null}
                      </label>
                      <div className={twMerge(el.className, "gap-3 p-5")}>
                        {el.options.map((o) => (
                          <Button
                            key={`${o.label}-${o.value}`}
                            type="button"
                            colour={undefined}
                            onClick={() =>
                              setSurveyState((s) => ({
                                ...s,
                                [currentSectionKey]: {
                                  ...s[currentSectionKey],
                                  [el.key]: o.value,
                                },
                              }))
                            }
                            className="flex w-full items-center gap-2"
                            variant="filled"
                          >
                            <input
                              name={`${el.key}-${o.value}`}
                              checked={questionValue === o.value}
                              type="radio"
                              className="border-dark-pink-500 before:bg-dark-pink-500 flex h-[24px] w-[24px] shrink-0 cursor-pointer appearance-none items-center justify-center rounded-full border-4 border-solid before:block before:h-[12px] before:w-[12px] before:scale-0 before:rounded-full before:transition-transform before:duration-75 before:ease-in before:content-[''] checked:before:scale-100"
                              readOnly
                            />
                            <p className="text-left font-normal">{o.label}</p>
                          </Button>
                        ))}
                      </div>
                    </Fragment>
                  ) : null;
                }
                // Image counter
                if (
                  el.type === "image-counter" &&
                  typeof surveyState[currentSectionKey][questionKey] ===
                    "object"
                ) {
                  const error = errors?.get(el.key);

                  // We want to allow users to add in their own drinks too
                  const OTHER_DRINKS_LIMIT = 2;

                  if (!writableOptions.length) {
                    setWritableOptions(
                      el.options.map((option) => ({
                        ...option,
                        isEditing: false,
                      }))
                    );
                  }

                  const isOtherDrink = (value: string) =>
                    value.startsWith("other");

                  const getOtherDrinksCount = () => {
                    return writableOptions.filter((option) =>
                      isOtherDrink(option.value)
                    ).length;
                  };

                  const getNewDrinkValue = () => {
                    const otherDrinks = writableOptions.filter((option) =>
                      isOtherDrink(option.value)
                    );

                    if (otherDrinks.length <= 0) {
                      return 1;
                    }

                    const otherDrinksIds = otherDrinks.map((o) =>
                      Number(o.value.split(" - ")[1])
                    );

                    return Math.max(...otherDrinksIds) + 1;
                  };

                  const findOptionIndex = (value: string) =>
                    writableOptions.findIndex((o) => o.value === value);

                  const handleAddAnotherDrinkButtonClick = () => {
                    const newDrink = {
                      label: "Other",
                      image: "",
                      value: `other - ${getNewDrinkValue()}`,
                      isEditing: true,
                    };

                    setWritableOptions((options) => [...options, newDrink]);

                    setOtherDrinkInputValue((values) => ({
                      ...values,
                      [newDrink.value]: "",
                    }));

                    setSurveyState((s) => ({
                      ...s,
                      [currentSectionKey]: {
                        ...s[currentSectionKey],
                        [questionKey]: {
                          ...s[currentSectionKey][questionKey],
                          [newDrink.value]: 1,
                        },
                      },
                    }));
                  };

                  const handleRemoveOtherDrinkButtonClick = (value: string) => {
                    const index = findOptionIndex(value);
                    if (index === -1) return;

                    setWritableOptions((options) =>
                      options.filter((_, i) => i !== index)
                    );
                    setOtherDrinkInputValue((values) => {
                      const newValues = structuredClone(values);
                      delete newValues[value];

                      return newValues;
                    });

                    const newSurveyState = (s: SurveyState<TSurvey>) => ({
                      ...s,
                      [currentSectionKey]: {
                        ...s[currentSectionKey],
                        [questionKey]: {
                          ...s[currentSectionKey][questionKey],
                          [value]: 0,
                        },
                      },
                    });

                    setSurveyState((s) => {
                      const newState = newSurveyState(s);

                      delete newState[
                        currentSectionKey as keyof typeof newSurveyState
                      ]?.[questionKey]?.[value];
                      return newState;
                    });
                  };

                  const handleOtherDrinkEditButtonClick = (value: string) => {
                    const index = findOptionIndex(value);
                    if (index === -1) return;

                    setWritableOptions((options) =>
                      options.map((option, i) =>
                        i === index ? { ...option, isEditing: true } : option
                      )
                    );
                  };

                  const handleOtherDrinkEditConfirmButtonClick = (
                    value: string
                  ) => {
                    const index = findOptionIndex(value);

                    if (index === -1) return;

                    setWritableOptions((options) =>
                      options.map((option, i) =>
                        i === index
                          ? {
                              ...option,
                              isEditing: false,
                            }
                          : option
                      )
                    );
                  };

                  const handleOtherDrinkOnChange = (
                    e: React.ChangeEvent<HTMLInputElement>,
                    key: string
                  ) => {
                    setOtherDrinkInputValue((values) => ({
                      ...values,
                      [key]: e.target.value,
                    }));
                  };

                  return shouldRender() ? (
                    <Fragment key={el.key}>
                      <label
                        className={twMerge(
                          "mt-5 flex flex-col",
                          el.labelClassName
                        )}
                      >
                        {el.label}
                        {showErrors && error ? (
                          <span className="text-error mb-2 font-semibold">
                            {error}
                          </span>
                        ) : null}
                      </label>
                      <div className="grid grid-cols-1 gap-4 lg:grid-cols-2 xl:grid-cols-3">
                        {writableOptions.map((o) => (
                          <div
                            className={twJoin(
                              "shadow-dark-pink-600 flex w-full items-center overflow-clip rounded-xl shadow-sm",
                              el.className
                            )}
                            id={el.key}
                            key={o.value}
                          >
                            {/* Stepper */}
                            <div className="border-dark-pink-500 z-10 flex h-full flex-col items-center justify-center rounded-l-xl border-4 bg-white text-black">
                              {/* Increment Button */}
                              <Button
                                type="button"
                                colour="dark-pink"
                                variant="filled"
                                className="rounded-none"
                                onClick={() =>
                                  setSurveyState((s) => ({
                                    ...s,
                                    [currentSectionKey]: {
                                      ...s[currentSectionKey],
                                      [questionKey]: {
                                        ...s[currentSectionKey][questionKey],
                                        [o.value]:
                                          ((
                                            s[currentSectionKey][
                                              questionKey
                                            ] as Record<string, number>
                                          )?.[o.value] ?? 0) + 1,
                                      },
                                    },
                                  }))
                                }
                              >
                                <Chevron direction="up" className="h-8 w-8" />
                              </Button>
                              <span className="my-2 text-xl">
                                {(
                                  surveyState[currentSectionKey][
                                    questionKey
                                  ] as Record<string, number>
                                )[o.value] ?? 0}
                              </span>
                              {/* Decrement Button */}
                              <Button
                                type="button"
                                colour="dark-pink"
                                variant="filled"
                                className="rounded-none"
                                onClick={() =>
                                  setSurveyState((s) => ({
                                    ...s,
                                    [currentSectionKey]: {
                                      ...s[currentSectionKey],
                                      [questionKey]: {
                                        ...s[currentSectionKey][questionKey],
                                        [o.value]: Math.max(
                                          ((
                                            s[currentSectionKey][
                                              questionKey
                                            ] as Record<string, number>
                                          )?.[o.value] ?? 0) - 1,
                                          0
                                        ),
                                      },
                                    },
                                  }))
                                }
                              >
                                <Chevron direction="down" className="h-8 w-8" />
                              </Button>
                            </div>
                            <div className="border-dark-pink-500 bg-dark-pink-500/10 relative z-0 -ml-8 flex h-full grow items-center justify-center rounded-xl border-4 px-5 pr-3 text-center">
                              {/* Remove other drink button */}
                              {isOtherDrink(o.value) ? (
                                <Button
                                  type="button"
                                  variant="outlined"
                                  className="absolute right-2 top-2 hover:opacity-50"
                                  onClick={() =>
                                    handleRemoveOtherDrinkButtonClick(o.value)
                                  }
                                >
                                  <Close className="h-6 w-6" />
                                </Button>
                              ) : undefined}
                              {isOtherDrink(o.value) ? undefined : (
                                <Image
                                  className="aspect-square object-contain"
                                  src={o.image}
                                  alt="logo"
                                  width={80}
                                  height={80}
                                  priority
                                />
                              )}
                              <span className="text-lg font-bold sm:text-xl md:text-2xl">
                                {/* Editable Other Drinks */}
                                {isOtherDrink(o.value) && o.isEditing ? (
                                  <span className="align-center relative mx-auto flex h-12 items-center">
                                    {/* Edit input */}
                                    <Input
                                      autoFocus={true}
                                      colour="black"
                                      className="ml-5 w-4/5"
                                      value={
                                        otherDrinkInputValue[o.value] ==
                                        undefined
                                          ? ""
                                          : otherDrinkInputValue[o.value] ??
                                            o.label
                                      }
                                      onChange={(e) =>
                                        handleOtherDrinkOnChange(e, o.value)
                                      }
                                      onKeyDown={(e) => {
                                        if (e.key === "Enter") {
                                          handleOtherDrinkEditConfirmButtonClick(
                                            o.value
                                          );
                                          e.preventDefault();
                                        }
                                      }}
                                    />
                                    {/* Save edit button */}
                                    <Button
                                      type="button"
                                      variant="outlined"
                                      onClick={() =>
                                        handleOtherDrinkEditConfirmButtonClick(
                                          o.value
                                        )
                                      }
                                    >
                                      <Done className="right-12 top-3.5 h-5 w-5 cursor-pointer rounded-lg border-2 hover:opacity-50" />
                                    </Button>
                                  </span>
                                ) : (
                                  // Edit Button
                                  <span>
                                    <p className="relative -ml-2">
                                      {otherDrinkInputValue[o.value] ?? (
                                        <span>
                                          {
                                            getQuantityFromDrink(o.label)
                                              ?.before
                                          }{" "}
                                          <span className="text-sm">
                                            {getQuantityFromDrink(o.label)
                                              ?.quantity
                                              ? `(${
                                                  getQuantityFromDrink(o.label)
                                                    ?.quantity
                                                })`
                                              : ""}
                                          </span>{" "}
                                          {getQuantityFromDrink(o.label)?.after}
                                        </span>
                                      )}
                                      {isOtherDrink(o.value) ? (
                                        <Button
                                          type="button"
                                          variant="outlined"
                                          onClick={() =>
                                            handleOtherDrinkEditButtonClick(
                                              o.value
                                            )
                                          }
                                        >
                                          <Edit className="absolute -right-8 top-1 h-5 w-5 cursor-pointer hover:opacity-50" />
                                        </Button>
                                      ) : undefined}
                                    </p>
                                  </span>
                                )}
                              </span>
                            </div>
                          </div>
                        ))}
                        {/* Add another drink card */}
                        {getOtherDrinksCount() < OTHER_DRINKS_LIMIT ? (
                          <Button
                            variant="outlined"
                            colour="dark-pink"
                            className="group rounded-xl border-4"
                            type="button"
                            onClick={handleAddAnotherDrinkButtonClick}
                          >
                            <span className="flex items-center justify-center gap-3 transition-transform duration-200 ease-in-out group-hover:scale-105">
                              <AddCircle className="h-8 w-8" />
                              <p>Add another drink</p>
                            </span>
                          </Button>
                        ) : undefined}
                      </div>
                    </Fragment>
                  ) : null;
                }
                return null;
              })
            : null}
        </div>
      </div>
      <div className="mt-8 flex gap-5">
        {/* {!isFirstSection ? (
          <Button
            disabled={props.saving}
            type="button"
            variant="outlined"
            colour="pink"
            onClick={handleBackButtonClicked}
            className="w-full"
          >
            Back
          </Button>
        ) : null} */}
        <Button
          disabled={props.saving}
          type="submit"
          variant="filled"
          colour="pink"
          className="w-full"
        >
          {isLastSection ? "Done" : "Next"}
        </Button>
      </div>
    </form>
  );
};
