export const ErrorToast = (props: { children: React.ReactNode }) => {
  return (
    <div className="fixed bottom-0 right-0 m-8 flex items-center gap-3 rounded-md bg-error p-3 pr-5 text-white shadow-md">
      <svg
        width="1.8em"
        height="1.8em"
        viewBox="0 0 16 16"
        fill="currentColor"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fillRule="evenodd"
          d="M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z"
        />
        <path
          fillRule="evenodd"
          d="M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z"
        />
      </svg>
      {props.children}
    </div>
  );
};
