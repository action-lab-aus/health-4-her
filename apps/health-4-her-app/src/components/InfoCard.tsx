import { useCreateAnonymousUser } from "@/mutations/useCreateAnonymousUser";
import { Button } from "./Button";
import { Card } from "./Card";
import { useQueryClient } from "react-query";
import { invalidateAuth } from "@/queries/useAuth";
import { Loading } from "./Loading";
import { ErrorToast } from "./ErrorToast";
import { ReactNode, useState } from "react";
import Image from "next/image";
import { Modal } from "./Modal";
import PISForm from "./PISForm";

const info = [
  {
    title: "Project number",
    text: "LR22-071-91112 (Eastern Health Human Research Ethics Committee)",
  },
  {
    title: "Principal Investigator",
    text: "Dr Jasmin Grigg",
  },
  {
    title: "Associate Investigators",
    text: "Ms Peta Stragalinos, Dr Darren Lockie, Ms Michelle Giles, Prof Robin Bell, Dr Alex Waddell, Mr Joshua Seguin, Dr Ling Wu, Dr Jue Xie, Dr Chris Greenwood, Dr Bosco Rowland, A/Prof Victoria Manning, Prof Dan Lubman",
  },
  {
    title: "Location",
    text: "Maroondah BreastScreen",
  },
];

type Props = {};

export const InfoCard: React.FC<Props> = () => {
  const queryClient = useQueryClient();
  const createAnonymousUser = useCreateAnonymousUser({
    onSuccess: () => {
      invalidateAuth(queryClient);
    },
  });
  const [openPIS, setOpenPIS] = useState(false);

  const handleSubmit = async () => {
    createAnonymousUser.mutate();
  };

  const description: ReactNode = (
    <div className="space-y-5">
      <p>
        <strong>Hello!</strong>
      </p>
      <p>
        Thanks for taking the time to complete the final survey for the
        Health4Her study. Today, we will ask you some questions about{" "}
        <strong>yourself</strong>, your{" "}
        <strong>past-month lifestyle behaviours</strong>, your{" "}
        <strong>next-month behaviour-related intentions</strong>, and some{" "}
        <strong>knowledge</strong> questions, and ask for your{" "}
        <strong>feedback about Health4Her</strong>. This will take about 15
        minutes to complete.
      </p>
      <p>
        Any information that we collect which can identify you will remain{" "}
        <strong>strictly confidential</strong>. Maroondah BreastScreen and
        Eastern Health will not have access to any information you provide.
      </p>
      <p>
        Participation is <strong>entirely voluntary</strong>. If you don’t wish
        to take part, you don’t have to. Whether or not you choose to take part
        will not affect your relationship with Maroondah BreastScreen, Eastern
        Health or Monash University.
      </p>
      <p>
        Thank you for participating in Health4Her, for your contribution we are
        pledging $10 to breast cancer research.
      </p>

      <div className="flex flex-wrap justify-center gap-5 py-5">
        <Image
          src="/images/Logos.png"
          alt="Organisation Logos"
          width={1000}
          height={1000}
        />
      </div>
    </div>
  );

  return (
    <>
      <Modal
        open={openPIS}
        setOpen={setOpenPIS}
        className="w-5/6 bg-white sm:max-w-none"
      >
        <PISForm setShowPIS={setOpenPIS} />
      </Modal>
      <Card className="max-w-6xl">
        {createAnonymousUser.isError ? (
          <ErrorToast>Something went wrong, please try again</ErrorToast>
        ) : null}
        <div className="flex flex-col gap-5 p-5">
          <div>{description}</div>
        </div>
        <div className="flex w-full gap-2 px-5">
          <Button
            onClick={handleSubmit}
            disabled={createAnonymousUser.isLoading}
            variant="filled"
            colour="pink"
            className="flex w-full justify-center"
          >
            {createAnonymousUser.isLoading ? (
              <Loading className="mx-3 text-white" />
            ) : (
              <span className="my-2">Next</span>
            )}
          </Button>
        </div>
      </Card>
    </>
  );
};
