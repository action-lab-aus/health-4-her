import { UseMutationOptions, useMutation } from "react-query";
import { signOutAuth } from "@/auth";

export const useSignOut = (
  options?: Omit<UseMutationOptions<void, unknown, void, unknown>, "mutationfn">
) => {
  return useMutation({
    mutationFn: () => {
      return signOutAuth();
    },
    ...options,
  });
};
