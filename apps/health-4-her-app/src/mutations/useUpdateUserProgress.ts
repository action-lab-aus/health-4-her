import { UserProgressCollectionType } from "./../types";
import { UseMutationOptions, useMutation } from "react-query";
import { updateUserProgress } from "@/services/firestore";
import { UserProgress } from "@/types";
import { collection, UpdateData } from "firebase/firestore";

export const useUpdateUserProgress = (
  options?: Omit<
    UseMutationOptions<
      void,
      unknown,
      {
        collectionType: UserProgressCollectionType;
        userId: string;
        progress: UpdateData<UserProgress>;
      },
      unknown
    >,
    "mutationFn"
  >
) =>
  useMutation({
    mutationFn: (params: {
      collectionType: UserProgressCollectionType;
      userId: string;
      progress: UpdateData<UserProgress>;
    }) =>
      updateUserProgress(params.collectionType, params.userId, params.progress),
    ...options,
  });
