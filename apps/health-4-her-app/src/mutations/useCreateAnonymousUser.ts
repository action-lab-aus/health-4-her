import { createAnonymousUser } from "@/auth";
import { UserCredential } from "firebase/auth";
import { UseMutationOptions, useMutation } from "react-query";

export const useCreateAnonymousUser = (
  options?: Omit<
    UseMutationOptions<UserCredential, unknown, void, unknown>,
    "mutationFn"
  >
) =>
  useMutation({
    mutationFn: () => createAnonymousUser(),
    ...options,
  });
