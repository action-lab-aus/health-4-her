import { Arm } from "./../../../health-4-her-api/functions/src/types/client-types";
import {
  AppEnv,
  BaselineSurveyCollectionType,
  BaselineSurveyProgressCollectionType,
} from "./../types";
import { assignSlot } from "@/services/functions";
import { setBaselineSurveyProgressWithBatch } from "./../services/firestore";
import { BaselineSurveyProgress, SurveyMetadata } from "./../surveys/types";
import {
  BaselineSurveyResponses,
  setBaselineSurveyWithBatch,
  setUserProgressWithBatch,
} from "@/services/firestore";
import { getFirestore, serverTimestamp, writeBatch } from "firebase/firestore";
import { UseMutationOptions, useMutation } from "react-query";
import { HttpsCallableResult } from "firebase/functions";
// import toast from "react-hot-toast";
import { UserProgressCollectionType } from "@/types";

const getFromRandomizationFn = async (
  appEnv: AppEnv
): Promise<"intervention" | "control"> => {
  const slotResult:
    | { data: { randomisationNo: string; arm: Arm } }
    | HttpsCallableResult<{
        randomisationNo: string;
        arm: Arm;
      }> = await assignSlot!({ appEnv: appEnv }).catch((error) => {
    console.error({ error });
    return { data: { randomisationNo: "P000", arm: "control" as Arm } };
  });

  let group: "intervention" | "control";
  if (typeof slotResult === "string") {
    group = "control";
  } else {
    group = slotResult.data.arm;
  }

  // toast.success(
  //   `Hi ${slotResult.data.randomisationNo}. You have been assigned the ${group} arm.`,
  //   {
  //     duration: 5000,
  //     style: {
  //       border: "1px solid #8D1A4E",
  //       padding: "16px",
  //       color: "#8D1A4E",
  //     },
  //     iconTheme: {
  //       primary: "#F4A1C5",
  //       secondary: "#FFFAEE",
  //     },
  //   }
  // );

  return group;
};

export const useSaveBaseline = (
  options?: Omit<
    UseMutationOptions<
      void,
      unknown,
      {
        userId: string;
        responses: BaselineSurveyResponses;
        isCompleted: boolean;
        progress?: BaselineSurveyProgress;
      },
      unknown
    >,
    "mutationFn"
  >
) =>
  useMutation({
    mutationFn: async (params: {
      baselineSurveyCollectionType: BaselineSurveyCollectionType;
      baselineSurveyProgressCollectionType: BaselineSurveyProgressCollectionType;
      userProgressCollectionType: UserProgressCollectionType;
      userId: string;
      responses: BaselineSurveyResponses;
      isCompleted: boolean;
      progress?: BaselineSurveyProgress;
      metadata?: SurveyMetadata;
      appEnv: AppEnv;
    }) => {
      const batch = writeBatch(getFirestore());
      setBaselineSurveyWithBatch(
        params.baselineSurveyCollectionType,
        batch,
        params.userId,
        params.responses,
        params.isCompleted,
        params.metadata
      );

      if (params.progress) {
        setBaselineSurveyProgressWithBatch(
          params.baselineSurveyProgressCollectionType,
          batch,
          params.userId,
          params.progress
        );
      }

      if (params.isCompleted) {
        let group: Arm;

        if (params.appEnv === "clinical" || params.appEnv === "dev") {
          group = await getFromRandomizationFn(params.appEnv);
        } else {
          group = "intervention";
        }

        setUserProgressWithBatch(
          params.userProgressCollectionType,
          batch,
          params.userId,
          {
            page: "video1",
            video1: { startedAt: serverTimestamp() },
            journey:
              group === "intervention"
                ? [
                    "video1",
                    "activity1",
                    "video2",
                    "activity2",
                    "video3",
                    "activity3",
                    "video4",
                    "activity4",
                    "video5Intervention",
                  ]
                : ["video1", "activity1", "video5Control"],
          }
        );
      }

      await batch.commit();
    },
    ...options,
  });
