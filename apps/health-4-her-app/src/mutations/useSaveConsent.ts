import { setConsent } from "./../services/firestore";
import { Consent } from "@/types";
import { getFirestore, writeBatch } from "firebase/firestore";
import { UseMutationOptions, useMutation } from "react-query";

export const useSaveConsent = (
  options?: Omit<
    UseMutationOptions<
      void,
      unknown,
      {
        userId: string;
        consent: Consent;
      },
      unknown
    >,
    "mutationFn"
  >
) =>
  useMutation({
    mutationFn: async (params: { userId: string; consent: Consent }) => {
      const batch = writeBatch(getFirestore());
      setConsent(batch, params.userId, params.consent);

      await batch.commit();
    },
    ...options,
  });
