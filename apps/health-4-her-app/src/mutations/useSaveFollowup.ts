import {
  FollowupSurveyCollectionType,
  FollowupSurveyProgressCollectionType,
} from "./../types";
import {
  FollowupSurveyResponses,
  setFollowupSurveyProgressWithBatch,
  setFollowupSurveyWithBatch,
} from "./../services/firestore";
import { FollowupSurveyProgress, SurveyMetadata } from "./../surveys/types";
import { getFirestore, writeBatch } from "firebase/firestore";
import { UseMutationOptions, useMutation } from "react-query";

export const useSaveFollowup = (
  options?: Omit<
    UseMutationOptions<
      void,
      unknown,
      {
        userId: string;
        responses: FollowupSurveyResponses;
        isCompleted: boolean;
        progress?: FollowupSurveyProgress;
      },
      unknown
    >,
    "mutationFn"
  >
) =>
  useMutation({
    mutationFn: async (params: {
      followupSurveyCollectionType: FollowupSurveyCollectionType;
      followupSurveyProgressCollectionType: FollowupSurveyProgressCollectionType;
      userId: string;
      responses: FollowupSurveyResponses;
      isCompleted: boolean;
      progress?: FollowupSurveyProgress;
      metadata?: SurveyMetadata;
      referenceId: string;
    }) => {
      const batch = writeBatch(getFirestore());
      setFollowupSurveyWithBatch(
        params.followupSurveyCollectionType,
        batch,
        params.userId,
        params.responses,
        params.isCompleted,
        params.metadata,
        params.referenceId
      );

      if (params.progress) {
        setFollowupSurveyProgressWithBatch(
          params.followupSurveyProgressCollectionType,
          batch,
          params.userId,
          params.progress
        );
      }

      await batch.commit();
    },
    ...options,
  });
