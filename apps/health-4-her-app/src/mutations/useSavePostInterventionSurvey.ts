import { PostInterventionSurveyCollectionType } from "./../types";
import {
  PostInterventionSurveyResponses,
  setPostInterventionSurveyWithBatch,
  updateUserProgressWithBatch,
} from "@/services/firestore";
import { UserProgressCollectionType } from "@/types";
import { getFirestore, writeBatch } from "firebase/firestore";
import { UseMutationOptions, useMutation } from "react-query";

export const useSavePostInterventionSurvey = (
  options?: Omit<
    UseMutationOptions<
      void,
      unknown,
      {
        userId: string;
        responses: PostInterventionSurveyResponses;
      },
      unknown
    >,
    "mutationFn"
  >
) =>
  useMutation({
    mutationFn: async (params: {
      postInterventionSurveyCollectionType: PostInterventionSurveyCollectionType;
      userProgressCollectionType: UserProgressCollectionType;
      userId: string;
      responses: PostInterventionSurveyResponses;
    }) => {
      const batch = writeBatch(getFirestore());
      setPostInterventionSurveyWithBatch(
        params.postInterventionSurveyCollectionType,
        batch,
        params.userId,
        params.responses
      );
      updateUserProgressWithBatch(
        params.userProgressCollectionType,
        batch,
        params.userId,
        { page: "done" }
      );
      return batch.commit();
    },
    ...options,
  });
