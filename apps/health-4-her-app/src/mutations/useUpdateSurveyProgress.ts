import { BaselineSurveyProgress } from "./../surveys/types";
import { UseMutationOptions, useMutation } from "react-query";
import { updateSurveyProgress } from "@/services/firestore";
import { UpdateData } from "firebase/firestore";
import { BaselineSurveyProgressCollectionType } from "@/types";

export const useUpdateSurveyProgress = (
  options?: Omit<
    UseMutationOptions<
      void,
      unknown,
      {
        userId: string;
        progress: UpdateData<BaselineSurveyProgress>;
      },
      unknown
    >,
    "mutationFn"
  >
) =>
  useMutation({
    mutationFn: (params: {
      collectionType: BaselineSurveyProgressCollectionType;
      userId: string;
      progress: UpdateData<BaselineSurveyProgress>;
    }) =>
      updateSurveyProgress(
        params.collectionType,
        params.userId,
        params.progress
      ),
    ...options,
  });
