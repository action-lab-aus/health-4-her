import { setSurveyDiscontinueWithBatch } from "./../services/firestore";
import { SurveyDiscontinueInfo } from "./../surveys/types";
import { getFirestore, writeBatch } from "firebase/firestore";
import { UseMutationOptions, useMutation } from "react-query";
import { SurveyDiscontinueCollectionType } from "@/types";

export const useSurveyDiscontinue = (
  options?: Omit<
    UseMutationOptions<
      void,
      unknown,
      {
        userId: string;
        discontinueInfo: SurveyDiscontinueInfo;
      },
      unknown
    >,
    "mutationFn"
  >
) =>
  useMutation({
    mutationFn: async (params: {
      collectionType: SurveyDiscontinueCollectionType;
      userId: string;
      discontinueInfo: SurveyDiscontinueInfo;
    }) => {
      const batch = writeBatch(getFirestore());
      setSurveyDiscontinueWithBatch(
        params.collectionType,
        batch,
        params.userId,
        params.discontinueInfo
      );
      await batch.commit();
    },
    ...options,
  });
