import { CollectionByEnvironment } from "./../constants/collections";
import { Environment, Arm } from "./../types/client-types";
import { timestampAfterNTimeDuration } from "../utils/date-utils";
import {
  // NOTIFICATION_DEV_INTERVALS,
  NOTIFICATION_PROD_INTERVALS,
} from "../constants/notifications-config";
import {
  emailTemplates,
  // INITIAL_EMAIL_TEMPLATE,
  // REMINDER_EMAIL_TEMPLATE,
  // templateId,
} from "../constants/email-templates";
import {
  Attachment,
  EmailSendType,
  ScheduledNotification,
  SMSSendType,
} from "../types/scheduled-notification-types";
import { BaselineSurveyState } from "../types/survey-types";
import * as functions from "firebase-functions";
import { baselineSurveyStateConverter } from "../converters/baseline-survey-state-converter";
import { db } from "../firebase-init";
import { DocumentSnapshot, Timestamp } from "firebase-admin/firestore";
import { scheduledNotificationsConverter } from "../converters/scheduled-notifications-converter";
import { v4 as uuidv4 } from "uuid";

import sgMail from "@sendgrid/mail";
sgMail.setApiKey(functions.config().sendgrid["api-key"]);

import twilio from "twilio";
import {
  INITIAL_SMS_TEMPLATE,
  QUESTIONNAIRE_SMS_TEMPLATE,
  REMINDER_SMS_TEMPLATE,
  REMINDER_SMS_TEMPLATE_2,
} from "../constants/sms-templates";
import {
  getControlAttachments,
  getInterventionAttachments,
} from "../service/attachment";
import { followupSurveyConverter } from "../converters/followup-survey-converter";

const accountSid = functions.config().twilio["sid"];
const authToken = functions.config().twilio["auth-token"];

const twilioClient = twilio(accountSid, authToken);

// Sendgrid
const fromEmailAddress = functions.config().sendgrid["from"];

// Twilio
const fromMobileNumber = functions.config().twilio["from"];

const routesConfig = functions.config().env.routes;

export const hasCompletedFollowupSurvey = async (
  referenceId: string,
  followupSurveyCollection: CollectionByEnvironment<"followupSurvey">
) => {
  const ref = db
    .collection(followupSurveyCollection)
    .withConverter(followupSurveyConverter);

  const query = ref.where("referenceId", "==", referenceId);
  const snapshots = await query.get();

  const docsWithCompletedSurveys = snapshots.docs.filter((doc) => {
    return doc.data().completedAt != null;
  });

  return docsWithCompletedSurveys.length > 0;
};

const getBaselineSurveySnapshot = async (
  baselineSurveyCollection: string,
  id: string
) => {
  try {
    return await db
      .collection(baselineSurveyCollection)
      .withConverter(baselineSurveyStateConverter)
      .doc(id)
      .get();
  } catch (error) {
    functions.logger.error(
      `Error retrieving baseline survey for ${id}: ${error}`
    );

    throw error;
  }
};

export const getAssignedArmForUser = async (
  userId: string,
  appEnv: Environment
): Promise<{ arm: Arm; id: string } | null> => {
  let collectionName = "slots-dev";

  if (appEnv === "clinical") {
    collectionName = "slots-clinical";
  } else if (appEnv === "pilot") {
    collectionName = "slots-pilot";
  } else {
    collectionName = "slots-dev";
  }

  const slotsRef = db.collection(collectionName);
  const querySnapshot = await slotsRef
    .where("userIds", "array-contains", userId)
    .get();

  if (querySnapshot.empty) {
    return null;
  }

  const slotData = querySnapshot.docs[0].data();
  return { arm: slotData.arm, id: slotData.id };
};

export const deleteScheduledNotifications = async (
  collections: CollectionByEnvironment<"scheduledNotifications">,
  userId: string
) => {
  const ref = db.collection(collections).doc(userId);

  try {
    await ref.delete();
    functions.logger.info(`Deleted scheduled notifications for ${userId}`);
  } catch (error) {
    functions.logger.error(
      `Error deleting scheduled notifications for ${userId}: ${error}`
    );
  }
};

export const createScheduledNotifications = async (
  snapshot: functions.firestore.QueryDocumentSnapshot,
  baselineSurveyCollection: string,
  scheduledNotificationsCollection: string,
  appEnv: Environment
) => {
  functions.logger.info(
    `Executing scheduled notification creation for ${snapshot.id}`
  );

  let baselineSurveySnapshot: DocumentSnapshot<BaselineSurveyState>;

  try {
    baselineSurveySnapshot = await getBaselineSurveySnapshot(
      baselineSurveyCollection,
      snapshot.id
    );
  } catch (error) {
    functions.logger.error(error);
    return;
  }

  if (!baselineSurveySnapshot.exists) {
    functions.logger.error(`Baseline survey for ${snapshot.id} does not exist`);
    return;
  }

  const baselineSnapshotData = baselineSurveySnapshot.data();
  if (!baselineSnapshotData || !baselineSnapshotData.aboutYou) {
    functions.logger.error(`'aboutYou' data for ${snapshot.id} does not exist`);
    return;
  }

  const { first_name, last_name, email, ph_mobile } =
    baselineSnapshotData.aboutYou;

  const userAllocation = await getAssignedArmForUser(snapshot.id, appEnv);

  const devScheduleConfig = NOTIFICATION_PROD_INTERVALS;
  // const devScheduleConfig = NOTIFICATION_DEV_INTERVALS;
  const createdAt: Timestamp = Timestamp.now();

  const ref = db.collection(scheduledNotificationsCollection);
  const data: ScheduledNotification = {
    firstName: first_name ?? "",
    lastName: last_name ?? "",
    emailAddress: email ?? null,
    mobileNumber: ph_mobile ?? null,
    createdAt: createdAt,
    channels: {
      email: email ? true : false,
      sms: ph_mobile ? true : false,
    },
    optOutPreferences: {
      email: {
        value: email ? false : true,
        optedOutAt: null,
      },
      sms: {
        value: ph_mobile ? false : true,
        optedOutAt: null,
      },
    },
    schedule: devScheduleConfig.map(
      ({ description, unit, value, template }) => ({
        title: description,
        sendAt: Timestamp.fromMillis(
          timestampAfterNTimeDuration(createdAt.toMillis(), unit, value)
        ),
        sent: false,
        template: template,
      })
    ),
    nextSendAt: Timestamp.fromMillis(
      timestampAfterNTimeDuration(
        createdAt.toMillis(),
        devScheduleConfig[0].unit,
        devScheduleConfig[0].value
      )
    ),
    referenceId: uuidv4(),
    arm: userAllocation?.arm ?? null,
  };

  try {
    await ref.doc(snapshot.id).set(data);
    functions.logger.info(`Notifications data set for ${snapshot.id}`);
  } catch (error) {
    functions.logger.error(`Error setting data for ${snapshot.id}: ${error}`);
  }

  // Send initial notification
  // try {
  //   const shouldSendEmail =
  //     data.channels.email && !data.optOutPreferences.email.value;
  //   const shouldSendSMS =
  //     data.channels.sms && !data.optOutPreferences.sms.value;

  //   if (shouldSendEmail && data.emailAddress) {
  //     functions.logger.log(`Sending initial email for ${snapshot.id}`);

  //     const attachments =
  //       data.arm === "control"
  //         ? await getControlAttachments()
  //         : await getInterventionAttachments();

  //     const template: EmailSendType = {
  //       to: data.emailAddress,
  //       from: fromEmailAddress,
  //       templateId: emailTemplates["initial"].templateId,
  //       dynamicTemplateData: {
  //         firstName: data.firstName,
  //         surveyLink: "",
  //       },
  //       attachments: attachments ?? [],
  //     };

  //     await sendEmailUsingSendgrid(snapshot.id, template);
  //   }

  //   if (shouldSendSMS && data.mobileNumber) {
  //     functions.logger.log(`Sending initial sms for ${snapshot.id}`);

  //     const smsTemplate = INITIAL_SMS_TEMPLATE;

  //     if (!smsTemplate) {
  //       functions.logger.error(
  //         `SMS template for ${snapshot.id} not found for initial sms`
  //       );
  //       return;
  //     }

  //     const template: SMSSendType = {
  //       to: data.mobileNumber,
  //       from: fromMobileNumber,
  //       body: smsTemplate(
  //         data.firstName,
  //         "",
  //         data.arm === "control" ? "control" : "intervention"
  //       ).body,
  //     };

  //     await sendSMSUsingTwilio(snapshot.id, template);
  //   }
  // } catch (error) {
  //   functions.logger.error(
  //     `Error sending initial notification for ${snapshot.id}: ${error}`
  //   );
  // }
};

async function sendNotificationAndUpdate(
  doc: FirebaseFirestore.QueryDocumentSnapshot<ScheduledNotification>,
  attachments: Attachment[] | null,
  appEnv: Environment,
  unregistered: boolean
) {
  const data = doc.data();
  const now = Timestamp.now();

  const shouldSendEmail =
    !unregistered && data.channels.email && !data.optOutPreferences.email.value;
  const shouldSendSMS =
    !unregistered && data.channels.sms && !data.optOutPreferences.sms.value;

  let notificationSent = false;
  const newSchedulePromises = data.schedule.map(async (item) => {
    if (!item || item.sent || item.sendAt.seconds > now.seconds) return item;

    if (!notificationSent) {
      const surveyLink = `${routesConfig["prod"]["host"]}${
        routesConfig["prod"][appEnv] ?? ""
      }${routesConfig["prod"]["followup-survey"]}?refId=${
        data?.referenceId ?? ""
      }`;

      try {
        if (shouldSendEmail && data.emailAddress) {
          functions.logger.log(`Sending ${item.template} email for ${doc.id}`);

          const template: EmailSendType = {
            to: data.emailAddress,
            from: fromEmailAddress,
            templateId: emailTemplates[item.template].templateId,
            dynamicTemplateData: {
              firstName: data.firstName,
              surveyLink: surveyLink,
            },
            attachments: attachments ?? [],
          };

          await sendEmailUsingSendgrid(doc.id, template);
        }

        if (shouldSendSMS && data.mobileNumber) {
          functions.logger.log(`Sending ${item.template} sms for ${doc.id}`);

          const smsTemplate =
            item.template === "initial"
              ? INITIAL_SMS_TEMPLATE
              : item.template === "reminder"
                ? REMINDER_SMS_TEMPLATE
                : item.template === "questionnaire"
                  ? QUESTIONNAIRE_SMS_TEMPLATE
                  : item.template === "reminder2"
                    ? REMINDER_SMS_TEMPLATE_2
                    : null;

          if (!smsTemplate) {
            functions.logger.error(
              `SMS template for ${doc.id} not found for ${item.template}`
            );
            return item;
          }

          const template: SMSSendType = {
            to: data.mobileNumber,
            from: fromMobileNumber,
            body: smsTemplate(
              data.firstName,
              surveyLink,
              data.arm === "control" ? "control" : "intervention"
            ).body,
          };

          await sendSMSUsingTwilio(doc.id, template);
        }
      } catch (error) {
        functions.logger.error(
          `Error sending notification for ${doc.id}: ${error}`
        );
        return item;
      }

      notificationSent = true;
    }

    return { ...item, sent: true };
  });

  const newSchedule = await Promise.all(newSchedulePromises);
  const nextSendAt = newSchedule.find((item) => !item.sent)?.sendAt || null;

  // Update document with the new schedule and nextSendAt
  try {
    await doc.ref.update({
      schedule: newSchedule,
      nextSendAt: nextSendAt,
    });
  } catch (error) {
    functions.logger.error(`Error updating document ${doc.id}: ${error}`);
    throw error;
  }
}

export const checkForPendingScheduledNotifications = async (
  appEnv: Environment,
  scheduledNotificationsCollection: CollectionByEnvironment<"scheduledNotifications">,
  followupSurveyCollection: CollectionByEnvironment<"followupSurvey">
) => {
  const unregisterFromNotifications = async (
    ref: FirebaseFirestore.DocumentReference<ScheduledNotification>
  ) => {
    await ref.update({ channels: { email: false, sms: false } });
  };
  const getPendingNotifications = async (
    scheduledNotificationsCollection: string
  ) => {
    const now = Timestamp.now();
    const ref = db
      .collection(scheduledNotificationsCollection)
      .withConverter(scheduledNotificationsConverter);

    const query = ref.where("nextSendAt", "<=", now);
    return await query.get();
  };
  const getAttachmentsForNotifications = async (
    snapshot: FirebaseFirestore.QuerySnapshot<ScheduledNotification>
  ) => {
    const docsWithInitialRemindersToBeSent = snapshot.docs.filter((doc) =>
      doc
        .data()
        .schedule.some((item) => item.template === "initial" && !item.sent)
    );

    const uniqueArms = new Set(
      docsWithInitialRemindersToBeSent.map((doc) => doc.data().arm)
    );

    let interventionAttachments: Attachment[] = [];
    let controlAttachments: Attachment[] = [];

    const attachmentPromises = Array.from(uniqueArms).map(async (arm) => {
      if (arm === "intervention") {
        return { arm, attachments: await getInterventionAttachments() };
      } else {
        return { arm, attachments: await getControlAttachments() };
      }
    });

    const results = await Promise.all(attachmentPromises);

    for (const result of results) {
      if (result.arm === "intervention") {
        interventionAttachments = result.attachments;
      } else {
        controlAttachments = result.attachments;
      }
    }

    return { interventionAttachments, controlAttachments };
  };
  const sendNotifications = async (
    snapshot: FirebaseFirestore.QuerySnapshot<ScheduledNotification>,
    attachments: {
      interventionAttachments: Attachment[];
      controlAttachments: Attachment[];
    },
    appEnv: Environment
  ) => {
    const promiseArray = snapshot.docs.map(async (doc) => {
      const attachmentForDoc =
        doc.data().arm === "intervention"
          ? attachments.interventionAttachments
          : attachments.controlAttachments;

      const hasCompletedFollowup = await hasCompletedFollowupSurvey(
        doc.data().referenceId ?? "",
        followupSurveyCollection
      );

      let unregistered = false;
      if (hasCompletedFollowup) {
        await unregisterFromNotifications(doc.ref);
        unregistered = true;
      }

      functions.logger.log(`Sending notification for ${doc.id}`);
      return sendNotificationAndUpdate(
        doc,
        attachmentForDoc,
        appEnv,
        unregistered
      );
    });

    try {
      await Promise.all(promiseArray);
    } catch (error) {
      functions.logger.error(`Error sending notifications: ${error}`);
      throw error;
    }
  };

  const pendingNotifications = await getPendingNotifications(
    scheduledNotificationsCollection
  );
  const { interventionAttachments, controlAttachments } =
    await getAttachmentsForNotifications(pendingNotifications);

  await sendNotifications(
    pendingNotifications,
    { interventionAttachments, controlAttachments },
    appEnv
  );
};

// Helper functions
const sendEmailUsingSendgrid = async (id: string, template: EmailSendType) => {
  const { to, from, templateId, dynamicTemplateData, attachments } = template;

  const msg = {
    to,
    from,
    templateId,
    dynamicTemplateData,
    attachments: attachments ?? [],
  };

  // console.log({ msg });

  sgMail
    .send(msg)
    .then(() => {
      functions.logger.log(`Email sent to ${id}`);
    })
    .catch((error) => {
      functions.logger.error(`Error sending email to ${id}: ${error}`);
    });
};

const sendSMSUsingTwilio = async (id: string, template: SMSSendType) => {
  const { to, from, body } = template;
  // console.log({ to, from, body });

  twilioClient.messages
    .create({ body, from, to })
    .then(() => {
      functions.logger.log(`SMS sent to ${id}`);
    })
    .catch((error) => {
      functions.logger.error(`Error sending SMS to ${id}: ${error}`);
    });
};
