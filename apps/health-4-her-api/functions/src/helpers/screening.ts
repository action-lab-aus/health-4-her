import { db } from "../firebase-init";
import { Environment } from "../types/client-types";

const getPrefix = (client: Environment): string => {
  switch (client) {
  case "clinical":
    return "cs";
  case "pilot":
    return "ps";
  case "dev":
    return "ds";
  default:
    throw new Error("Invalid client type.");
  }
};

export const getScreeningNumber = async (client: Environment) => {
  if (!client) {
    return null;
  }

  const prefix = getPrefix(client);
  const docRef = db.collection("counters").doc(`number-counter-${client}`);
  const doc = await docRef.get();

  let count: number;

  // Initialize if it doesn't exist
  if (!doc.exists) {
    count = 0;
    await docRef.set({ count });
  } else {
    const data = doc.data();
    if (data == undefined) return null;

    count = data.count;

    count++;
    await docRef.update({ count });
  }

  return `${prefix}` + String(count).padStart(3, "0");
};

export const initializeSlots = (
  order: number[],
  collectionName = "slots-dev",
  startingIndex = 1
) => {
  // Random sequence for Intervention/Control arm assignment
  const batch = db.batch();

  order.forEach((value, index) => {
    const docRef = db
      .collection(collectionName)
      .doc(`slot${(index + startingIndex).toString().padStart(3, "0")}`);
    batch.set(docRef, {
      id: `P${(index + startingIndex).toString().padStart(3, "0")}`,
      arm: value === 0 ? "control" : "intervention",
      isAssigned: false,
      userIds: [],
    });
  });

  return batch.commit();
};
