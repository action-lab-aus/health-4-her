import { Timestamp } from "firebase-admin/firestore";
import { DateTime, Interval } from "luxon";

type BaseDateTime = {
  date: string;
  time: string;
};

export const calculateDaysFromNow = (timestamp: Timestamp): number => {
  const now = DateTime.now();
  const then = DateTime.fromMillis(timestamp.toMillis());

  const interval = Interval.fromDateTimes(then, now);

  return interval.length();
};

export const isAfterNDays =
  (n: number) =>
    (timestamp: Timestamp): boolean => {
      const now = DateTime.now();
      const then = DateTime.fromMillis(timestamp.toMillis());

      return Interval.fromDateTimes(then, now).length("days") > n;
    };

export const timestampAfterNTimeDuration = (
  startTimestampInMillis: number,
  unit: "days" | "minutes",
  value: number
) => {
  return DateTime.fromMillis(startTimestampInMillis)
    .plus({ [unit]: value })
    .toMillis();
};

export const timestampToTzDate = (
  secondsTimestamp: number,
  timezone = "Australia/Melbourne"
): BaseDateTime => {
  const milliseconds = secondsTimestamp * 1000;

  const date = new Date(milliseconds);
  const dateOptions: Intl.DateTimeFormatOptions = {
    timeZone: timezone,
    year: "numeric",
    month: "long",
    day: "numeric",
  };

  const timeOptions: Intl.DateTimeFormatOptions = {
    timeZone: timezone,
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit",
  };

  return {
    date: date.toLocaleDateString("en-AU", dateOptions),
    time: date.toLocaleTimeString("en-AU", timeOptions),
  };
};
