import {
  order_alternate,
  // order_dev,
  order_prod,
} from "./../constants/random-sequence";
import { Environment, ClientType } from "./../types/client-types";
import { db } from "../firebase-init";
import * as functions from "firebase-functions";
import { API_KEY, region } from "../constants/config";
import {
  getScreeningNumber as getScreeningNumberAux,
  initializeSlots as initializeSlotsAux,
} from "../helpers/screening";

export const initializeSlots = functions
  .region(region)
  .https.onRequest(async (request, response) => {
    if (request.headers.authorization !== API_KEY) {
      response.status(403).send("Unauthorized");
      return;
    }

    await initializeSlotsAux(order_prod);
    response.send({ status: "Slots initialized successfully." });
  });

export const assignSlot = functions
  .region(region)
  .https.onCall(async (data, context) => {
    if (!context.auth) {
      console.error("Unauthenticated");
      throw new functions.https.HttpsError(
        "unauthenticated",
        "The function must be called by an authenticated user."
      );
    }

    const appEnv: Environment = data.appEnv;

    let collectionName = "slots-dev";
    let order = order_prod;
    if (appEnv == "clinical") {
      collectionName = "slots-clinical";
    } else if (appEnv == "pilot") {
      collectionName = "slots-pilot";
    } else {
      collectionName = "slots-dev";
      order = order_alternate;
    }

    const userId = context.auth.uid;
    const slotQuery = db
      .collection(collectionName)
      .where("isAssigned", "==", false)
      .orderBy("id")
      .limit(1);

    let querySnapshot = await slotQuery.get();
    if (querySnapshot.empty) {
      await db.runTransaction(async (transaction) => {
        const allSlotsQuery = db.collection(collectionName);
        const allSlotsSnapshot = await transaction.get(allSlotsQuery);

        allSlotsSnapshot.docs.forEach((doc) => {
          transaction.update(doc.ref, { isAssigned: false });
        });
      });

      querySnapshot = await slotQuery.get();
    }

    // If no slot found, initialize slots first
    if (querySnapshot.empty) {
      await initializeSlotsAux(order, collectionName);
      querySnapshot = await slotQuery.get();

      if (querySnapshot.empty) {
        throw new functions.https.HttpsError(
          "internal",
          "Failed to initialize slots"
        );
      }
    }

    const slotDoc = querySnapshot.docs[0];
    const slotData = slotDoc.data();

    slotData.userIds.push(userId);
    await slotDoc.ref.update({
      userIds: slotData.userIds,
      isAssigned: true,
    });

    const randomisationNo = slotData.id;
    const slotArm = slotData.arm;

    return { randomisationNo: randomisationNo, arm: slotArm };
  });

export const getScreeningNumber = functions
  .region(region)
  .https.onCall(async (data) => {
    const screeningNumber = await getScreeningNumberAux(
      data.client as ClientType
    );

    if (screeningNumber == null) {
      throw new functions.https.HttpsError(
        "invalid-argument",
        "Client type is required."
      );
    }

    return screeningNumber;
  });
