import sgMail from "@sendgrid/mail";
import twilio from "twilio";
import fs from "fs";
import csv from "csv-parser";
import { z } from "zod";

const emailSchema = z.string().email();

// Use this command to run the script
// pnpx ts-node -r dotenv/config send-bulk-reminders.ts

type TemplateId = "reminder1" | "reminder2";

const SEND_EMAIL_FLAG = false;
const SEND_SMS_FLAG = false;

const convertToInternationalFormat = (number: string): string => {
  // Remove all non-numeric characters
  const numericOnly = number.replace(/\D/g, "");

  // Check if the number is valid (starts with '0' and has at least 2 digits)
  if (!numericOnly.startsWith("0")) {
    return numericOnly;
  }

  // Remove the leading '0' and add the '+61' prefix
  return "+61" + numericOnly.substring(1);
};

const createPilotReminderBody = (name: string) => `
Dear ${name},

Thank you for your interest in the Health4Her study.
Please access Health4Her here: https://health4her.actionlab.dev

Please complete Health4Her in the next 5 days. We thank you for your valuable contribution to this women's health research.

Thank you,
The Health4Her Team
`;

const createPilotReminder2Body = (name: string) => `
Dear ${name},

Thank you for your interest in the Health4Her study. If you have completed the study - thank you!

If not, you still have time.
Please access Health4Her here: https://health4her.actionlab.dev

Please complete Health4Her in the next 5 days. We thank you for your valuable contribution to this women's health research.

Thank you,
The Health4Her Team
`;

const envVariables = [
  "SENDGRID_API_KEY",
  "SENDGRID_FROM_EMAIL",
  "SENDGRID_TEMPLATE_ID_PILOT_REMINDER",
  "SENDGRID_TEMPLATE_ID_PILOT_REMINDER_2",
  "TWILIO_SID",
  "TWILIO_AUTH_TOKEN",
  "TWILIO_FROM_NUMBER",
] as const;

envVariables.forEach((envVariable) => {
  if (!process.env[envVariable]) {
    throw new Error(`Missing environment variable ${envVariable}`);
  }
});

sgMail.setApiKey(process.env["SENDGRID_API_KEY"] ?? "");

const accountSid = process.env["TWILIO_SID"];
const authToken = process.env["TWILIO_AUTH_TOKEN"];
const twilioClient = twilio(accountSid, authToken);

const recipients: { name: string; email: string; mobile: string }[] = [];

fs.createReadStream("pilot-manual-1.csv")
  .pipe(csv())
  .on("data", (row) => {
    const email = emailSchema.safeParse(row["email "].trim());
    const contactData = {
      name: row["first name"].trim(),
      email: email.success ? row["email "].trim() : "",
      mobile: convertToInternationalFormat(row["mobile"]).trim(),
    };
    recipients.push(contactData);
  })
  .on("end", async () => {
    console.log("CSV file successfully processed");

    const emailPromises = recipients.map(async (recipient) => {
      try {
        await withRetry(() =>
          sendEmailUsingSendgrid(recipient.email, recipient.name, "reminder2")
        );
      } catch (error) {
        console.error(`Error sending email to ${recipient.email}:`, error);
      }
    });
    const smsPromises = recipients.map(async (recipient) => {
      try {
        await withRetry(() =>
          sendSMSUsingTwilio(recipient.name, recipient.mobile, "reminder2")
        );
      } catch (error) {
        console.error(`Error sending SMS to ${recipient.mobile}:`, error);
      }
    });

    await Promise.all(emailPromises);
    await Promise.all(smsPromises);
  });

const sendEmailUsingSendgrid = async (
  toEmail: string,
  name: string,
  template: TemplateId
) => {
  if (!toEmail) {
    console.error(`Invalid email: ${toEmail}`);
    return;
  }

  const templateId =
    template === "reminder1"
      ? "SENDGRID_TEMPLATE_ID_PILOT_REMINDER"
      : "SENDGRID_TEMPLATE_ID_PILOT_REMINDER_2";

  const msg = {
    to: toEmail,
    from: process.env["SENDGRID_FROM_EMAIL"] ?? "",
    templateId: process.env[templateId] ?? "",
    dynamicTemplateData: { firstName: name },
  };

  try {
    if (SEND_EMAIL_FLAG) {
      await sgMail.send(msg);
      console.log(`${template} Email sent to ${toEmail}`);
    } else {
      console.debug(`Email: ${toEmail}, Template: ${template}`);
    }
  } catch (error) {
    console.error(`Error sending email to ${toEmail}:`, error);
  }
};

const sendSMSUsingTwilio = async (
  name: string,
  toNumber: string,
  templateId: TemplateId
) => {
  if (!toNumber.startsWith("+61")) {
    console.error(`Invalid phone number: ${toNumber}`);
    return;
  }

  const from = process.env["TWILIO_FROM_NUMBER"];
  const body =
    templateId === "reminder1"
      ? createPilotReminderBody(name)
      : createPilotReminder2Body(name);

  try {
    if (SEND_SMS_FLAG) {
      await twilioClient.messages.create({ body, from, to: toNumber });
      console.log(`${templateId} SMS sent to ${toNumber}`);
    } else {
      console.debug(`SMS: ${toNumber}, TemplateId: ${templateId}`);
    }
  } catch (error) {
    console.error(`Error sending SMS to ${toNumber}: ${error}`);
  }
};

const withRetry = async <T>(
  fn: () => Promise<T>,
  maxRetries = 3,
  delayMs = 1000
): Promise<T> => {
  let retries = 0;
  while (retries < maxRetries) {
    try {
      return await fn();
    } catch (error) {
      retries++;
      if (retries >= maxRetries) {
        throw new Error(
          `Failed after ${maxRetries} retries: ${handleError(error)}`
        );
      }
      console.warn(`Attempt ${retries} failed. Retrying in ${delayMs}ms...`);
      await new Promise((res) => setTimeout(res, delayMs));
      delayMs *= 2; // Exponential backoff
    }
  }
  throw new Error(`Failed after ${maxRetries} retries.`);
};

const handleError = (error: unknown) => {
  if (typeof error === "string") {
    console.error(error);
  } else if (error instanceof Error) {
    console.error(error.message);
  } else {
    console.error("Unknown error occurred");
  }
};
