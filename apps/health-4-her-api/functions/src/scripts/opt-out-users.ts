import * as admin from "firebase-admin";
import * as fs from "fs";
import { scheduledNotificationsConverter } from "../converters/scheduled-notifications-converter";

type AttributeName = "email" | "sms";
type FirestorePath = "emailAddress" | "mobileNumber";

const batchUpdate = async (
  filePath: string,
  attributeName: AttributeName,
  firestorePath: FirestorePath,
  db: admin.firestore.Firestore
) => {
  const items: string[] = fs
    .readFileSync(filePath, "utf-8")
    .split("\n")
    .map((item) => item.trim())
    .filter((item) => item.length > 0);

  const batch = db.batch();

  for (const item of items) {
    const snapshot = await db
      .collection("scheduled-notifications")
      .withConverter(scheduledNotificationsConverter)
      .where(firestorePath, "==", item)
      .where(`optOutPreferences.${attributeName}.value`, "==", false)
      .get();

    if (snapshot.empty) {
      continue;
    }

    console.log(
      `Updating opt-out preferences for ${attributeName}: `,
      snapshot.docs[0]?.data()[firestorePath]
    );

    snapshot.docs.forEach((doc) => {
      const ref = doc.ref;

      if (!doc.data().optOutPreferences[attributeName].value) {
        batch.update(ref, `optOutPreferences.${attributeName}`, {
          value: true,
          optedOutAt: admin.firestore.Timestamp.now(),
        });
        batch.update(ref, "optOutPreferences.sms", {
          value: true,
          optedOutAt: admin.firestore.Timestamp.now(),
        });
      }
    });
  }

  await batch.commit();

  console.log(
    `Successfully updated opt-out preferences for the given ${attributeName}s.`
  );
};

(async () => {
  admin.initializeApp({
    projectId: process.env.PROJECT_ID,
  });

  if (process.env.FIRESTORE_EMULATOR_HOST) {
    admin.firestore().settings({
      host: process.env.FIRESTORE_EMULATOR_HOST,
      ssl: false,
    });
  }

  const db = admin.firestore();

  await batchUpdate("./emails/emails.txt", "email", "emailAddress", db);
  await batchUpdate("./mobiles/mobiles.txt", "sms", "mobileNumber", db);
})();
