import { emailTemplates } from "../constants/email-templates";
import { bucket } from "../firebase-init";
import { Attachment } from "../types/scheduled-notification-types";

const getSingleAttachment = async (
  filename: string
): Promise<Attachment | null> => {
  try {
    const file = bucket.file(filename);
    const [pdfBuffer] = await file.download();
    const pdfBase64 = pdfBuffer.toString("base64");
    return {
      content: pdfBase64,
      filename: filename,
      type: "application/pdf",
      disposition: "attachment",
    };
  } catch (_) {
    return null;
  }
};

const getAttachmentsAux = async (
  filenames: readonly string[]
): Promise<Attachment[]> => {
  const promises = filenames.map((filename) => getSingleAttachment(filename));
  const results = await Promise.all(promises);

  return results.filter(Boolean) as Attachment[];
};

export const getInterventionAttachments = () =>
  getAttachmentsAux(emailTemplates.initial.attachments.intervention);

export const getControlAttachments = () =>
  getAttachmentsAux(emailTemplates.initial.attachments.control);
