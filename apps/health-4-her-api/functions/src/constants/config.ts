import * as functions from "firebase-functions";

export const envConfig = functions.config()?.env;
export const region = envConfig?.region || "australia-southeast1";
export const API_KEY = envConfig?.["api-key"];
