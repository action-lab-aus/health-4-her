import {
  CollectionMapping,
  Collections,
  Environment,
} from "../types/client-types";

export type CollectionByEnvironment<K extends keyof typeof CollectionMapping> =
  | (typeof collections)["dev"][K]
  | (typeof collections)["pilot"][K]
  | (typeof collections)["clinical"][K];

type ValuesOf<T> = T[keyof T];
export type CollectionInEnvironment<E extends Environment> = ValuesOf<
  (typeof collections)[E]
>;

export const collections: Collections = {
  dev: {
    baselineSurvey: "baseline-survey-dev",
    postInterventionSurvey: "post-intervention-survey-dev",
    surveyDiscontinue: "survey-discontinue-dev",
    scheduledNotifications: "scheduled-notifications-dev",
    followupSurvey: "followup-survey-dev",
    userProgress: "userProgressDev",
  } as const,
  pilot: {
    baselineSurvey: "baseline-survey-pilot",
    postInterventionSurvey: "post-intervention-survey-pilot",
    surveyDiscontinue: "survey-discontinue-pilot",
    scheduledNotifications: "scheduled-notifications-pilot",
    followupSurvey: "followup-survey-pilot",
    userProgress: "userProgressPilot",
  } as const,
  clinical: {
    baselineSurvey: "baseline-survey-clinical",
    postInterventionSurvey: "post-intervention-survey-clinical",
    surveyDiscontinue: "survey-discontinue-clinical",
    scheduledNotifications: "scheduled-notifications-clinical",
    followupSurvey: "followup-survey-clinical",
    userProgress: "userProgressClinical",
  } as const,
} as const;
