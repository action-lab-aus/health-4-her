import { ScheduledNotificationInterval } from "../types/scheduled-notification-types";

export const NOTIFICATION_PROD_INTERVALS: ScheduledNotificationInterval[] = [
  {
    value: 60,
    unit: "minutes",
    template: "initial",
    description: "Initial email/sms after Post-intervention survey",
  },
  {
    value: 28,
    unit: "days",
    template: "questionnaire",
    description: "Email/sms for Follow-up questionnaire",
  },
  {
    value: 30,
    unit: "days",
    template: "reminder",
    description: "D+2 Reminder for Follow-up questionnaire",
  },
  {
    value: 34,
    unit: "days",
    template: "reminder2",
    description: "D+6 Reminder for Follow-up questionnaire",
  },
];

export const NOTIFICATION_DEV_INTERVALS: ScheduledNotificationInterval[] = [
  {
    value: 1,
    unit: "minutes",
    template: "initial",
    description: "Initial email/sms after Post-intervention survey",
  },
  {
    value: 3,
    unit: "minutes",
    template: "questionnaire",
    description: "Email/sms for Follow-up questionnaire",
  },
  {
    value: 6,
    unit: "minutes",
    template: "reminder",
    description: "D+2 reminder email/sms for Follow-up questionnaire",
  },
  {
    value: 9,
    unit: "minutes",
    template: "reminder2",
    description: "D+6 reminder email/sms for Follow-up questionnaire",
  },
];
