import { Arm } from "../types/client-types";
import { NotificationEventType } from "../types/scheduled-notification-types";

interface ReminderSMSTemplate {
  key: NotificationEventType;
  body: string;
}

export const INITIAL_SMS_TEMPLATE = (
  firstName: string,
  _: string,
  arm: Arm
): ReminderSMSTemplate => {
  return {
    key: "initial",
    body: `
Dear ${firstName},

Thank you for participating in the Health4Her study. There's a follow-up questionnaire due in 30 days. We will provide you with more details closer to the date.

${
  arm === "intervention"
    ? "Alcohol brochure: https://health4her.actionlab.dev/pdf/h4h-brochure.pdf"
    : ""
}
Nutrition brochure: https://health4her.actionlab.dev/pdf/healthy-eating-for-adult-guidelines.pdf
Participation information sheet: https://health4her.actionlab.dev/pdf/h4h-participant-information-sheet.pdf

Thank you,
The Health4Her Team`,
  };
};

export const QUESTIONNAIRE_SMS_TEMPLATE = (
  firstName: string,
  surveyLink: string
): ReminderSMSTemplate => ({
  key: "questionnaire",
  body: `
Dear ${firstName},

Thank you for participating in the Health4Her study. Your final survey is now due to be completed online, and will take 15 mins. Complete this survey and we will pledge $10 to breast cancer research.
Access the survey here: ${surveyLink}

If you’d like to withdraw please email health4her@easternhealth.org.au

Thank you,
The Health4Her Team`,
});

export const REMINDER_SMS_TEMPLATE = (
  firstName: string,
  surveyLink: string
): ReminderSMSTemplate => ({
  key: "reminder",
  body: `
Dear ${firstName},

If you have completed the Health4Her final survey - thank you! If not, you still have time. It will take 15 mins. Complete this survey and we will pledge $10 to breast cancer research.
Access the survey here: ${surveyLink}

If you’d like to withdraw please email health4her@easternhealth.org.au

Thank you,
The Health4Her Team
`,
});

export const REMINDER_SMS_TEMPLATE_2 = (firstName: string, surveyLink: string) => ({
  key: "reminder-2",
  body: `
Dear ${firstName},

If you have completed the Health4Her final survey - thank you! If not, you still have time. If we don't receive your response, we'll try getting in touch by telephone. Complete this survey and we will pledge $10 to breast cancer research.
Access the survey here: ${surveyLink}

If you’d like to withdraw please email health4her@easternhealth.org.au

Thank you,
The Health4Her Team
`,
});

export type SMSTemplateType =
  | typeof INITIAL_SMS_TEMPLATE
  | typeof QUESTIONNAIRE_SMS_TEMPLATE
  | typeof REMINDER_SMS_TEMPLATE
  | typeof REMINDER_SMS_TEMPLATE_2;
