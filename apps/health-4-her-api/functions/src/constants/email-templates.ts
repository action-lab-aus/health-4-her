import * as functions from "firebase-functions";

const TemplateId = functions.config()?.sendgrid["template-id"] as {
  initial: string;
  questionnaire: string;
  reminder: string;
  reminder2: string;
};

const SendgridConfig = functions.config()?.sendgrid as {
  "attachment-brochure-intervention": string;
  "attachment-brochure-control": string;
  "attachment-pis": string;
};

export const templateId = {
  initial: TemplateId.initial,
  questionnaire: TemplateId.questionnaire,
  reminder: TemplateId.reminder,
};

type AttachmentsByArm = {
  control: readonly string[];
  intervention: readonly string[];
};

export type EmailTemplateWithAttachmentType = {
  initial: {
    templateId: string;
    attachments: AttachmentsByArm;
  };
  questionnaire: { templateId: string; attachments: AttachmentsByArm };
  reminder: { templateId: string; attachments: AttachmentsByArm };
  reminder2: { templateId: string; attachments: AttachmentsByArm };
};

export const emailTemplates: EmailTemplateWithAttachmentType = {
  initial: {
    templateId: TemplateId.initial,
    attachments: {
      control: [
        SendgridConfig["attachment-brochure-control"],
        SendgridConfig["attachment-pis"],
      ],
      intervention: [
        SendgridConfig["attachment-brochure-intervention"],
        SendgridConfig["attachment-brochure-control"],
        SendgridConfig["attachment-pis"],
      ],
    },
  },
  questionnaire: {
    templateId: TemplateId.questionnaire,
    attachments: { control: [], intervention: [] },
  },
  reminder: {
    templateId: TemplateId.reminder,
    attachments: { control: [], intervention: [] },
  },
  reminder2: {
    templateId: TemplateId.reminder2,
    attachments: { control: [], intervention: [] },
  },
};
