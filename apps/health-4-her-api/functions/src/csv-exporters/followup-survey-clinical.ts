import { CollectionInEnvironment } from "./../constants/collections";
import { timestampToTzDate } from "../utils/date-utils";
import { db } from "../firebase-init";
import { createObjectCsvWriter as createCsvWriter } from "csv-writer";
import { getArmForReferenceId } from "../collections/scheduled-notifications";

const FOLLOWUP_SURVEY_COLLECTION_NAME = "followup-survey-clinical";

const CSV_OUTPUT_FILE_NAME = "./followup-survey-clinical.csv";

const fetchSnapshotDataFromFirestore = async (
  collectionName: CollectionInEnvironment<"clinical">
) => {
  const snapshot = await db.collection(collectionName).get();
  return snapshot.docs.map((doc) => ({
    ...doc.data(),
    docId: doc.id,
  })) as any[];
};

type Prefix = "bhvquant" | "intquant";

enum DrinksKeySequence {
  bhvquant = "bhvquant",
  intquant = "intquant",
}

const BHV_QUANT_DRINKS_KEY_SEQUENCE = [
  "bhvquant_avred",
  "bhvquant_avwhite",
  "bhvquant_avsparkle",
  "bhvquant_smlred",
  "bhvquant_smlwhite",
  "bhvquant_shot",
  "bhvquant_cocktail",
  "bhvquant_rtd",
  "bhvquant_beer",
  "bhvquant_oth1a",
  "bhvquant_oth1b",
  "bhvquant_oth2a",
  "bhvquant_oth2b",
];

const INT_QUANT_DRINKS_KEY_SEQUENCE = [
  "intquant_avred",
  "intquant_avwhite",
  "intquant_avsparkle",
  "intquant_smlred",
  "intquant_smlwhite",
  "intquant_shot",
  "intquant_cocktail",
  "intquant_rtd",
  "intquant_beer",
  "intquant_oth1a",
  "intquant_oth1b",
  "intquant_oth2a",
  "intquant_oth2b",
];

interface DrinkMap {
  [key: string]: string | number;
}

function addOtherKeysToSet(
  record: any,
  outerKey: string,
  prefix: Prefix,
  set: Set<DrinkMap>
) {
  if (record[outerKey] == undefined) return;

  Object.keys(record[outerKey]).forEach((key) => {
    const otherMatch = key.match(/^other - (\d)\(([^)]*)\)$/);

    if (otherMatch) {
      const num = otherMatch[1];
      set.add({ [`${prefix}_oth${num}a`]: otherMatch[2] });
      set.add({ [`${prefix}_oth${num}b`]: record[outerKey][key] });
    } else {
      set.add({ [key]: record[outerKey][key] });
    }
  });
}

function getMappedResult(
  sequence: string[],
  drinksArray: DrinkMap[]
): DrinkMap {
  const drinksMap: DrinkMap = Object.fromEntries(
    drinksArray.map((drink) => [Object.keys(drink)[0], Object.values(drink)[0]])
  );
  const result: DrinkMap = {};
  sequence.forEach((key) => {
    if (drinksMap[key] !== undefined) {
      result[key] = drinksMap[key];
    }
  });
  return result;
}

// TODO: Add type safety to collections
const exportFirestoreToCsv2 = async () => {
  const snapshotData = await fetchSnapshotDataFromFirestore(
    FOLLOWUP_SURVEY_COLLECTION_NAME
  );

  let records = structuredClone(snapshotData);

  records = records.map((record) => ({
    referenceId: record.referenceId,
    ...record.aboutYou,
    ...record.demographic,
    ...record.pastMonthBehaviour,
    ...record.pastMonthAlcohol,
    ...record.pastMonthSecondaryBehaviour,
    ...record.outcomes,
    ...record.secondaryOutcomes,
    ...record.secondaryOutcomesPart2,
    ...record.secondaryOutcomesPart3,
    ...record.riskKnowledge,
    ...record.knowledge,
    ...record.programEval,
    ...record.programEvalSharing,
    ...record.programEvalMessaging,
    ...record.programEvalMotivation,
    ...record.programEvalOverallMessaging,
    ...record.programEvalOpinion,
    ...record.programEvalFeedback,
    docId: record.docId,
  }));

  const addOtherDrinksToKeys = (arr: string[], prefix: Prefix): string[] => {
    const sequence =
      prefix === DrinksKeySequence.bhvquant
        ? BHV_QUANT_DRINKS_KEY_SEQUENCE
        : INT_QUANT_DRINKS_KEY_SEQUENCE;
    return arr.concat(sequence);
  };

  const flattenDrinksObject = (prefix: Prefix, record: any) => {
    const sequence =
      prefix === DrinksKeySequence.bhvquant
        ? BHV_QUANT_DRINKS_KEY_SEQUENCE
        : INT_QUANT_DRINKS_KEY_SEQUENCE;
    const outerKey =
      prefix === DrinksKeySequence.bhvquant ? "bhvDrinkOnDay" : "intDrinkOnDay";
    const set = new Set<DrinkMap>();
    addOtherKeysToSet(record, outerKey, prefix, set);
    const drinksArray = Array.from(set);
    const result = getMappedResult(sequence, drinksArray);

    return result;
  };

  const aboutYouKeys = ["first_name", "last_name", "dob"] as const;

  const pastMonthBehaviourKeys = addOtherDrinksToKeys(
    ["bhvfreq_phys", "bhvquant_phys_mins", "bhvfreq_sun", "bhvfreq_alc"],
    "bhvquant"
  );

  const pastMonthSecondaryBehaviourKeys = ["bhvfreq_cig", "bhvfreq_veg"];

  const outcomesKeys = addOtherDrinksToKeys(
    [
      "int_phys",
      "int_sun",
      "int_alc",
      "int_cig",
      "int_veg",
      "intfreq_phys",
      "intquant_phys_mins",
      "intfreq_sun",
      "intfreq_alc",
    ],
    "intquant"
  );

  const secondaryOutcomesKeys = ["intfreq_cig", "intfreq_veg"];

  const riskKnowledgeKeys = [
    "bcrisk_famhist",
    "bcrisk_phys",
    "bcrisk_sunexpo",
    "bcrisk_alc",
    "bcrisk_cig",
    "bcrisk_ovweight",
  ] as const;

  const knowledgeKeys = [
    "knowl_phys",
    "knowl_sunscr",
    "knowl_sd",
    "knowl_redwine",
    "knowl_alcguideline",
    "knowl_diet",
  ] as const;

  const programEvalKeys = [
    "h4h_infoaccept",
    "h4h_stdservice",
    "h4h_easyuse",
    "h4h_timeaccept",
    "h4h_animaccept",
  ];

  const programEvalSharingKeys = [
    "infoshare",
    "infoshare_who",
    "infoshare_who_oth",
    "infoshare_typ",
  ];

  const programEvalMessagingKeys = [
    "h4hmsg_newinfo",
    "h4hmsg_easy",
    "h4hmsg_trustw",
    "h4hmsg_relevant",
    "h4hmsg_useful",
  ];

  const programEvalMotivationKeys = [
    "motivbhv",
    "motivbhv_typ",
    "motivbhv_desc",
    "h4h_interest",
    "influ_screen",
    "influ_screen_less",
    "msg_insenstv",
    "msg_insenstv_desc",
    "msg_stigma",
    "msg_stigma_desc",
  ];

  const programEvalOverallMessagingKeys = [
    "h4h_think",
    "h4h_persrelevant",
    "h4h_motivates",
    "h4h_concerned",
    "h4h_easyundrstnd",
    "h4h_believable",
  ];

  const programEvalOpinionKeys = [
    "h4h_negattitudes",
    "h4h_negattitudes_desc",
    "h4h_blame",
    "h4h_blame_desc",
    "h4h_simple",
    "h4h_simple_desc",
  ];

  const programEvalFeedbackKeys = ["h4h_suggest", "qual_feedback"];

  const keys = [
    "referenceId",
    ...aboutYouKeys,
    ...pastMonthBehaviourKeys,
    ...pastMonthSecondaryBehaviourKeys,
    ...outcomesKeys,
    ...secondaryOutcomesKeys,
    ...riskKnowledgeKeys,
    ...knowledgeKeys,
    ...programEvalKeys,
    ...programEvalSharingKeys,
    ...programEvalMessagingKeys,
    ...programEvalMotivationKeys,
    ...programEvalOverallMessagingKeys,
    ...programEvalOpinionKeys,
    ...programEvalFeedbackKeys,
  ];

  const flattenedRecordsPromises = records.map(async (record, idx) => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const flattened: any = {
      ...record,
      ...flattenDrinksObject("bhvquant", record),
      ...flattenDrinksObject("intquant", record),
    };
    delete flattened.intDrinkOnDay;
    delete flattened.bhvDrinkOnDay;

    // Let's add other embedded data that is outside of the baseline survey
    const { date: startedDate, time: startedTime } = timestampToTzDate(
      snapshotData[idx].startedAt?._seconds ?? ""
    );

    const { date: completedDate, time: completedTime } = timestampToTzDate(
      snapshotData[idx].completedAt?._seconds ?? ""
    );

    const arm = await getArmForReferenceId(record.referenceId, "clinical");

    flattened["startdate"] = startedDate;
    flattened["starttime"] = startedTime;
    flattened["random_arm"] = arm ?? "";
    flattened["enddate"] = completedDate;
    flattened["endtime"] = completedTime;
    flattened["devicetype"] = snapshotData[idx].deviceType;
    flattened["finished"] = snapshotData[idx]?.completedAt ? "1" : "0";

    return flattened;
  });

  const flattenedRecords = await Promise.all(flattenedRecordsPromises);

  const embeddedDataPre = [
    { id: "startdate", title: "startdate" },
    { id: "starttime", title: "starttime" },
    { id: "random_arm", title: "random_arm" },
  ];

  const embeddedDataPost = [
    { id: "enddate", title: "enddate" },
    { id: "endtime", title: "endtime" },
    { id: "finished", title: "finished" },
    { id: "devicetype", title: "devicetype" },
  ];

  const header = embeddedDataPre
    .concat(keys.map((key) => ({ id: key, title: key })))
    .concat(embeddedDataPost);

  const csvWriter = createCsvWriter({
    path: CSV_OUTPUT_FILE_NAME,
    header: header,
  });

  const sortedFlattenedRecords = flattenedRecords.sort((a, b) => {
    const aDateTime = new Date(`${a.startdate} ${a.starttime}`);
    const bDateTime = new Date(`${b.startdate} ${b.starttime}`);
    return aDateTime.getTime() - bDateTime.getTime();
  });
  await csvWriter.writeRecords(sortedFlattenedRecords);
  console.log(`Data exported to ${CSV_OUTPUT_FILE_NAME}`);
};

exportFirestoreToCsv2();
