import { CollectionInEnvironment } from "./../constants/collections";
import { getAssignedArmForUser } from "./../helpers/notifications";
import { timestampToTzDate } from "../utils/date-utils";
import { db } from "../firebase-init";
import { createObjectCsvWriter as createCsvWriter } from "csv-writer";

const BASELINE_SURVEY_COLLECTION_NAME = "baseline-survey-clinical";
const POST_INTERVENTION_SURVEY_COLLECTION_NAME =
  "post-intervention-survey-clinical";
const SURVEY_DISCONTINUE_COLLECTION_NAME = "survey-discontinue-clinical";
const USER_PROGRESS_COLLECTION_NAME = "userProgressClinical";
const SCHEDULED_NOTIFICATIONS_COLLECTION_NAME =
  "scheduled-notifications-clinical";

const CSV_OUTPUT_FILE_NAME = "./baseline-survey-clinical.csv";

const fetchSnapshotDataFromFirestore = async (
  collectionName: CollectionInEnvironment<"clinical">
) => {
  const snapshot = await db.collection(collectionName).get();
  return snapshot.docs.map((doc) => ({
    ...doc.data(),
    docId: doc.id,
  })) as any[];
};

type Prefix = "bhvquant" | "intquant";

enum DrinksKeySequence {
  bhvquant = "bhvquant",
  intquant = "intquant",
}

const BHV_QUANT_DRINKS_KEY_SEQUENCE = [
  "bhvquant_avred",
  "bhvquant_avwhite",
  "bhvquant_avsparkle",
  "bhvquant_smlred",
  "bhvquant_smlwhite",
  "bhvquant_shot",
  "bhvquant_cocktail",
  "bhvquant_rtd",
  "bhvquant_beer",
  "bhvquant_oth1a",
  "bhvquant_oth1b",
  "bhvquant_oth2a",
  "bhvquant_oth2b",
];

const INT_QUANT_DRINKS_KEY_SEQUENCE = [
  "intquant_avred",
  "intquant_avwhite",
  "intquant_avsparkle",
  "intquant_smlred",
  "intquant_smlwhite",
  "intquant_shot",
  "intquant_cocktail",
  "intquant_rtd",
  "intquant_beer",
  "intquant_oth1a",
  "intquant_oth1b",
  "intquant_oth2a",
  "intquant_oth2b",
];

interface DrinkMap {
  [key: string]: string | number;
}

function addOtherKeysToSet(
  record: any,
  outerKey: string,
  prefix: Prefix,
  set: Set<DrinkMap>
) {
  if (record[outerKey] == undefined) return;

  Object.keys(record[outerKey]).forEach((key) => {
    const otherMatch = key.match(/^other - (\d)\(([^)]*)\)$/);

    if (otherMatch) {
      const num = otherMatch[1];
      set.add({ [`${prefix}_oth${num}a`]: otherMatch[2] });
      set.add({ [`${prefix}_oth${num}b`]: record[outerKey][key] });
    } else {
      set.add({ [key]: record[outerKey][key] });
    }
  });
}

function getMappedResult(
  sequence: string[],
  drinksArray: DrinkMap[]
): DrinkMap {
  const drinksMap: DrinkMap = Object.fromEntries(
    drinksArray.map((drink) => [Object.keys(drink)[0], Object.values(drink)[0]])
  );

  const result: DrinkMap = {};
  sequence.forEach((key) => {
    if (drinksMap[key] !== undefined) {
      result[key] = drinksMap[key];
    }
  });
  return result;
}

// TODO: Add type safety to collections
const exportFirestoreToCsv2 = async () => {
  const snapshotData = await fetchSnapshotDataFromFirestore(
    BASELINE_SURVEY_COLLECTION_NAME
  );
  const postInterventionSurveySnapshotData =
    await fetchSnapshotDataFromFirestore(
      POST_INTERVENTION_SURVEY_COLLECTION_NAME
    );
  const surveyDiscontinueSnapshotData = await fetchSnapshotDataFromFirestore(
    SURVEY_DISCONTINUE_COLLECTION_NAME
  );
  const userProgressSnapshotData = await fetchSnapshotDataFromFirestore(
    USER_PROGRESS_COLLECTION_NAME
  );
  const scheduledNotificationsSnapshotData =
    await fetchSnapshotDataFromFirestore(
      SCHEDULED_NOTIFICATIONS_COLLECTION_NAME
    );

  let records = structuredClone(snapshotData);

  records = records.map((record) => ({
    ...record.aboutYou,
    ...record.demographic,
    ...record.pastMonthBehaviour,
    ...record.pastMonthAlcohol,
    ...record.pastMonthSecondaryBehaviour,
    ...record.outcomes,
    ...record.secondaryOutcomes,
    ...record.secondaryOutcomesPart2,
    ...record.secondaryOutcomesPart3,
    ...record.riskKnowledge,
    ...record.knowledge,
    docId: record.docId,
  }));

  const addOtherDrinksToKeys = (arr: string[], prefix: Prefix): string[] => {
    const sequence =
      prefix === DrinksKeySequence.bhvquant
        ? BHV_QUANT_DRINKS_KEY_SEQUENCE
        : INT_QUANT_DRINKS_KEY_SEQUENCE;
    return arr.concat(sequence);
  };

  const flattenDrinksObject = (prefix: Prefix, record: any) => {
    const sequence =
      prefix === DrinksKeySequence.bhvquant
        ? BHV_QUANT_DRINKS_KEY_SEQUENCE
        : INT_QUANT_DRINKS_KEY_SEQUENCE;
    const outerKey =
      prefix === DrinksKeySequence.bhvquant ? "bhvDrinkOnDay" : "intDrinkOnDay";

    const set = new Set<DrinkMap>();
    addOtherKeysToSet(record, outerKey, prefix, set);
    const drinksArray = Array.from(set);
    const result = getMappedResult(sequence, drinksArray);

    return result;
  };

  const aboutYouKeys = [
    "first_name",
    "last_name",
    "comm_pref",
    "email",
    "ph_mobile",
    "ph_home",
    "postcode",
    "dob",
    "birthplace",
    "birthplace_oth",
    "lang",
    "lang_oth",
    "atsi",
    "gender",
    "gender_oth",
    "sex",
    "sex_oth",
    "lgbt",
    "edu",
    "disable",
  ] as const;

  const pastMonthBehaviourKeys = addOtherDrinksToKeys(
    ["bhvfreq_phys", "bhvquant_phys_mins", "bhvfreq_sun", "bhvfreq_alc"],
    "bhvquant"
  );

  const pastMonthSecondaryBehaviourKeys = ["bhvfreq_cig", "bhvfreq_veg"];

  const outcomesKeys = addOtherDrinksToKeys(
    [
      "int_phys",
      "int_sun",
      "int_alc",
      "int_cig",
      "int_veg",
      "intfreq_phys",
      "intquant_phys_mins",
      "intfreq_sun",
      "intfreq_alc",
    ],
    "intquant"
  );

  const secondaryOutcomesKeys = ["intfreq_cig", "intfreq_veg"];

  const riskKnowledgeKeys = [
    "bcrisk_famhist",
    "bcrisk_phys",
    "bcrisk_sunexpo",
    "bcrisk_alc",
    "bcrisk_cig",
    "bcrisk_ovweight",
  ] as const;

  const knowledgeKeys = [
    "knowl_phys",
    "knowl_sunscr",
    "knowl_sd",
    "knowl_redwine",
    "knowl_alcguideline",
    "knowl_diet",
  ] as const;

  const keys = [
    ...aboutYouKeys,
    ...pastMonthBehaviourKeys,
    ...pastMonthSecondaryBehaviourKeys,
    ...outcomesKeys,
    ...secondaryOutcomesKeys,
    ...riskKnowledgeKeys,
    ...knowledgeKeys,
  ];

  const userProgressMapping = {
    activity1: "actv1_bcrisk",
    activity2: "actv2_lowalcrisk",
    activity3: "actv3_hlthimprv",
    activity4: "actv4_drinkless",
  };

  const actv1 = userProgressMapping["activity1"];
  const actv2 = userProgressMapping["activity2"];
  const actv3 = userProgressMapping["activity3"];
  const actv4 = userProgressMapping["activity4"];

  const flattenedRecordsPromises = records.map(async (record, idx) => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const flattened: any = {
      ...record,
      ...flattenDrinksObject("bhvquant", record),
      ...flattenDrinksObject("intquant", record),
    };

    delete flattened.intDrinkOnDay;
    delete flattened.bhvDrinkOnDay;

    // Let's add other embedded data that is outside of the baseline survey
    const { date: startedDate, time: startedTime } = timestampToTzDate(
      snapshotData[idx].startedAt?._seconds ?? ""
    );

    const { date: completedDate, time: completedTime } = timestampToTzDate(
      snapshotData[idx].completedAt?._seconds ?? ""
    );

    const userAllocation = await getAssignedArmForUser(
      snapshotData[idx].docId,
      "clinical"
    );

    const userPostInterventionSurveyData =
      postInterventionSurveySnapshotData.find(
        (data) => data.docId === snapshotData[idx].docId
      );
    const userSurveyDiscontinueData = surveyDiscontinueSnapshotData.find(
      (data) => data.docId === snapshotData[idx].docId
    );
    const userProgressData = userProgressSnapshotData.find(
      (data) => data.docId === snapshotData[idx].docId
    );
    const scheduledNotificationsData = scheduledNotificationsSnapshotData.find(
      (data) => data.docId === snapshotData[idx].docId
    );

    // console.log({ scheduledNotificationsData });

    const discontinuedReason =
      userSurveyDiscontinueData?.discontinuedReason ?? "";

    if (discontinuedReason.startsWith("Other")) {
      flattened["discont"] = "7";
      flattened["discont_oth"] = discontinuedReason.split("Other - ")[1];
    } else {
      flattened["discont"] = discontinuedReason;
    }

    if (!scheduledNotificationsData?.["referenceId"]) {
      console.log(`No referenceId for ${snapshotData[idx].docId}`);
    }

    flattened["startdate"] = startedDate;
    flattened["starttime"] = startedTime;
    flattened["reference_id"] = scheduledNotificationsData?.["referenceId"];
    flattened["cscreen_no"] = snapshotData[idx].screeningNumber;
    flattened["random_arm"] = userAllocation?.arm ?? "";
    flattened["random_no"] = userAllocation?.id ?? "";
    flattened["enddate"] = completedDate;
    flattened["endtime"] = completedTime;
    flattened["devicetype"] = snapshotData[idx].deviceType;
    flattened["finished"] = userPostInterventionSurveyData?.completedAt
      ? "1"
      : "0";
    flattened["withdraw"] = userSurveyDiscontinueData
      ? userSurveyDiscontinueData.contactForFollowUpSurvey
        ? "1"
        : "0"
      : "";

    const addActvData = (actv: string, activity: string) => {
      const { date: actv_start_date, time: actv_start_time } =
        userProgressData?.[activity]?.startedAt?._seconds
          ? timestampToTzDate(userProgressData?.[activity]?.startedAt?._seconds)
          : { date: "", time: "" };
      const { date: actv_answered_at_date, time: actv_answered_at_time } =
        userProgressData?.[activity]?.answeredAt._seconds
          ? timestampToTzDate(
            userProgressData?.[activity]?.answeredAt?._seconds
          )
          : { date: "", time: "" };
      const { date: actv_completed_at_date, time: actv_completed_at_time } =
        userProgressData?.[activity]?.completedAt?._seconds
          ? timestampToTzDate(
            userProgressData?.[activity]?.completedAt?._seconds
          )
          : { date: "", time: "" };
      flattened[`${actv}_started_at_date`] = actv_start_date ?? "";
      flattened[`${actv}_started_at_time`] = actv_start_time ?? "";
      flattened[`${actv}_answered_at_date`] = actv_answered_at_date ?? "";
      flattened[`${actv}_answered_at_time`] = actv_answered_at_time ?? "";
      flattened[`${actv}_completed_at_date`] = actv_completed_at_date ?? "";
      flattened[`${actv}_completed_at_time`] = actv_completed_at_time ?? "";

      const answer = userProgressData?.[activity]?.answer;
      flattened[actv] = Array.isArray(answer)
        ? answer.join(", ")
        : answer ?? "";
    };

    addActvData(actv1, "activity1");
    addActvData(actv2, "activity2");
    addActvData(actv3, "activity3");
    addActvData(actv4, "activity4");
    flattened["actv-last-page"] = userProgressData?.page ?? "";

    const addVideoData = (video_key: string, video: string) => {
      const { date: video_start_date, time: video_start_time } =
        userProgressData?.[video]?.startedAt?._seconds
          ? timestampToTzDate(userProgressData?.[video]?.startedAt?._seconds)
          : { date: "", time: "" };
      const { date: video_completed_at_date, time: video_completed_at_time } =
        userProgressData?.[video]?.completedAt?._seconds
          ? timestampToTzDate(userProgressData?.[video]?.completedAt?._seconds)
          : { date: "", time: "" };
      flattened[`${video_key}_started_at_date`] = video_start_date;
      flattened[`${video_key}_started_at_time`] = video_start_time;
      flattened[`${video_key}_completed_at_date`] = video_completed_at_date;
      flattened[`${video_key}_completed_at_time`] = video_completed_at_time;
    };

    addVideoData("vid1", "video1");
    addVideoData("vid2", "video2");
    addVideoData("vid3", "video3");
    addVideoData("vid4", "video4");
    addVideoData("vid5_intrvn", "video5Intervention");
    addVideoData("vid5_ctrl", "video5Control");

    // Post-intervention survey
    flattened["pis_phys"] =
      userPostInterventionSurveyData?.outcomes?.increaseExercise;
    flattened["pis_sun"] =
      userPostInterventionSurveyData?.outcomes?.increaseSunscreen;
    flattened["pis_veg"] =
      userPostInterventionSurveyData?.outcomes?.increaseVegetables;
    flattened["pis_cig"] =
      userPostInterventionSurveyData?.outcomes?.reduceTobaccoProducts;
    flattened["pis_alc"] =
      userPostInterventionSurveyData?.outcomes?.reduceAlcohol;
    const { date: pisCompletedDate, time: pisCompletedTime } =
      userPostInterventionSurveyData?.completedAt?._seconds
        ? timestampToTzDate(
          userPostInterventionSurveyData?.completedAt?._seconds
        )
        : { date: "", time: "" };
    flattened["pis_completed_date"] = pisCompletedDate;
    flattened["pis_completed_time"] = pisCompletedTime;

    return flattened;
  });

  const flattenedRecords = await Promise.all(flattenedRecordsPromises);

  const embeddedDataPre = [
    { id: "startdate", title: "startdate" },
    { id: "starttime", title: "starttime" },
    { id: "cscreen_no", title: "cscreen_no" },
    { id: "reference_id", title: "reference_id" },
  ];

  const getActvHeaders = (actv: string) => {
    return [
      {
        id: `${actv}_started_at_date`,
        title: `${actv}_started_at_date`,
      },
      {
        id: `${actv}_started_at_time`,
        title: `${actv}_started_at_time`,
      },
      {
        id: `${actv}_answered_at_date`,
        title: `${actv}_answered_at_date`,
      },
      {
        id: `${actv}_answered_at_time`,
        title: `${actv}_answered_at_time`,
      },
      {
        id: `${actv}_completed_at_date`,
        title: `${actv}_completed_at_date`,
      },
      {
        id: `${actv}_completed_at_time`,
        title: `${actv}_completed_at_time`,
      },
      {
        id: actv,
        title: actv,
      },
    ];
  };

  const getVideoHeaders = (video: string) => {
    return [
      {
        id: `${video}_started_at_date`,
        title: `${video}_started_at_date`,
      },
      {
        id: `${video}_started_at_time`,
        title: `${video}_started_at_time`,
      },
      {
        id: `${video}_completed_at_date`,
        title: `${video}_completed_at_date`,
      },
      {
        id: `${video}_completed_at_time`,
        title: `${video}_completed_at_time`,
      },
    ];
  };

  const tool = [
    ...getActvHeaders(actv1),
    ...getActvHeaders(actv2),
    ...getActvHeaders(actv3),
    ...getActvHeaders(actv4),
    ...getVideoHeaders("vid1"),
    ...getVideoHeaders("vid2"),
    ...getVideoHeaders("vid3"),
    ...getVideoHeaders("vid4"),
    ...getVideoHeaders("vid5_intrvn"),
    ...getVideoHeaders("vid5_ctrl"),
    { id: "actv-last-page", title: "actv-last-page" },
  ];

  const embeddedDataPost = [
    { id: "random_arm", title: "random_arm" },
    { id: "random_no", title: "random_no" },
    { id: "enddate", title: "enddate" },
    { id: "endtime", title: "endtime" },
    { id: "discont", title: "discont" },
    { id: "discont_oth", title: "discont_oth" },
    { id: "withdraw", title: "withdraw" },
    { id: "finished", title: "finished" },
    { id: "devicetype", title: "devicetype" },
  ];

  const piSurveyData = [
    { id: "pis_phys", title: "pis_phys" },
    { id: "pis_sun", title: "pis_sun" },
    { id: "pis_alc", title: "pis_alc" },
    { id: "pis_cig", title: "pis_cig" },
    { id: "pis_veg", title: "pis_veg" },
    { id: "pis_completed_date", title: "pis_completed_date" },
    { id: "pis_completed_time", title: "pis_completed_time" },
  ];

  const header = embeddedDataPre
    .concat(keys.map((key) => ({ id: key, title: key })))
    .concat(tool)
    .concat(embeddedDataPost)
    .concat(piSurveyData);

  const csvWriter = createCsvWriter({
    path: CSV_OUTPUT_FILE_NAME,
    header: header,
  });

  const sortedFlattenedRecords = flattenedRecords.sort((a, b) => {
    return a.cscreen_no.localeCompare(b.cscreen_no);
  });
  await csvWriter.writeRecords(sortedFlattenedRecords);
  console.log(`Data exported to ${CSV_OUTPUT_FILE_NAME}`);
};

exportFirestoreToCsv2();
