import { createScheduledNotifications } from "../helpers/notifications";
import * as functions from "firebase-functions";
import { collections } from "../constants/collections";
import { Environment } from "../types/client-types";

const envConfig = functions.config()?.env;
const region = envConfig?.region || "australia-southeast1";

const createTriggeredNotificationFunction = (appEnv: Environment) => {
  const userProgressCollection =
    collections[appEnv].userProgress;
  const baselineSurveyCollection = collections[appEnv].baselineSurvey;
  const scheduledNotificationsCollection =
    collections[appEnv].scheduledNotifications;

  return functions
    .region(region)
    .firestore.document(`${userProgressCollection}/{userId}`)
    .onCreate(async (snapshot) => {
      await createScheduledNotifications(
        snapshot,
        baselineSurveyCollection,
        scheduledNotificationsCollection,
        appEnv
      );
    });
};

export const triggeredScheduledNotificationsOnSurveyCompletionDev =
  createTriggeredNotificationFunction("dev");

export const triggeredScheduledNotificationsOnSurveyCompletionPilot =
  createTriggeredNotificationFunction("pilot");

export const triggeredScheduledNotificationsOnSurveyCompletionClinical =
  createTriggeredNotificationFunction("clinical");
