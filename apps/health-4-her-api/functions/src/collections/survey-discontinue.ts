import { Environment } from "./../types/client-types";
import * as functions from "firebase-functions";
import { collections } from "../constants/collections";
import { deleteScheduledNotifications } from "../helpers/notifications";

const envConfig = functions.config()?.env;
const region = envConfig?.region || "australia-southeast1";

const createTriggeredNotificationFunction = (appEnv: Environment) => {
  const surveyDiscontinueCollection = collections[appEnv].surveyDiscontinue;
  // const baselineSurveyCollection = collections[appEnv].baselineSurvey;
  const scheduledNotificationsCollection =
    collections[appEnv].scheduledNotifications;

  return functions
    .region(region)
    .firestore.document(`${surveyDiscontinueCollection}/{userId}`)
    .onCreate(async (snapshot) => {
      const data = snapshot.data();
      // if (data?.contactForFollowUpSurvey) {
      //   await createScheduledNotifications(
      //     snapshot,
      //     baselineSurveyCollection,
      //     scheduledNotificationsCollection,
      //     appEnv
      //   );
      // }

      if (!data?.contactForFollowUpSurvey) {
        await deleteScheduledNotifications(
          scheduledNotificationsCollection,
          snapshot.id
        );
      }
    });
};

export const triggeredNotificationsOnSurveyDiscontinueDev =
  createTriggeredNotificationFunction("dev");

export const triggeredNotificationsOnSurveyDiscontinuePilot =
  createTriggeredNotificationFunction("pilot");

export const triggeredNotificationsOnSurveyDiscontinueClinical =
  createTriggeredNotificationFunction("clinical");
