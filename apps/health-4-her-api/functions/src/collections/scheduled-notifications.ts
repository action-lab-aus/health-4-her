import { db } from "../firebase-init";
import { collections } from "../constants/collections";
import * as functions from "firebase-functions";
import { hasCompletedFollowupSurvey } from "../helpers/notifications";
import { Arm, Environment } from "../types/client-types";

const envConfig = functions.config()?.env;
const region = envConfig?.region || "australia-southeast1";

async function doesReferenceIdExist(
  referenceId: string,
  appEnv: "dev" | "pilot" | "clinical"
): Promise<boolean> {
  const snapshot = await db
    .collection(collections[appEnv].scheduledNotifications)
    .where("referenceId", "==", referenceId)
    .limit(1)
    .get();

  return !snapshot.empty;
}

export async function getArmForReferenceId(
  referenceId: string,
  appEnv: "dev" | "pilot" | "clinical"
): Promise<"control" | "intervention"> {
  const snapshot = await db
    .collection(collections[appEnv].scheduledNotifications)
    .where("referenceId", "==", referenceId)
    .limit(1)
    .get();

  if (snapshot.empty) {
    throw new Error("Reference ID not found");
  }

  const doc = snapshot.docs[0];

  return doc.get("arm");
}

export const checkReferenceId = functions
  .region(region)
  .https.onCall(
    async (data: {
      referenceId: string;
      appEnv: Environment;
    }): Promise<{ result: boolean; arm: Arm | null; errMsg: string }> => {
      const invalidRefIdErrMsg =
        "Invalid reference ID. Please re-check the link provided to you in the Email/SMS";

      const hasCompletedFupErrMsg =
        "You have already completed the follow-up survey. Thank you for your participation.";

      if (!data.referenceId) {
        return { result: false, arm: null, errMsg: invalidRefIdErrMsg };
      }

      const referenceIdExists = await doesReferenceIdExist(
        data.referenceId,
        data.appEnv
      );

      if (!referenceIdExists) {
        return { result: false, arm: null, errMsg: invalidRefIdErrMsg };
      }

      const hasCompletedFollowup = await hasCompletedFollowupSurvey(
        data.referenceId,
        collections[data.appEnv].followupSurvey
      );

      if (hasCompletedFollowup) {
        return { result: false, arm: null, errMsg: hasCompletedFupErrMsg };
      }

      const arm = await getArmForReferenceId(data.referenceId, data.appEnv);

      return { result: true, arm: arm, errMsg: "" };
    }
  );
