import * as admin from "firebase-admin";
import dotenv from "dotenv";
dotenv.config();

// eslint-disable-next-line @typescript-eslint/no-var-requires
const serviceAccount = require("../health-4-her-service-account.json");

admin.initializeApp({
  projectId: process.env.PROJECT_ID,
  credential: admin.credential.cert(serviceAccount),
});

if (process.env.FIRESTORE_EMULATOR_HOST) {
  admin.firestore().settings({
    host: process.env.FIRESTORE_EMULATOR_HOST,
    ssl: false,
  });
}

export const db = admin.firestore();
export const bucket = admin.storage().bucket("gs://health-4-her.appspot.com");
