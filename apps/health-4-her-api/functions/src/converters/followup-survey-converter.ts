import { firestore } from "firebase-admin";
import { CompletedFollowupSurvey } from "../types/survey-types";

export const followupSurveyConverter: firestore.FirestoreDataConverter<CompletedFollowupSurvey> =
  {
    toFirestore(followupSurvey: CompletedFollowupSurvey) {
      return followupSurvey;
    },
    fromFirestore(
      snapshot: firestore.QueryDocumentSnapshot
    ): CompletedFollowupSurvey {
      const data = snapshot.data();
      return data as CompletedFollowupSurvey;
    },
  };
