import { firestore } from "firebase-admin";
import { BaselineSurveyState } from "../types/survey-types";

export const baselineSurveyStateConverter: firestore.FirestoreDataConverter<BaselineSurveyState> =
  {
    toFirestore(surveyState: BaselineSurveyState) {
      return surveyState;
    },
    fromFirestore(
      snapshot: firestore.QueryDocumentSnapshot
    ): BaselineSurveyState {
      const data = snapshot.data();
      return data as BaselineSurveyState;
    },
  };
