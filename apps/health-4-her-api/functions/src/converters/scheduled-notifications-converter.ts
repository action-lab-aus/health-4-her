import { firestore } from "firebase-admin";
import { ScheduledNotification } from "./../types/scheduled-notification-types";

export const scheduledNotificationsConverter: firestore.FirestoreDataConverter<ScheduledNotification> =
  {
    toFirestore(scheduledNotification: ScheduledNotification) {
      return scheduledNotification;
    },
    fromFirestore(
      snapshot: firestore.QueryDocumentSnapshot
    ): ScheduledNotification {
      const data = snapshot.data();
      return data as ScheduledNotification;
    },
  };
