import * as functions from "firebase-functions";
import { collections } from "../constants/collections";
import { region } from "../constants/config";
import { checkForPendingScheduledNotifications } from "../helpers/notifications";

const melbourneTimezone = "Australia/Melbourne";
const everyDayAt12PM = "0 12 * * *";
// const everySixHoursStartingFromMidnight = "0 */12 * * *";
const everyHour = "0 * * * *";

export const sendScheduledNotificationsForPilot = functions
  .region(region)
  .pubsub.schedule(everyHour)
  .timeZone(melbourneTimezone)
  .onRun(async () => {
    functions.logger.info("Checking for pending scheduled notifications...");
    await checkForPendingScheduledNotifications(
      "pilot",
      collections.pilot.scheduledNotifications,
      collections.pilot.followupSurvey
    );
  });

export const sendScheduledNotificationsForClinic = functions
  .region(region)
  .pubsub.schedule(everyHour)
  .timeZone(melbourneTimezone)
  .onRun(async () => {
    functions.logger.info("Checking for pending scheduled notifications...");
    await checkForPendingScheduledNotifications(
      "clinical",
      collections.clinical.scheduledNotifications,
      collections.clinical.followupSurvey
    );
  });

export const sendScheduledNotificationsForDev = functions
  .region(region)
  .pubsub.schedule(everyDayAt12PM)
  .timeZone(melbourneTimezone)
  .onRun(async () => {
    functions.logger.info("Checking for pending scheduled notifications...");
    await checkForPendingScheduledNotifications(
      "dev",
      collections.dev.scheduledNotifications,
      collections.dev.followupSurvey
    );
  });

// console.log({
//   sendScheduledNotificationsForClinic,
//   sendScheduledNotificationsForDev,
//   sendScheduledNotificationsForPilot,
// });
