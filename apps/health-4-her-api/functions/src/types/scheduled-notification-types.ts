import { Arm } from "./client-types";
import { Timestamp } from "firebase-admin/firestore";

export type NotificationEventType =
  | "initial"
  | "questionnaire"
  | "reminder"
  | "reminder2";

export type Schedule = {
  sendAt: Timestamp;
  sent: boolean;
  title: string;
  template: NotificationEventType;
};

export type ScheduledNotification = {
  channels: { email: boolean; sms: boolean };
  firstName: string;
  lastName: string;
  emailAddress: string | null;
  nextSendAt: Timestamp | null;
  optOutPreferences: {
    email: { value: boolean; optedOutAt: Timestamp | null };
    sms: { value: boolean; optedOutAt: Timestamp | null };
  };
  mobileNumber: string | null;
  createdAt: Timestamp;
  schedule: Schedule[];
  referenceId?: string;
  arm: Arm | null;
};

export type ScheduledNotificationInterval = {
  value: number;
  unit: "days" | "minutes";
  template: NotificationEventType;
  description: string;
};

export type EmailSendType = {
  from: string;
  to: string;
  templateId: string;
  dynamicTemplateData: { firstName: string, surveyLink?: string };
  attachments: Attachment[];
};

export type Attachment = {
  content: string;
  filename: string;
  type: string;
  disposition: string;
};

export type SMSSendType = {
  from: string;
  to: string;
  body: string;
};
