export type ClientType = "clinical" | "pilot";

export type Environment = "dev" | "pilot" | "clinical";

export type Collections = {
  [env in Environment]: {
    [key in keyof typeof CollectionMapping]: key extends "userProgress"
      ? UserProgress
      : `${(typeof CollectionMapping)[key]}-${env}`;
  };
};

export type Arm = "intervention" | "control";

export const CollectionMapping = {
  baselineSurvey: "baseline-survey",
  postInterventionSurvey: "post-intervention-survey",
  surveyDiscontinue: "survey-discontinue",
  scheduledNotifications: "scheduled-notifications",
  followupSurvey: "followup-survey",
  userProgress: "user-progress",
} as const;

type UserProgress =
  | "userProgressDev"
  | "userProgressClinical"
  | "userProgressPilot";
