import { Timestamp } from "firebase-admin/firestore";

type SurveySection<T> = {
  [K in keyof T]: T[K] extends string
    ? string | undefined
    : T[K] extends object
    ? T[K]
    : never;
};

type AboutYouSection = SurveySection<{
  first_name: string;
  last_name: string;
  ph_mobile: string;
  email: string;
  postcode: string;
}>;

export type BaselineSurveyState = {
  aboutYou: AboutYouSection;
} & { completedAt: Timestamp };

export type CompletedFollowupSurvey = {
  referenceId: string;
  completedAt: Timestamp;
};
