type BaselineSurveyCSV = {
  firstName: string;
  familyName: string;
  communicationPreferences: string;
  mobileNumber: string;
  phoneNumber: string;
  emailAddress: string;
  postcode: string;
  currentAge: string;
  countryOfBirth: string;
  languageOtherThanEnglish: string;
  aboriginalOrTorresStraightIslander: string;
};
