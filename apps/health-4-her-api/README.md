# Health4Her API

A website designed to educate on the dangers of alcohol consumption.
This is the Backend for the Health4Her website that makes use of Firebase Functions.

## Setup

**Prerequisites**:

- [NodeJS](https://nodejs.org/en/download) (>=16)
- [pnpm](https://pnpm.io/installation) (>=8)

**Steps**:

- Copy the env variables required from [.runtimeconfig.template.json](.runtimeconfig.template.json) to a new file called `.runtimeconfig.json`
- Copy the env variables required from [src/.env.template](functions/src/.env.template) to a new file called `.env` under the same directory

## Running scripts of opting out users

- Access the script [here](functions/src/scripts/opt-out-users.ts)
- Create two files `emails/emails.txt` and `mobiles/mobiles.txt` which will be a newline separated list of emails and mobile numbers respectively
- From the scripts directory, run `ts-node opt-out-users.ts` to opt out the users whose emails and/or mobile numbers are in the files
- The script will access the Firestore collection to mark the user as opted out and also record the timestamp of when the user opted out
